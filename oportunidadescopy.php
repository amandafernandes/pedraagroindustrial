<?
require_once("inc/conn.php");
$_VVAG = true;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link href="estilos.css" rel="stylesheet" type="text/css" />
        <title>Pedra Agroindustrial</title>
        <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
        <!--[if IE 6]>
                <script type="text/javascript" src="files/png_fix.js"></script>
                <script type="text/javascript">
        DD_belatedPNG.fix('.pngFix');
    </script>
            <![endif]-->
        <script>
            window.onload = function (){
                var body = document.body,
                html = document.documentElement;

                var height = document.getElementById("oportunidades-copy").offsetHeight;
                height += 200;
                setInterval(function() {
                    parent.postMessage(height,"https://www.pedraagroindustrial.com.br/");
                },1000);
            };

            
            function inscrever(idp){
                if(!confirm("Deseja se inscrever neste Processo Seletivo?"))return false;
                    <? if ($_SESSION['estaLogado'] == true) { ?>
                        location="verLogin.php?irpara=1&idp="+idp;
                    <? } else { ?>

                        location="trabalheConoscocopy.php?irpara=1&idp="+idp;
                    <? } ?>
            }

        </script>
    </head>

    <body>
        <div id="oportunidades-copy">
            <?
            if ($_SESSION['estaLogado'] == true) {
                include("menuRHcopy.php");
            }
            ?>
        
            <div class="filter">
                <div class="filter-text">
                    <span>Selecione as vagas por unidade</span>
                </div>
                <div class="filter-select">
                    <form name="frmunidade" method="post" action="" style="margin:0px;">

                    <select name="unidadeid" onchange="document.frmunidade.submit();" id="unidade-select">
                        <option value="0" <? if (isset($_POST['unidadeid']) && $_POST['unidadeid'] == 0) {
                        echo 'selected';
                        } ?>>Todas Unidades</option>
                        <?
                        $rs = pg_query("SELECT * FROM rhtd_unidade ORDER BY nm_unidade ASC");
                        if (pg_num_rows($rs)) {
                        while ($rr = pg_fetch_assoc($rs)) {
                        ?>
                        <option value="<?= $rr['id_unidade'] ?>" <? if (isset($_POST['unidadeid']) && $_POST['unidadeid'] == $rr['id_unidade']) {
                        echo 'selected';
                        } ?>><?= $rr['nm_unidade'] ?></option>
                        <?
                        }
                        }
                        ?>
                        </select>

                    </form>
                    <div class="filter-select_btn">
                        <button onclick="document.frmunidade.submit();" >Filtrar</button>
                    </div>
                </div>

            </div>
                    <?
                    if (isset($_POST['unidadeid']) && $_POST['unidadeid'] > 0) {
                    $WHERE = " AND rhtd_unidade.id_unidade=" . $_POST['unidadeid'];
                    }

                    $rs = pg_query("
                        SELECT
                            rhtd_unidade.nm_unidade as nomeunidade,
                            rhtd_vaga_disponivel.processo_seletivo as idprocesso,
                            rhtd_vaga_disponivel.titulo_vaga as titulo,
                            rhtd_vaga_disponivel.data_divulgacao as data,
                            rhtd_vaga_disponivel.qtde_vagas as qtdevagas,
                            rhtd_vaga_disponivel.descricao_vaga as descricao,
                            rhtd_vaga_disponivel.conhecimentos as conhecimentos,
                            rhtd_vaga_disponivel.observacoes as obs,
                            rhtd_vaga_disponivel.remuneracao as remuneracao,
                            rhtp_area_atuacao_cand.nm_area_atuacao as nomeareaatuacao
                        FROM
                            rhtd_unidade
                            Inner Join rhtd_vaga_disponivel ON rhtd_vaga_disponivel.unidade = rhtd_unidade.id_unidade
                            Inner Join rhtp_area_atuacao_cand ON rhtp_area_atuacao_cand.id_tp_area_atuacao_cand = rhtd_vaga_disponivel.area_atuacao 
                        WHERE 
                            (rhtd_vaga_disponivel.data_divulgacao <= '" . date("Y-m-d") . "' AND rhtd_vaga_disponivel.data_encer_divulgacao >= '" . date("Y-m-d") . "')
                            " . $WHERE . "
                        ORDER BY
                            rhtd_vaga_disponivel.data_divulgacao DESC
                        ");

                        if (pg_num_rows($rs)) {
                            while ($rr = pg_fetch_assoc($rs)) {
                                                                                                    ?>
                            <div class="job-item">
                                <div class="job-item__container">
                                    <div class="tag green"><span> <?= $rr['nomeunidade'] ?></span></div>
                                    <h2><?= $rr['titulo'] ?> | <?= dataeuabr($rr['data']) ?></h2>
                                    <p><b>Área de Atuação : </b> <?= $rr['nomeareaatuacao'] ?> </p>        
                                    <p><b>Remuneração : </b> R$<?= number_format($rr['remuneracao'], 2, ',', ''); ?></p>
                                    <p><b>Quantidade de Vagas:</b> <?= $rr['qtdevagas'] ?></p>
                                    <p><b>Descrição da Vaga:</b><?= $rr['descricao'] ?></p>
                                    <p><b>Conhecimento : </b>
                                    <?= $rr['conhecimentos'] ?></p>
                                    <p><b>Observações : </b>
                                    <?= $rr['obs'] ?></p>

                                    <div class="job-item__btn">
                                        <div class="job-item__btn-container">
                                            <a href="#" class="btn" onclick="inscrever(<?= $rr['idprocesso'] ?>)">
                                                <span class="btn-text">Inscreva-se no Processo Seletivo </span>
                                                <span class="btn-arrow">
                                                    <i class="icon icon-svg_arrow-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                    <?
                }
            }
            ?>    
            </div>

        </div>
        </div>                                                            
    </body>
    <script>
        //Deleta Ibira das opcoes
        var selectobject = document.getElementById('unidade-select');
            for (var i=0; i<selectobject.length; i++){
              if (selectobject.options[i].value == '21' )
                 selectobject.remove(i);
            }
    </script>
</html>
