<?

	require_once("inc/db_class.php");

	if (hasPermission2()==false) {
		session_destroy();
		die("<script>location = 'index.php';</script>");
	}

	$ERROS = array('',
		'Digite um nome',
		'Selecione o arquivo',
		'Selecione uma Categoria'
	);

	$FORM = 'arquivoForm.php';
	$LIST = "arquivos.php";

	$ADDPERMI = 'add_arquivo';
	$CHAPERMI = 'change_arquivo';
	$DELPERMI = 'delete_arquivo';

	if($_GET['acao'] == 'add'){

		echo "[".$_POST['emp_publico']."]<br>";

		if($_POST['nome'] == "") $IDERROR = 1;
		else if($_FILES["arquivo"]["name"] == '') $IDERROR = 2;
		else if($_POST['categoria'] == '') $IDERROR = 3;
		else{
			$relatorio = "";
			if($_FILES["arquivo"]["name"] != ""){
				saveArquivo($_FILES["arquivo"], "arquivos/".$_FILES["arquivo"]["name"]);
				$relatorio = $_FILES["arquivo"]["name"];
			}
			/*
			$SQL = "INSERT INTO adm_arq_restr (nome, arquivo, data_inc) VALUES ('".$_POST['nome']."', '".$relatorio."', now())";

			//fg_pub_emp, categ_arq, fg_pub_tp_filial

			$DB->query($SQL);

			if($_SESSION['adminSuper']==1){
				$SQL = "SELECT * FROM relatorios";
				$rs = $DB->query($SQL);
				$i = 0;
				$t = $DB->numRows($rs);
				if($t>0){
					while($rr = $DB->fetchAssoc($rs)){
						$i++;
						$rels .= $rr['id'];
						if($i<$t)$rels .= ",";
					}
				}
			}else{
				$SQL = "SELECT * FROM auth_user_relatorios WHERE id_usuario = ".$_SESSION['adminAuthUserId'];
				$rs = $DB->query($SQL);
				$i = 0;
				$t = $DB->numRows($rs);
				if($t>0){
					while($rr = $DB->fetchAssoc($rs)){
						$i++;
						$rels .= $rr['id_relatorio'];
						if($i<$t)$rels .= ",";
					}
				}
			}
			$_SESSION['relatoriosAdmin'] = $rels;
			*/

			unset($_POST);
			httpResponseRedirect($LIST."?IDERROR=3");
		}
	}

	if($_GET['acao'] == 'alt'){
		if($_POST['nome'] == "") $IDERROR = 1;
		else if($_POST['categoria'] == '') $IDERROR = 3;		
		else{
			/*
			$SQL = "UPDATE relatorios set nome='".$_POST['nome']."' WHERE id = ".$_GET['id'];
			$DB->query($SQL);
			*/

			$relatorio = "";
			if($_FILES["arquivo"]["name"] != ""){
				saveArquivo($_FILES["arquivo"], "relatorios/".$_FILES["arquivo"]["name"]);
				$relatorio = $_FILES["arquivo"]["name"];

				$SQL = "UPDATE relatorios set arquivo='".$relatorio."' WHERE id = ".$_GET['id'];
				$DB->query($SQL);
			}

			unset($_POST);
			httpResponseRedirect($LIST."?IDERROR=4");
		}
	}

	if($_GET['acao'] == "del"){
		if($_GET['id'] != ""){
			/*
			$DB->query("DELETE FROM usfilial_acesso WHERE relatorio=".$_GET['id']);
			$DB->query("DELETE FROM auth_user_relatorios WHERE id_relatorio=".$_GET['id']);
			$DB->query("DELETE FROM relatorios WHERE id=".$_GET['id']);

			if($_SESSION['adminSuper']==1){
				$SQL = "SELECT * FROM relatorios";
				$rs = $DB->query($SQL);
				$i = 0;
				$t = $DB->numRows($rs);
				if($t>0){
					while($rr = $DB->fetchAssoc($rs)){
						$i++;
						$rels .= $rr['id'];
						if($i<$t)$rels .= ",";
					}
				}
			}else{
				$SQL = "SELECT * FROM auth_user_relatorios WHERE id_usuario = ".$_SESSION['adminAuthUserId'];
				$rs = $DB->query($SQL);
				$i = 0;
				$t = $DB->numRows($rs);
				if($t>0){
					while($rr = $DB->fetchAssoc($rs)){
						$i++;
						$rels .= $rr['id_relatorio'];
						if($i<$t)$rels .= ",";
					}
				}
			}
			$_SESSION['relatoriosAdmin'] = $rels;
			*/
			
			httpResponseRedirect($LIST."?IDERROR=1");
		}
	}

	$AREA = 'Arquivo';
	$OQUE = "Adicionar";
	if($_GET['id'] != "") $OQUE = "Alterar";
	$TYPE = "FORM";

	require_once("topo.php");
?>
	<div id="content" class="colM">

        <h1><?=$OQUE;?> - <?=$AREA;?></h1>

        <?
		$DELLINK = "";
		
		$t = "checked";
		$tdiv = "none";
		/*
		if($_GET['id'] != ""){
			$SQL = "SELECT * FROM adm_arq_restr WHERE id = ".$_GET['id'];
			$qDados = $DB->query($SQL);
			$arrDados = $DB->fetchAssoc($qDados);
			
			//se for publico prá todas empresas, será publico para todos 
			if ($arrDados['fg_pub_emp']=='N') {
				$t = '';
				$tdiv = 'block';
			}
		}

		if($_GET['id'] != ""){
			$DELLINK = $FORM.'?acao=del&id='.$_GET['id'];
		}
		*/

		if($_GET['id'] != ""){
			$FRMACTION = "?acao=alt&id=".$_GET['id'];
		}else{
			$FRMACTION = "?acao=add";
		}

		$ENCTYPE = "multipart/form-data";

		$f = new Form();
		$f->crate($FRMACTION,'form_noticia',true,$DELLINK,$ENCTYPE);

		$f->fieldSet("Informações");
			$v = ($arrDados['nome'] != "")?$arrDados['nome']:"";
			$f->charField("nome", "Nome", $v);
		$f->endFieldSet();

		$f->fieldSet("Arquivo");
			$arq = ($arrDados['arquivo'] != "")?array('url'=>'arquivos/'.$arrDados['arquivo'],'nome'=>$arrDados['arquivo']):array();
			$f->imageField("arquivo", "Arquivo", $arq, true, '', false);
		$f->endFieldSet();

		$f->fieldSet("Empresas");
			//tipo filiais
			$SQLt = "SELECT * FROM adm_tp_filial ORDER BY nome";
			$qt = $DB->query($SQLt);

			$r = "<option value=\'S\'>Todos os tipos</option>";
			while($dt = $DB->fetchAssoc($qt)){
				$r .= "<option value='".$dt['id_tp_filial']."'";
				//if () {
				//	$r .= " selected ";
				//}
				$r .= ">".$dt['nome']."</option>";
			}
			
			$f->htmlContentBlock('
				<div class="form-row emp_publico "><div>
					<div style="float:left; width:600px; line-height:30px;">
						<input id="id_emp_publico" type="checkbox" name="emp_publico" '.$t.' onclick="mostraEmpresas()" />
						<label class="vCheckboxLabel" for="id_emp_publico">Tornar público, este arquivo, para os fornecedores de todas as empresas.</label>
					</div>
					<div style="float:left; width:400px; line-height:30px;" id="filial_global">
						<div style="float:left;">
							Tipo de Filiais:&nbsp;&nbsp;&nbsp;
						</div>
						<div style="float:left;">
							<select id="sel_tipofiliais_global">'.$r.'</select>
						</div>
					</div>
				</div></div>
			');

			//empresas
			$SQL = "SELECT a.nome_filial as nome, a.id_empresa_prop as id FROM usempresa_prop a WHERE a.id_empresa_prop IN ( 1, 11, 21, 31 ) ORDER BY a.id_empresa_prop";
			$q = $DB->query($SQL);

			$f->htmlContentBlock('<div class="form-row" id="div_empresas" style="display:'.$tdiv.';">');
			$i = 0;
			while($d = $DB->fetchAssoc($q)){
				$f->htmlContentBlock('
					<div style="float:left; width:1050px; line-height:30px; border-bottom:1px solid #eee;">
						<div style="float:left; width:500px; line-height:30px;">
							<input id="check_emp_'.$i.'" type="checkbox" onclick="mostraPublicoFilial(\''.$i.'\')" />
							<label class="vCheckboxLabel" for="check_emp_'.$i.'">'.$d['nome'].'</label>
						</div>
						<div id="div_pubfiliais_'.$i.'" style="float:left; width:550px; line-height:30px; display:none;">
							<div style="float:left;">
								Tipo de Filiais:&nbsp;&nbsp;&nbsp;
							</div>
							<div style="float:left;">
								<select id="sel_tipofiliais_'.$i.'">'.$r.'</select>
							</div>
						</div>
						<input type="hidden" value="'.$d['id'].'" id="empresa_'.$i.'" />
					</div>
				');
				$i++;
			}
			$f->htmlContentBlock('</div>');


		$f->endFieldSet();

		$f->endForm($DELLINK);

		?>
        <br class="clear" />
        <span id="DOM"></span>
    </div>
	<script>
	function mostraPublicoFilial(x){
		c = $('check_emp_'+x);
		d = $('div_pubfiliais_'+x);
		if (c.checked) d.style.display = 'block';
		else d.style.display = 'none';
	}
	function mostraEmpresas(){
		d = $('div_empresas');
		f = $('filial_global');
		if (d.style.display == 'none') {
			d.style.display = 'block';
			f.style.display = 'none';
		} else {
			d.style.display = 'none';
			f.style.display = 'block';
		}
	}
	</script>
<?
	require_once("rodape.php");
?>