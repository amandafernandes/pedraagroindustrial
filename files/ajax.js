var mensure = null;
///////////////////////////////////////////////////////////////
// Funes e variveis comuns para todos os objetos          //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

var isIE = (/\bmsie\b/i.test(navigator.userAgent) && document.all && !(/\bopera\b/i.test(navigator.userAgent)));
$ = function() { return document.getElementById(arguments[0]); }

///////////////////////////////////////////////////////////////
// Funes para criar Drag and Drop em objetos               //
// Verso: 1.0                                               //
// Bibliotecas Necessrias:                                  //
//  - pageSize()                                             //
///////////////////////////////////////////////////////////////

var objSelecionado = null;
var Moving = false;
var mouseOffset = null;
var newPosX, newPosY, finPosX, finPosY, objAtual, scrTop;
var iStart = 1000;


String.prototype.isDate = function() {
	try {
		var arrData = new Array();
		arrData = this.split('/');
		var dia = arrData[0];
		var mes = arrData[1]-1;
		var ano = arrData[2];
		var dataEntrada = (arrData[0]*1)+'/'+(arrData[1]*1)+'/'+arrData[2];
		var minhaData = new Date(ano,mes,dia);
		var dataRetorno = minhaData.getDate()+'/'+(minhaData.getMonth()+1)+'/'+minhaData.getFullYear();
		if (dataEntrada == dataRetorno) {
			return true;
		} else {
			return false;
		}
	} catch(e) {
		return false;
	}
}



function addEvent(obj, evType, fn) {
	if (typeof obj == "string") {
		if (null == (obj=document.getElementById(obj))) {
			throw new Error("Elemento HTML no encontrado. No foi possvel adicionar o evento.");
		}
	}
	if (obj.attachEvent) {
		return obj.attachEvent(("on"+evType), fn);
	} else if (obj.addEventListener) {
		return obj.addEventListener(evType, fn, true);
	} else {
		throw new Error("Seu browser no suporta adio de eventos.");
	}
}
/*
document.onmousemove = function(ev) {
	var ev = ev || window.event;
	var mousePos = mouseCoords(ev);
	if (objSelecionado) {
		objAtual = objSelecionado;
		Moving = true;
		newPosX = mousePos.x-mouseOffset.x;
		newPosY = mousePos.y-mouseOffset.y;
		
		if (newPosX < 0) newPosX = 0;
		if (newPosY < scrTop) newPosY = scrTop;
		if (newPosX > finPosX) newPosX = finPosX;
		if (newPosY > finPosY) newPosY = finPosY;
		
		if (newPosX < 10) newPosX = 0;
		if (newPosY < (scrTop-10)) newPosY = scrTop;
		if (newPosX > (finPosX-10)) newPosX = finPosX;
		if (newPosY > (finPosY-10)) newPosY = finPosY;
		
		with ($('div_alert_move').style) {
			display = 'block';
			left = newPosX+'px';
			top = newPosY+'px';
			width = $(objAtual)._width+'px';
			height = $(objAtual)._height+'px';
			margin = '0px';
		}
		return false;
	}
};
*/
function mouseCoords(ev) {
	if (ev.pageX || ev.pageY) {
		return {x:ev.pageX, y:ev.pageY};
	}
	return {x:ev.clientX+document.body.scrollLeft-document.body.clientLeft, y:ev.clientY+document.body.scrollTop-document.body.clientTop};
}

function getPosition(e, ev) {
	e = document.getElementById(e);
	var left = 0;
	var top = 0;
	var coords = mouseCoords(ev);
	while (e.offsetParent) {
		left += e.offsetLeft;
		top += e.offsetTop;
		e = e.offsetParent;
	}
	left += e.offsetLeft;
	top += e.offsetTop;
	return {x:coords.x-left, y:coords.y-top};
}
/*
document.onmouseup = function() {
	if ((objSelecionado) && (Moving)) {
		with ($(objSelecionado).style) {
			left = newPosX+'px';
			top = newPosY+'px';
			margin = '0px';
		}
	}
	$('div_alert_move').style.display = 'none';
	Moving = false;
	objSelecionado = null;
};
*/
function dragdrop(local_click, caixa_movida) {
	document.getElementById(local_click).style.cursor = 'move';
	addEvent(local_click, 'mousedown', function (ev) {
		objSelecionado = caixa_movida;
		mouseOffset = getPosition(objSelecionado, ev);
		change(objSelecionado);
		var ne = new pageSize();
		scrTop = ne['sT'];
		finPosX = Number(ne['w'])-Number($(objSelecionado)._height)-150;

		finPosY = Number(ne['h'])-Number($(objSelecionado)._width)+138+scrTop;
	});
}

function drag(obj, janela) {
	objSelecionado = janela;
	mouseOffset = getPosition(objSelecionado, window.event);
	change(objSelecionado);
	var ne = new pageSize();
	scrTop = ne['sT'];
	finPosX = Number(ne['w'])-Number($(objSelecionado)._height)-150;
	finPosY = Number(ne['h'])-Number($(objSelecionado)._width)+138+scrTop;
}

function change(obj) {
	$(obj).style.zIndex = ++iStart;
	$('div_alert_move').style.zIndex = ++iStart;
}

///////////////////////////////////////////////////////////////
// Objeto para cculo de mdias artimticas e ou ponderada   //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

calcMedia = function() {

	// Variveis utilizadas no objeto
	var numQuant = new Number(0);
	var arrCalc = new Array();
	var arrPond = new Array();
	var numRet = new Number(0);

	// Retorna o valor calculado
	this.getReturn = function() {
		return numRet;
	}

	// Adiciona itens  matriz que Ser Calculada
	// 1 argumento - Valor que ser adicionado
	// 2 argumento - Peso (Opicional, valor padro 1)
	this.addItem = function() {
		arrCalc[numQuant] = arguments[0];
		if (arguments.length > 1) {
			arrPond[numQuant++] = arguments[1];
		} else {
			arrPond[numQuant++] = 1;
		}
	}

	// Limpa o array do clculo
	this.empty = function() {
		numQuant = new Number(0);
		arrCalc = new Array();
		arrPond = new Array();
	}

	// Executa a soma e retorna o resultado
	this.execute = function() {
		try {
			var soma = new Number(0);
			var div = new Number(0);
			for (var i = 0; i < arrCalc.length; i++) {
				soma += arrCalc[i] * arrPond[i];
				div += arrPond[i];
			}
			numRet = (soma/div);
			return (soma/div);
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}

}

///////////////////////////////////////////////////////////////
// Objeto para formatao de datas                           //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

formatDate = function() {

	// Variveis utilizadas no objeto
	var strFormat = new String('DD/MM/YYYY');
	var strReturn = new String('');
	var dteDateIn = new Date();
	var tipos = new Array();
	var semana = new Array('domingo','segunda-feira','tera-feira','quarta-feira','quinta-feira','sexta-feira','sbado');
	var meses = new Array('janeiro','fevereiro','maro','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro');

	// Informa a data que ser utilizada na formatao
	this.setDateIn = function() {
		dteDateIn = new Date(arguments[0]);
	}

	// Informa o formato de sada
	this.setFormatOut = function() {
		strFormat = arguments[0];
	}

	// Retorna a data j convertida
	this.getReturn = function() {
		return strReturn;
	}

	// Executa a formatao e retorna o valor j formatado
	this.execute = function() {
		try {
			tipos['ms'] = dteDateIn.getMilliseconds();
			tipos['dd'] = semana[dteDateIn.getDay()];
			tipos['mmmm'] = meses[dteDateIn.getMonth()];
			tipos['mmm'] = meses[dteDateIn.getMonth()].substring(0, 3);
			tipos['hh'] = ((dteDateIn.getHours() < 10) ? "0" : "") + dteDateIn.getHours();
			tipos['mm'] = ((dteDateIn.getMinutes() < 10) ? "0" : "") + dteDateIn.getMinutes();
			tipos['ss'] = ((dteDateIn.getSeconds() < 10) ? "0" : "") + dteDateIn.getSeconds();
			tipos['DD'] = dteDateIn.getDate();
			tipos['MM'] = (dteDateIn.getMonth()+1);
			tipos['YYYY'] = dteDateIn.getFullYear();
			tipos['DD2'] = ((dteDateIn.getDay() < 10) ? "0" : "") + dteDateIn.getDay();
			tipos['MM2'] = ((dteDateIn.getMonth() < 10) ? "0" : "") + dteDateIn.getMonth();
			strReturn = strFormat;
			for (var i in tipos) {
				strReturn = strReturn.replace(i,tipos[i]);
			}
			return strReturn;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}

}

///////////////////////////////////////////////////////////////
// Objeto para trabalho com xml                              //
// Verso: 2.0                                               //
///////////////////////////////////////////////////////////////

xmlConnection = function() {
	// Variveis utilizadas no objeto
	var strNav = new String('');
	var objAjax = null;
	var strMethod = new String('GET');
	var strParams = new String('');
	var strURL = new String('');
	var strChildName = new String('');
	var arrParamName = new Array();
	var arrParamValue = new Array();
	var numTotalParams = new Number(0);
	var xmlReturn = null;
	var textReturn = new String('');
	var thisObj = this;
	
	// Evento executado quando o xml  lido comsucesso
	this.onComplete = function () { };
	// Evento executado quando o xml acabou de ser lido | Retorna o cdigo do resultado Ex.: 200, 401...
	this.onLoad = function () { };
	// Evento executado quando o xml troca de status | Retorna o status atual
	this.onStateChange = function () { };
	// Evento executado quando o xml no foi lido | Retorna o cdigo do erro
	this.onError = function() { alert('Erro ao ler o xml!'); }
	
	// Adiciona itens as array de parmetros (variveis que sero passadas)
	this.addParameters = function() {	
		arrParamName[numTotalParams] = arguments[0];
		arrParamValue[numTotalParams] = arguments[1];
		numTotalParams++;
	}
	
	this.add = function() {
		arrParamName[numTotalParams] = arguments[0];
		arrParamValue[numTotalParams] = arguments[1];
		numTotalParams++;
	};	

	// Limpa o array de parmetros
	this.emptyParameters = function() {
		numTotalParams = new Number(0);
		arrParamName = new Array();
		arrParamValue = new Array();
	}
	
	// Informa para o objeto a URL do xml
	this.setURL = function() {
		strURL = arguments[0];
	}

	// Informa para o objeto o mtodo utilizado para os envios dos dados
	this.setMethod = function() {
		if ((arguments[0].toUpperCase() == 'POST') || (arguments[0].toUpperCase() == 'GET')) {
			strMethod = arguments[0].toUpperCase();
		}
	}
	
	// Informa para o objeto o nome da tag padro para retorno dos dados
	this.setChildName = function() {
		strChildName = arguments[0];
	}

	// Retorna o status da leitura do xml
	this.getStatus = function() {
		return objAjax.statusText;
	}

	// Retorna o browser do usurio ('ie' ou 'ff')
	this.getBrowser = function() {
		return strNav;
	}

	// Retorna o XML recebido
	this.getXML = function() {
		return xmlReturn;
	}

	// Retorna o texto recebido
	this.getText = function() {
		return textReturn;
	}
	
	// Retorna a quantidade de itens da tag especificada pela funo setChildName
	this.getCountItens = function() {
		return xmlReturn.getElementsByTagName(strChildName).length;
	}
	
	// Retorna o atributo da tag especificada pela funo setChildName
	this.getAttByName = function() {
		try { return xmlReturn.getElementsByTagName(strChildName)[arguments[1]].getAttribute(arguments[0]); } catch(e) { return null; }
	}
	
	// Retorna o CDATA da tag especificada pela funo setChildName
	this.getDataByName = function() {
		try { return xmlReturn.getElementsByTagName(strChildName)[arguments[0]].firstChild.data; } catch(e) { return null; }
	}
	
	// Faz o envio de dados e aguarda o retorno
	this.execute = function() {
		strParams = '';
		for (var i = 0; i < arrParamName.length; i++) {
			if (i > 0) strParams += '&';
			strParams += arrParamName[i]+'='+arrParamValue[i];
		}
		if (strMethod == 'GET') {
			objAjax.open(strMethod, strURL+'?'+strParams, true);
		} else {
			objAjax.open(strMethod, strURL, true);
		}
		objAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");//; iso-8859-1
		objAjax.setRequestHeader("CharSet", "UTF-8");
		objAjax.setRequestHeader("Cache-Control","no-store, no-cache, must-revalidate");
		objAjax.setRequestHeader("Cache-Control","post-check=0, pre-check=0");
		objAjax.setRequestHeader("Pragma", "no-cache");
		objAjax.onreadystatechange = function() {
			thisObj.onStateChange(objAjax.readyState);
			if (objAjax.readyState == 4){
				thisObj.onLoad(objAjax.status);
				if (objAjax.status == 200){
					xmlReturn = objAjax.responseXML;
					textReturn = objAjax.responseText;
					thisObj.onComplete();
				} else {
					thisObj.onError(objAjax.statusText, objAjax.status);
				}
			}
		}
		if (strMethod == 'GET') {
			objAjax.send(null);
		} else {
			objAjax.send(strParams);
		}
	}

	// Cria o objeto AJAX (utilizado para envio e recebimento dos dados)
	this.create = function() {
		if (window.ActiveXObject) {
			strNav = 'ie';
			try {
				objAjax = new ActiveXObject("Msxml2.XMLHTTP.4.0");
			} catch(e) {
				try {
					objAjax = new ActiveXObject("Msxml2.XMLHTTP.3.0");
				} catch(e) {
					try {
						objAjax = new ActiveXObject("Msxml2.XMLHTTP");
					} catch(e) {
						try {
							objAjax = new ActiveXObject("Microsoft.XMLHTTP");
						} catch(e) {
							objAjax = null;
							return false;
						}
					}
				}
			}
		} else if (window.XMLHttpRequest) {
			objAjax = new XMLHttpRequest();
			strNav = 'ff';
		}
		return true;
	}

}


///////////////////////////////////////////////////////////////
// Objeto para manuteno de css                             //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

cssConnection = function() {
	
	// Variveis utilizadas no objeto
	var rulesName = isIE ? 'rules' : 'cssRules';
	var domNode = isIE ? 'owningElement' : 'ownerNode';
	var numStyles = new Number(0);
	var numRules = new Number(0);
	var strRuleAnt = new String('');
	
	// Adiciona estulos na pgina criada dinmicamente
	this.addStyle = function() {
		if (arguments[0] != '') {
			strRuleAnt = arguments[0];
		}
		try {
			if (isIE) {
				document.styleSheets[numStyles].addRule(strRuleAnt, arguments[1]+': '+arguments[2]+';');
			} else {
				document.styleSheets[numStyles].insertRule(strRuleAnt+' {'+arguments[1]+': '+arguments[2]+';}', document.styleSheets[numStyles].cssRules.length);
			}
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}
	
	// Cria a pgina de estilos dinmica e guarda o endereo dela
	this.create = function() {
		try {
			if (document.styleSheets.length > 0) {
				document.writeln("<style>body { } <\/style>");
			}
			numStyles = document.styleSheets.length-1;
			numRules = document.styleSheets[numStyles][rulesName].length-1;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}	

}

///////////////////////////////////////////////////////////////
// Objeto para criao de tabela                             //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

table = function() {

	// Variveis utilizadas no objeto
	var mtzTable = new Array();
	var mtzTableGen = new Array();
	var numColls = new Number(0);
	var numLines = new Number(1);
	var strTable = new String('');
	var arrProperts = new Array();
	var strProperts = new String('');
	var arrTtClass = new Array();
	var arrClClass = new Array();
	var arrClClass2 = new Array();
	var arrProperts = new Array();
	
	// Adiciona colunas  tabela dinmica
	// 1 argumento - ttulo da tabela
	// 2 argumento - classe do ttlo da coluna
	// 3 argumento - classe do item 1 da coluna
	// 4 argumento - classe do item 2 da coulna (paginao)
	this.addColumn = function() {
		mtzTable[numColls] = new Array();
		mtzTable[numColls][0] = arguments[0];
		if (arguments.length > 1) {
			arrTtClass[numColls] = arguments[1];
		}
		if (arguments.length > 2) {
			arrClClass[numColls] = arguments[2];
		}
		if (arguments.length > 3) {
			arrClClass2[numColls] = arguments[3];
		}
		numColls++;
	}
	
	// Retorna o HTML da tabela
	this.getHTML = function() {
		return strTable;
	}
	
	// Adiciona linhas  tabela (Os argumentos so os dados das colunas)
	this.addLine = function() {
		for (var i = 0; i < arguments.length; i++) {
			mtzTable[i][numLines] = arguments[i];
		}
		numLines++;
	}
	
	// Adiciona as propriedades  tabela (Ex.: cellspacing, border...)
	this.addPropert = function() {
		arrProperts[arguments[0]] = arguments[1];
	}
	
	// Remove uma coluna especificada
	this.emptyColumn = function() {
		mtzTableGen = mtzTable;
		mtzTable = new Array();
		var aux01 = new Number(0);
		for (var i = 0; i < numColls; i++) {
			if (i != arguments[0]) {
				mtzTable[aux01] = new Array();
				for (var j = 0; j < numLines; j++) {
					mtzTable[aux01][j] = mtzTableGen[i][j];
				}
				aux01++;
			}
		}
		numColls--;
	}
	
	// Remove uma linha especificada
	this.emptyLine = function() {
		mtzTableGen = mtzTable;
		mtzTable = new Array();
		var aux01 = new Number(0);
		for (var i = 0; i < numColls; i++) {
			mtzTable[i] = new Array();
			aux01 = 0;
			for (var j = 0; j < numLines; j++) {
				if (j != arguments[0]) {
					mtzTable[i][aux01] = mtzTableGen[i][j];
					aux01++;
				}
			}
		}
		numLines--;
	}
	
	// Limpa a tabela inteira
	this.emptyTable = function() {
		mtzTable = new Array();
		numColls = new Number(0);
		numLines = new Number(0);
	}
	
	// Zera as propriedades da tabela
	this.emptyProperts = function() {
		arrProperts = new Array();
	}
	
	// Escreve a tabela em forma de HTML
	this.execute = function() {
		strProperts = '';
		for (indice in arrProperts) {
			strProperts += ' '+indice+'="'+arrProperts[indice]+'"';
		}
		strTable = '<table'+strProperts+'>';
		var strClass = new String('');
		var aux01 = new Number(0);
		for (var i = 0; i < numLines; i++) {
			strTable += '<tr>';
			aux01 = i%2;
			for (var j = 0; j < numColls; j++) {
				if (i == 0) {
					strClass = arrTtClass[j];
				} else {
					if ((aux01 == 1) || (arrClClass2[j] == '')) {
						strClass = arrClClass[j];
					} else {
						strClass = arrClClass2[j];
					}
				}
				if (mtzTable[j][i] != '') {
					strTable += '<td class="'+strClass+'">'+mtzTable[j][i]+"<\/td>";
				} else {
					strTable += '<td class="'+strClass+'">&nbsp;'+"<\/td>";
				}
			}
			strTable += "<\/tr>";
		}
		strTable += "<\/table>";
		return strTable;
	}
	
}

///////////////////////////////////////////////////////////////
// Objeto para manuteno de cookies                         //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

cookieConnection = function() {
	
	// Variveis utilizadas no objeto
	var data = new Date();
	
	// Grava dados no cookie
	this.writeCookie = function() {
		try {
			var expires = "";
			if(arguments.length > 2) {
			  data.setTime(data.getTime()+(arguments[2]*86400000));
			  expires = data.toGMTString() + "; ";
			}
			document.cookie = arguments[0] + "=" + arguments[1] + "; " + expires + "path=/";
			return true;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}
	
	// L dados do cookie
	this.readCookie = function() {
		try {
			var nameE = arguments[0] + "=";
			var cookies = document.cookie.split(";");
			for(var i = 0, Cookie; Cookie = cookies[i]; i++) {
			  while(Cookie.charAt(0) == " ") {
				Cookie = Cookie.substring(1,Cookie.length);
			  }
			  if(Cookie.indexOf(nameE) == 0) {
				return Cookie.substring(nameE.length,Cookie.length);
			  }
			}
			return false;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}	
	
	// Apaga dados do cookie
	this.deleteCookie = function() {
		try {
			this.writeCookie(arguments[0], "", -1);
			return true;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}	

}

///////////////////////////////////////////////////////////////
// Objeto para deteco dos tamanhos das pginas             //
// Verso: 1.0                                               //
///////////////////////////////////////////////////////////////

pageSize = function() {

	var arrReturn = new Array();
	var theWidth, theHeight, theWidth_, theHeight_,scrollY;

	if (self.pageYOffset){
		scrollY = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop) {
		scrollY = document.documentElement.scrollTop;
	} else if (document.body) {
		scrollY = document.body.scrollTop;
	}

	if (window.innerWidth) {
		theWidth=window.innerWidth;
	} else if (document.documentElement && document.documentElement.clientWidth) {
		theWidth=document.documentElement.clientWidth;
	} else if (document.body) {
		theWidth=document.body.clientWidth;
	}
	
	if (window.innerHeight) {
		theHeight=window.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) {
		theHeight=document.documentElement.clientHeight;
	} else if (document.body) {
		theHeight=document.body.clientHeight;
	}
	
	theWidth_=document.body.clientWidth;
	theHeight_=document.body.clientHeight;
	
	arrReturn['w'] = theWidth;
	arrReturn['h'] = theHeight;
	arrReturn['w_'] = theWidth_;
	arrReturn['h_'] = theHeight_;
	arrReturn['sT'] = scrollY;
	
	return arrReturn;

}

///////////////////////////////////////////////////////////////
// Objeto para criao de alertas customizados               //
// Verso: 2.0                                               //
// Bibliotecas Necessrias:                                  //
//  - pageSize()                                             //
//  - Funces de Drag and Drop                               //
///////////////////////////////////////////////////////////////

var numAlerts = new Number(0);
var numOpenedAlerts = new Number(0);
var numOpenedFundo = new Number(0);
pubAlert = function() {
	
	// Variveis utilizadas no objeto
	var strReturn = new String('');
	var strTitle = new String('Erro!');
	var strButtons = new String('OK');
	var htmButtons = new String();
	var strMessage = new String('');
	var strIcon = new String('');
	var arrButtons = new Array('OK');
	var thisObj = this;
	var strBtName = new String('___bt');
	var ps = new pageSize();
	var blnCreated = new Boolean(false);
	var numAlertWidth = new Number(300);
	var numAlertHeight = new Number(50);
	var hideOnClick = new Boolean(true);
	var arrButtons = new Array('YES','NO','OK','CANCEL','INSERT','DELETE','UPDATE','SAVE','FIND','CLOSE');
	var numThisAlert = new Number(0);
	var strNameObject = new String(arguments[0]);
	var numTotalSize = new Number(0);
	
	// Retorna o boto que foi clicado
	this.onClick = function() { };
	
	// Cria o alerta
	// 1 argumento: Mensagem
	// 2 argumento: Ttulo da mensagem
	// 3 argumento: Botes (Ex.: 'YES|NO|CANCEL', 'OK|CANCEL', ou qualquer combinao)
	// 4 argumento: cone do alerta
	// 5 argumento: Largura da janela
	// 6 argumento: Altura da janela
	// 7 argumento: Fechar automaticamente quando for clicado em um boto
	// 8 argumento: Posio inicial em Y (top)
	// 9 argumento: Posio inicial em X (left)
	this.show = function() {
		try {
			var obj = document.getElementsByTagName('select');
			for (var i = 0; i < obj.length; i++) obj[i].style.display = "none";
			numTotalSize = 14;
			if (isIE) {
				document.body.scroll="no";
			} else {
				document.body.style.overflow="hidden";
			}
			ps = new pageSize();
			if (blnCreated == false) this.create();
			strMessage = arguments[0];
			if (arguments.length > 1) strTitle = arguments[1];
			if (strTitle != '') {
				if (isIE) {
					$('pubAlertTitleTR'+numThisAlert).style.display = "block";
				} else {
					$('pubAlertTitleTR'+numThisAlert).style.display = "table-row";
				}
				numTotalSize += 24;
			} else {
				$('pubAlertTitleTR'+numThisAlert).style.display = "none";
			}
			arrButtons = new Array();
			if (arguments.length > 2) {
				if (arguments[2] != '') {
					strButtons = arguments[2];
					arrButtons = arguments[2].split('|');
					if (isIE) {
						$('pubAlertButtonsTR'+numThisAlert).style.display = "block";
					} else {
						$('pubAlertButtonsTR'+numThisAlert).style.display = "table-row";
					}
					numTotalSize += 30;
				} else {
					$('pubAlertButtonsTR'+numThisAlert).style.display = "none";
				}
			}
			strIcon = '';
			if (arguments.length > 3) strIcon = arguments[3];
			numAlertWidth = 300;
			if (arguments.length > 4) numAlertWidth = arguments[4];
			numAlertHeight = 60;
			if (arguments.length > 5) numAlertHeight = arguments[5];
			hideOnClick = true;
			if (arguments.length > 6) hideOnClick = arguments[6];
			htmButtons = '';
			numTotalSize += numAlertHeight;
			for (var i = 0; i < arrButtons.length; i++) {
				if (hideOnClick) {
					htmButtons += '<a href="javascript:'+strNameObject+'.onClick(\''+arrButtons[i]+'\');'+strNameObject+'.hide();" id="'+strBtName+numThisAlert+arrButtons[i]+'">';
				} else {
					htmButtons += '<a href="javascript:'+strNameObject+'.onClick(\''+arrButtons[i]+'\');" id="'+strBtName+numThisAlert+arrButtons[i]+'">';
				}
				htmButtons += '<img border=0 vspace=5 hspace=3 src="images/'+arrButtons[i]+'.gif">';
				htmButtons += '</a>';
			}
			var newTop = new Number(0);
			var newLeft = new Number(0);
			newTop = (ps['h'] - numTotalSize - 10) / 2;
			newLeft = (ps['w'] - numAlertWidth - 10) / 2;
			if ((arguments.length > 7) && (arguments[7] != null)) {
				newTop = arguments[7];
			}
			if ((arguments.length > 8) && (arguments[8] != null)) {
				newLeft = arguments[8];
			}
			newTop += ps['sT'];
			if (numOpenedAlerts < 1){
				with ($('div_alert_background'+numThisAlert).style) {
					if (isIE) {
						width = ps['w'] + 17 + "px";
						height = ps['h_'] + 30 + "px";
					} else {
						width = ps['w'] + "px";
						height = ps['h_'] + 17 + "px";
					}
					left = "0px";
					top = "0px";
					display = "block";
				}
				numOpenedFundo = numThisAlert;
			}
			numOpenedAlerts++;
			$('pubAlertButtons'+numThisAlert).innerHTML = htmButtons;
			if (isIE) {
				$('pubAlertTitle'+numThisAlert).innerHTML = '<table width="100%"><tr><td id="pubAlertSpan'+numThisAlert+'" onmousedown="drag(this, \''+'div_alert'+numThisAlert+'\');" width="100%" style="width:100%;cursor:move;font-weight:bold">'+strTitle+'</td></tr></table>';
			} else {
				$('pubAlertTitle'+numThisAlert).innerHTML = strTitle;
			}
			if (strIcon != '') {
				$('pubAlertMessage'+numThisAlert).innerHTML = '<table valign="middle" border=0 cellspacing=5 cellpadding=0><tr><td valign=top><img hspace="3" src="images/'+strIcon+'"></td><td>'+strMessage+'</td></tr></table>';
			} else {
				$('pubAlertMessage'+numThisAlert).innerHTML = strMessage;
			}
			$('pubAlertMessage'+numThisAlert).style.width = numAlertWidth + 'px';
			$('pubAlertMessage'+numThisAlert).style.height = numAlertHeight + 'px';
			$('div_alert'+numThisAlert).style.top = newTop + 'px';
			$('div_alert'+numThisAlert).style.left = newLeft + 'px';
			$('div_alert'+numThisAlert).style.display = 'block';
			$('div_alert'+numThisAlert)._height = numTotalSize;
			$('div_alert'+numThisAlert)._width = numAlertWidth+8;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}

	// Cria o alerta
	this.create = function() {
		try {
			numThisAlert = ++numAlerts;
			this.thisAlert = numThisAlert;
			// Plano de fundo
			var divBg = document.createElement('div');
			divBg.setAttribute('id','div_alert_background'+numThisAlert);
			divBg.style.display = 'none';
			divBg.className = 'div_alert_background';
			
			// Criando tabela em tempo de execuo
			var tbAlert;
			if (!isIE) {
				tbAlert = document.createElement('table');
				tbAlert.setAttribute('id','div_alert'+numThisAlert);
				tbAlert.setAttribute('border','0');
				tbAlert.setAttribute('background','white');
				tbAlert.setAttribute('cellspacing','0');
				tbAlert.setAttribute('cellpadding','0');
			} else {
				document.body.innerHTML += '<table id="div_alert'+numThisAlert+'" cellspacing="0" cellpadding="0"></table>';
				tbAlert = $('div_alert'+numThisAlert);
			}
			tbAlert.style.display = 'none';
			tbAlert.className = 'div_alert';
			
			// Corpo da tabela
			var tbdAlert = document.createElement('tbody');
			tbAlert.appendChild(tbdAlert);
			
			// Inicio
			var trInicio = document.createElement('tr');
			var tdInicio1 = document.createElement('td');
			var tdInicio2 = document.createElement('td');
			var tdInicio3 = document.createElement('td');
			tdInicio2.setAttribute('background','images/cantoSuperiorMeio.gif');
			tdInicio1.innerHTML = '<img src="images/cantoSuperiorEsquerdo.gif">';
			tdInicio2.innerHTML = '';
			tdInicio3.innerHTML = '<img src="images/cantoSuperiorDireito.gif">';
			tbdAlert.appendChild(trInicio);
			trInicio.appendChild(tdInicio1);
			trInicio.appendChild(tdInicio2);
			trInicio.appendChild(tdInicio3);
			
			// Title
			var trTitle = document.createElement('tr');
			trTitle.setAttribute('id','pubAlertTitleTR'+numThisAlert);
			trTitle.style.display = 'none';
			var tdTitle1 = document.createElement('td');
			var tdTitle2 = document.createElement('td');
			var tdTitle3 = document.createElement('td');
			tdTitle2.setAttribute('id','pubAlertTitle'+numThisAlert);
			tdTitle2.setAttribute('background','images/fundoTitulo.gif');
			tdTitle2.style.paddingLeft = '5px';
			tdTitle2.style.fontWeight = 'bold';
			tdTitle2.style.height = '24px';
			tdTitle1.setAttribute('background','images/cantoMeioEsquerdo.gif');
			tdTitle3.setAttribute('background','images/cantoMeioDireito.gif');
			tdTitle1.innerHTML = '&nbsp;';
			tdTitle2.innerHTML = '&nbsp;';
			tdTitle3.innerHTML = '&nbsp;';
			tbdAlert.appendChild(trTitle);
			trTitle.appendChild(tdTitle1);
			trTitle.appendChild(tdTitle2);
			trTitle.appendChild(tdTitle3);
			
			// Conteudo
			var trConteudo = document.createElement('tr');
			var tdConteudo1 = document.createElement('td');
			var tdConteudo2 = document.createElement('td');
			var tdConteudo3 = document.createElement('td');
			tdConteudo2.style.paddingTop = '0px';
			tdConteudo2.style.paddingLeft = '0px';
			tdConteudo2.style.paddingRight = '0px';
			tdConteudo2.style.paddingBottom = '0px';
			tdConteudo2.setAttribute('background','white');
			tdConteudo1.setAttribute('background','images/cantoMeioEsquerdo.gif');
			tdConteudo3.setAttribute('background','images/cantoMeioDireito.gif');
			tdConteudo2.style.height = '20px';
			tdConteudo2.style.width = '20px';
			tdConteudo1.innerHTML = '&nbsp;';
			tdConteudo2.innerHTML = '<div align=center valign=middle style="overflow:auto;vertical-align:middle;" id="pubAlertMessage'+numThisAlert+'"></div>';
			tdConteudo3.innerHTML = '&nbsp;';
			tbdAlert.appendChild(trConteudo);
			trConteudo.appendChild(tdConteudo1);
			trConteudo.appendChild(tdConteudo2);
			trConteudo.appendChild(tdConteudo3);
			
			// Botoes
			var trBotoes = document.createElement('tr');
			trBotoes.setAttribute('id','pubAlertButtonsTR'+numThisAlert);
			trBotoes.style.display = 'none';
			var tdBotoes1 = document.createElement('td');
			var tdBotoes2 = document.createElement('td');
			var tdBotoes3 = document.createElement('td');
			tdBotoes2.setAttribute('id','pubAlertButtons'+numThisAlert);
			tdBotoes1.setAttribute('background','images/cantoMeioEsquerdo.gif');
			tdBotoes3.setAttribute('background','images/cantoMeioDireito.gif');
			tdBotoes2.style.textAlign = 'center';
			tdBotoes2.style.height = '35px';
			tdBotoes1.innerHTML = '&nbsp;';
			tdBotoes2.innerHTML = '&nbsp;';
			tdBotoes3.innerHTML = '&nbsp;';
			tbdAlert.appendChild(trBotoes);
			trBotoes.appendChild(tdBotoes1);
			trBotoes.appendChild(tdBotoes2);
			trBotoes.appendChild(tdBotoes3);
			
			// Final
			var trFinal = document.createElement('tr');
			var tdFinal1 = document.createElement('td');
			var tdFinal2 = document.createElement('td');
			var tdFinal3 = document.createElement('td');
			tdFinal2.setAttribute('background','images/cantoInferiorMeio.gif');
			tdFinal1.innerHTML = '<img src="images/cantoInferiorEsquerdo.gif">';
			tdFinal2.innerHTML = '';
			tdFinal3.innerHTML = '<img src="images/cantoInferiorDireito.gif">';
			tbdAlert.appendChild(trFinal);
			trFinal.appendChild(tdFinal1);
			trFinal.appendChild(tdFinal2);
			trFinal.appendChild(tdFinal3);
			
			document.body.appendChild(divBg);
			document.body.appendChild(tbAlert);
			if (!isIE) dragdrop('pubAlertTitle'+numThisAlert,'div_alert'+numThisAlert);
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}

	// Cria o alerta
	this.hide = function() {
		try {
			with ($('div_alert'+numThisAlert).style) {
				left = "-2000px";
				top = "-2000px";
				display = "none";
			}
			if (numOpenedAlerts < 2) {
				var obj = document.getElementsByTagName('select');
				for (var i = 0; i < obj.length; i++) obj[i].style.display = "block";
				with ($('div_alert_background'+numOpenedFundo).style) {
					width = "0px";
					height = "0px";
					left = "-2000px";
					top = "-2000px";
					display = "none";
				}
				with (document.body) {
					if (isIE) {
						scroll="yes";
					} else {
						style.overflow="auto";
					}
					style.overflowX="hidden";
				}
			}
			numOpenedAlerts--;
		} catch(e) {
			alert((isIE)?e.description:e);
		}
	}

}

nocache = function () {
	var data = new Date();
	return data.getYear()+'.'+(data.getMonth()+1)+'.'+data.getDate()+'.'+data.getHours()+'.'+data.getMinutes()+'.'+data.getSeconds()+'.'+data.getMilliseconds();
};

/* Funo para carregar itens em objetos do tipo select */
selectLoader = function () {
	var xml = new xmlConnection();
	var thisObj = this;
	this.onLoad = function() {
	};
	this.load = function(strObj, strList, strValue, cache) {
		cache = cache || false;
		if (xml.create()) {
			xml.setURL(strList);
			xml.setMethod('GET');
			if (cache != false) {
				xml.add('nocache', nocache());
			}
			xml.onComplete = function() {
				this.setChildName('item');
				$(strObj).innerHTML = '';
				if (this.getCountItens()>1) {
					var linha = document.createElement("OPTION");
					$(strObj).appendChild(linha);
					linha.value = '';
					linha.text = 'Selecione...';
					linha.style.color = '#999999';
					var index = 0;
					for (var j = 0; j<this.getCountItens(); j++) {
						linha = document.createElement("OPTION");
						$(strObj).appendChild(linha);
						linha.value = this.getAttByName('id', j);
						linha.text = this.getAttByName('nome', j);
						if (strValue) {
							if (linha.value == strValue) {
								linha.selected = true;
								index = (j+1);
							}
						}
					}
					$(strObj).selectedIndex = index;
					$(strObj).disabled = false;
				} else if (this.getCountItens() == 1) {
					linha = document.createElement("OPTION");
					$(strObj).appendChild(linha);
					linha.value = this.getAttByName('id', 0);
					linha.text = this.getAttByName('nome', 0);
					linha.selected = true;
					$(strObj).disabled = true;
				} else {
					linha = document.createElement("OPTION");
					$(strObj).appendChild(linha);
					linha.value = '';
					linha.text = 'Sem dados';
					linha.selected = true;
					$(strObj).disabled = true;
				}
				thisObj.onLoad(($(strObj).disabled) ? false : true);
			};
			xml.execute();
		}
	};
};
