function $(id){return document.getElementById(id);}


validaCpf = function(cpf) {
	cpf = cpf.replace(/\./g, "").replace(/\-/g, "");
	var rrValida = new Array(00000000000,11111111111,22222222222,33333333333,44444444444,55555555555,66666666666,77777777777,88888888888,99999999999);
	for(i=0;i<11;i++)if(cpf == rrValida[i])return false;	
    var i;
    var c = cpf.substr(0,9);
    var dv = cpf.substr(9,2);
    var d1 = 0;   
    for (i = 0; i < 9; i++) d1 += c.charAt(i)*(10-i);
    if (d1 == 0) return false;
    d1 = 11 - (d1 % 11);
    if (d1 > 9) d1 = 0;
    if (dv.charAt(0) != d1) return false;
    d1 *= 2;
    for (i = 0; i < 9; i++) d1 += c.charAt(i)*(11-i);
    d1 = 11 - (d1 % 11);
    if (d1 > 9) d1 = 0;
    if (dv.charAt(1) != d1)  return false;
    return true;
}

String.prototype.isWhite = function() {
	if (!isVoid(this)) {
		var value = this.replace(/^\s+/m,'').replace(/\s+$/m,'');
		return (value == '');
	}
	return true;
}

String.prototype.isDate = function() {
	try {
		var arrData = new Array();
		arrData = this.split('/');
		var dia = arrData[0];
		var mes = arrData[1]-1;
		var ano = arrData[2];
		var dataEntrada = (arrData[0]*1)+'/'+(arrData[1]*1)+'/'+arrData[2];
		if (parseInt(ano) < 1920) {
			return null;
		}
		var minhaData = new Date(ano,mes,dia,12,0,0);
		var dataRetorno = minhaData.getDate()+'/'+(minhaData.getMonth()+1)+'/'+minhaData.getFullYear();
		if (dataEntrada == dataRetorno) {
			return true;
		} else {
			return null;
		}
	} catch(e) {
		return null;
	}
}

function isVoid(obj) {
	if (obj != null && typeof obj != 'undefined' && obj != "") return false;
	return true;
}

function verifica_email(str_email){
	if (str_email.search(/^[a-zA-Z0-9]+[_a-zA-Z0-9-]*(\.[_a-z0-9-]+)*@[a-z?G0-9]+(-[a-z?G0-9]+)*(\.[a-z?G0-9-]+)*(\.[a-z]{2,4})$/) != -1) return true;
	else return false;
}

var key = 0;
function soNumeroPress(evento){
	if (key == 0) {
		key = evento.keyCode;	
	}	
	if ((key == 8 || key == 13 || key == 9 || key == 71 || key == 46 || key  == 37  || key  == 39) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
		VerifiqueTAB=true; 
		return true;
	} else{
		return false;
	}
}

function soNumeroDown(evento){
	key = evento.keyCode;
	if ((key == 8 || key == 13 || key == 9 || key == 46) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)){
		VerifiqueTAB=true; 
		return true;
	} else {
		return false;
	}
}

VerifiqueTAB=true;
function Mostra(quem, tammax) {
	if ( (quem.value.length == tammax) && (VerifiqueTAB) ) {
		var i=0,j=0, indice=-1;
		for (i=0; i<document.forms.length; i++) {
			for (j=0; j<document.forms[i].elements.length; j++) {
				if (document.forms[i].elements[j].name == quem.name) {
					indice=i;
					break;
				}
			}
			if (indice != -1)
		         break;
		}
		for (i=0; i<=document.forms[indice].elements.length; i++) {
			if (document.forms[indice].elements[i].name == quem.name) {
				while ( (document.forms[indice].elements[(i+1)].type == "hidden") &&
						(i < document.forms[indice].elements.length) ) {
							i++;
				}
				document.forms[indice].elements[(i+1)].focus();
				VerifiqueTAB=false;
				break;
			}
		}
	}
}


function openFileIFrame(form, url)
{
	var anticache = "?anticache=" + new Date().getMilliseconds() + Math.random();
	var ts = new Date().getTime();
	var frame_name = 'frmupload'+ts;

	if (document.all && !window.opera) {
		//var html = '<iframe name="'+frame_name+'" src="/media/img/blank.gif" style="width:0px;height:0px;visibility:hidden;"></iframe>';
		var html = '<iframe name="'+frame_name+'" src="/media/img/blank.gif" style="width:1000px;height:500px;"></iframe>';
		document.body.insertAdjacentHTML('BeforeEnd', html);
	} else {
		var frame = document.createElement('IFRAME');
		frame.name = frame_name;
		frame.width = 10;
		frame.height = 10;
		frame.style.visibility = 'hidden';
		document.body.appendChild(frame);
	}

	var frm = $(form);
	frm.target = frame_name;
	frm.action = url + anticache;
	frm.submit();
}