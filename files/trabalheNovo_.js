var abaAtual = 1;
function abreAba(q){
	var abaAnt = abaAtual;
	abaAtual = q;

	if(abaAtual==1){
		$('anteriorBtn').disabled = true;
		$('anteriorBtnT').disabled = true;
		$('proximoBtn').disabled = false;
		$('proximoBtnT').disabled = false;
	}
	if(abaAtual>1 && abaAtual<5){
		$('anteriorBtn').disabled = false;
		$('proximoBtn').disabled = false;
		$('anteriorBtnT').disabled = false;
		$('proximoBtnT').disabled = false;
	}
	if(abaAtual==5){
		$('anteriorBtn').disabled = false;
		$('anteriorBtnT').disabled = false;
		$('proximoBtn').disabled = true;
		$('proximoBtnT').disabled = true;
	}

	if (abaAtual==abaAnt) return;
	
	$('aba_'+abaAnt).style.display = 'none';
	$('lbl_aba_'+abaAnt).className = 'abainativa';
	$('aba_'+abaAtual).style.display = 'block';
	$('lbl_aba_'+abaAtual).className = 'abaativa';

	$('qabaT').innerHTML = abaAtual;
	$('qaba').innerHTML = abaAtual;
}
function proximaAba(a){
	var abaAnt = abaAtual;
	if(a=='add')abaAtual++;
	if(a=='del')abaAtual--;

	if(abaAtual==1){
		$('anteriorBtn').disabled = true;
		$('anteriorBtnT').disabled = true;
		abaAtual=1;
	}
	if(abaAtual>1){
		$('anteriorBtn').disabled = false;
		$('anteriorBtnT').disabled = false;
	}
	if(abaAtual==5){
		$('proximoBtn').disabled = true;
		$('proximoBtnT').disabled = true;
		abaAtual=5;
	}
	if(abaAtual<5){
		$('proximoBtn').disabled = false;
		$('proximoBtnT').disabled = false;
	}

	if (abaAtual==abaAnt) return;
	
	$('aba_'+abaAnt).style.display = 'none';
	$('lbl_aba_'+abaAnt).className = 'abainativa';
	$('aba_'+abaAtual).style.display = 'block';
	$('lbl_aba_'+abaAtual).className = 'abaativa';

	$('qabaT').innerHTML = abaAtual;
	$('qaba').innerHTML = abaAtual;
}

var INFORMACAO = {
	'local': 'contentInformacao',
	'template': 'templateInformacao',
	'qtd': 'qtdInformacao',
	'obj': 'objInformacao',
	'bt': 'btRemoverInformacao',
	'shift': function(){
		var id = parseInt($(this.qtd).value);
		if((id-1) <= 1) $S(this.bt).display = "none";
		if(id <= 1) return;
		$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
		$(this.qtd).value = --id;
	},
	'push': function(){
		var template = $(this.template).innerHTML;		
		var qtd = parseInt($(this.qtd).value);
		$(this.local).innerHTML = $(this.local).innerHTML+template.replace(/#n#/g,(++qtd));
		$(this.qtd).value = qtd;
		$S(this.bt).display = "block";
	},
	'reset': function(){
		while($(this.qtd).value>1){
			var id = parseInt($(this.qtd).value);
			if((id-1) <= 1) $S(this.bt).display = "none";
			$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
			$(this.qtd).value = --id;
		}
		return;
	}
}

var TRABALHE = {
	'local': 'contentFormacao',
	'template': 'templateFormacao',
	'qtd': 'qtdFormacao',
	'obj': 'objFormacao',
	'bt': 'btRemoverFormacao',
	'shift': function(){
		var id = parseInt($(this.qtd).value);
		if((id-1) <= 1) $S(this.bt).display = "none";
		if(id <= 1) return;
		$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
		$(this.qtd).value = --id;
	},
	'push': function(){
		var template = $(this.template).innerHTML;		
		var qtd = parseInt($(this.qtd).value);
		$(this.local).innerHTML = $(this.local).innerHTML+template.replace(/#n#/g,(++qtd));
		$(this.qtd).value = qtd;
		$S(this.bt).display = "block";
	},
	'reset': function(){
		while($(this.qtd).value>1){
			var id = parseInt($(this.qtd).value);
			if((id-1) <= 1) $S(this.bt).display = "none";
			$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
			$(this.qtd).value = --id;
		}
		return;
	}
}

var CURSOS = {
	'local': 'contentCursos',
	'template': 'templateCursos',
	'qtd': 'qtdCursos',
	'obj': 'objCurso',
	'bt': 'btRemoverCursos',
	'shift': function(){
		var id = parseInt($(this.qtd).value);
		if((id-1) <= 1) $S(this.bt).display = "none";
		if(id <= 1) return;
		$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
		$(this.qtd).value = --id;
	},
	'push': function(){
		var template = $(this.template).innerHTML;		
		var qtd = parseInt($(this.qtd).value);
		$(this.local).innerHTML = $(this.local).innerHTML+template.replace(/#n#/g,(++qtd));
		$(this.qtd).value = qtd;
		$S(this.bt).display = "block";		
	},
	'reset': function(){
		while($(this.qtd).value>1){
			var id = parseInt($(this.qtd).value);
			if((id-1) <= 1) $S(this.bt).display = "none";
			$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
			$(this.qtd).value = --id;
		}
		return;
	}
}

var EXPERIENCIA = {
	'local': 'contentExperiencia',
	'template': 'templateExperiencia',
	'qtd': 'qtdExperiencia',
	'obj': 'objExperiencia',
	'bt': 'btRemoverExperiencia',
	'shift': function(){
		var id = parseInt($(this.qtd).value);
		if((id-1) <= 1) $S(this.bt).display = "none";
		if(id <= 1) return;
		$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
		$(this.qtd).value = --id;
	},
	'push': function(){
		var template = $(this.template).innerHTML;
		var qtd = parseInt($(this.qtd).value);
		$(this.local).innerHTML = $(this.local).innerHTML+template.replace(/#n#/g,(++qtd));
		$(this.qtd).value = qtd;
		$S(this.bt).display = "block";		
	},
	'reset': function(){
		while($(this.qtd).value>1){
			var id = parseInt($(this.qtd).value);
			if((id-1) <= 1) $S(this.bt).display = "none";
			$(this.obj+""+id).parentNode.removeChild($(this.obj+""+id));
			$(this.qtd).value = --id;
		}
		return;
	}
}

var enviarForm = function(){
	var alt = ($('acao').value=='alt')?true:false;

	if($('cpf').value==""){
		alert("Preencha o CPF.");
		abreAba(1);
		$('cpf').focus();
		return false;
	}

	if($('cpf').value!="" && !validaCpf($('cpf').value)){
		alert("CPF inválido.");
		abreAba(1);
		$('cpf').focus();
		return false;
	}

	/**** nomes ****/
	if($('nomecompleto').value==""){
		alert("Preencha o nome completo.");
		abreAba(2);
		$('nomecompleto').focus();
		return false;
	}
	if($('nomecompleto').value!="" && $('nomecompleto').value.length<=10){
		alert("Preencha corretamente o nome completo.");
		abreAba(2);
		$('nomecompleto').focus();
		return false;
	}
	if($('nomemae').value==""){
		alert("Preencha o nome da mãe.");
		abreAba(2);
		$('nomemae').focus();
		return false;
	}

	/**** nascimento ****/
	if($('nascimento').value==""){
		alert("Preencha a data de nascimento.");
		abreAba(2);
		$('nascimento').focus();
		return false;
	}
	if($('nascimento').value!="" && !$('nascimento').value.isDate()){
		alert("Data de nascimento inválido.");
		abreAba(2);
		$('nascimento').focus();
		return false;
	}

	/**** cidade estado ****/
	if($('cidade').value==""){
		alert("Preencha a cidade.");
		abreAba(2);
		$('cidade').focus();
		return false;
	}
	if($('estado').value==""){
		alert("Preencha o estado.");
		abreAba(2);
		$('estado').focus();
		return false;
	}

	/**** fone ****/
	if($('fonepreferencial').value==""){
		alert("Preencha o telefone preferencial.");
		abreAba(2);
		$('fonepreferencial').focus();
		return false;
	}

	/**** email senha ****/
	if($('email').value==""){
		alert("Preencha o email.");
		abreAba(2);
		$('email').focus();
		return false;
	}
	if (!alt){
		if($('senha').value==""){
			alert("Preencha o senha.");
			abreAba(2);
			$('senha').focus();
			return false;
		}
	}

	if($('expedicao').value!="" && !$('expedicao').value.isDate()){
		alert("Data de expedição de RG inválida.");
		abreAba(1);
		$('expedicao').focus();
		return false;
	}

	var totEx = $('qtdExperiencia').value;
	if (totEx > 0) {
		for(var i=1; i<=parseInt(totEx); i++){
			if($('experienciaAdmissao'+i).value!="" && !$('experienciaAdmissao'+i).value.isDate()){
				alert("Data de admissão inválida.");
				abreAba(4);
				$('experienciaAdmissao'+i).focus();
				return false;
			}
			if($('experienciaDemissao'+i).value!="" && !$('experienciaDemissao'+i).value.isDate()){
				alert("Data de demissão inválida.");
				abreAba(4);
				$('experienciaDemissao'+i).focus();
				return false;
			}
		}
	}
	/*
	if($('rg').value==""){
		alert("Preencha o RG.");
		$('rg').focus();
		return false;
	}
	if($('expedicao').value==""){
		alert("Preencha a expedição.");
		$('expedicao').focus();
		return false;
	}
	if($('cnh').value==""){
		alert("Preencha a cnh.");
		$('cnh').focus();
	}
	if($('categoriacnh').value==""){
		alert("Preencha a categoria da cnh.");
		$('categoriacnh').focus();
	}
	if($('nomepai').value==""){
		alert("Preencha o nome da pai.");
		$('nomepai').focus();
		return false;
	}
	if($('nomepai').value==""){
		alert("Preencha o nome da pai.");
		$('nomepai').focus();
		return false;
	}
	if($('endereco').value==""){
		alert("Preencha o endereço.");
		$('endereco').focus();
		return false;
	}
	if($('bairro').value==""){
		alert("Preencha o bairro.");
		$('bairro').focus();
		return false;
	}
	if($('cep').value==""){
		alert("Preencha o cep.");
		$('cep').focus();
		return false;
	}
	if($('celular').value==""){
		alert("Preencha o telefone celular.");
		$('celular').focus();
		return false;
	}
	// EXPERIENCIA PROFISSIONAL
	if($('experienciaEmpresa1').value==""){
		alert("Preencha ao menos uma experiencia profissional.");
		$('experienciaEmpresa1').focus();
		return false;
	}
	if($('experienciaAdmissao1').value==""){
		alert("Preencha ao menos uma admissão.");
		$('experienciaAdmissao1').focus();
		return false;
	}
	if($('experienciaAdmissao1').value==""){
		alert("Preencha ao menos uma admissão.");
		$('experienciaAdmissao1').focus();
		return false;
	}
	if($('experienciaDemissao1').value==""){
		alert("Preencha ao menos uma demissão.");
		$('experienciaAdmissao1').focus();
		return false;
	}
	if($('experienciaSalario1').value==""){
		alert("Preencha ao menos um salário.");
		$('experienciaSalario1').focus();
		return false;
	}
	*/
	
	var url = 'gravarCurriculo.php';
	if (alt) url= 'editCurriculo.php';

	openFileIFrame('curriculo', url);
	//$('curriculo').submit();
}

window.onload = function(){	
	$('cpf').onkeyup = function(event){ return mask('###.###.###-##',event,this); }
	$('expedicao').onkeyup = function(event){ return mask('##/##/####',event,this); }
	setSomenteNum('cnh','onkeydown');
	$('nascimento').onkeyup = function(event){ return mask('##/##/####',event,this); }
	$('fonepreferencial').onkeyup = function(event){ return mask('(##)####-####',event,this); }
	//$('fonesecundario').onkeyup = function(event){ return mask('(##)####-####',event,this); }
	$('celular').onkeyup = function(event){ return mask('(##)####-####',event,this); }
	$('cep').onkeyup = function(event){ return mask('#####-###',event,this); }
}

function mudaEstado(sigla){
	var ld = new selectLoader();
	ld.load('cidade','cidadesXML.php?sigla='+sigla+'&');
}


function mudaAreaAtuacao(id, i){
	var ld = new selectLoader();
	ld.load('cargopretendido'+i,'cargosXML.php?id='+id+'&');
}

function chkpne(){
	var v = $('pne').value;
	if(v=='S') $('tipopne').disabled=false;
	else $('tipopne').disabled=true;
}

function chkCurComp(n){
	var v = $('cursosCurso'+n).value;
	if(v=='') {
		$('txtOutrosCursos'+n).style.display='none';
		$('txtCursosCurso'+n).style.display='none';
		return;
	}
	if(v=='9999') {
		$('txtOutrosCursos'+n).style.display='block';
		$('txtCursosCurso'+n).style.display='block';
	} else {
		$('txtOutrosCursos'+n).style.display='none';
		$('txtCursosCurso'+n).style.display='block';
	}
}

function chkCurComp2(n){
	var v = $('formacaoCurso'+n).value;
	if(v=='') {
		$('txtOutrosFormacao'+n).style.display='none';
		$('txtFormacaoCurso'+n).style.display='none';
		return;
	}
	if(v=='9998') {
		$('txtOutrosFormacao'+n).style.display='block';
		$('txtFormacaoCurso'+n).style.display='block';
	} else {
		$('txtOutrosFormacao'+n).style.display='none';
		$('txtFormacaoCurso'+n).style.display='block';
	}
}
/*
function chkfa(n){
	var v = $('formacaoCurso'+n).value;
	if(v=='') $('txtFormacaoCurso'+n).style.display='none';
	else $('txtFormacaoCurso'+n).style.display='block';
}

function chkfa2(n){
	var v = $('formacaoCurso'+n).value;
	if(v=='') $('txtFormacaoCurso'+n).style.display='none';
	else $('txtFormacaoCurso'+n).style.display='block';
}
*/
