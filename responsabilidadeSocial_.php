<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div style="width:970px; margin:0 auto;">
    <div style="position:absolute; z-index:999; width:126px; height:126px; padding:15px 0 0 750px; margin:0 auto;"><img class="pngFix" src="img/selo.png" width="126" height="126" alt="80 anos" /></div> <div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td valign="top"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Responsabilidade Social</div>
                                    <p>Estimular  o desenvolvimento das comunidades onde atua é um compromisso do grupo Pedra  Agroindustrial. A conduta ética, a valorização e o fortalecimento da relação  com seus diferentes públicos são pilares da sua atuação.</p>
                                    <p>Para promover o bem-estar e o crescimento das  pessoas, a empresa proporciona um conjunto de programas nas áreas de saúde, educação, cultura, esporte e lazer, que  beneficia o funcionário, sua família e as comunidades. </p>
                                    <p>Todos os funcionários do grupo e seus familiares  têm acesso à assistência médica e hospitalar (Plano de  Saúde), odontológica e farmacêutica.</p>
                                    <p>Através  de investimento em projetos sociais, a empresa marca presença em defesa da  criança e do adolescente. Essa conduta associada a não permissão de trabalho infantil,  faz do grupo Pedra Agroindustrial uma Empresa Amiga da Criança, dede 1999.<br />
                                      <br />
                                      </p>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="29%" valign="top"><table width="190" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                              <tr>
                                                <td height="92" valign="top"><a href="responsabilidadeSocialProgramaViagens.php"><img src="img/responsabilidade1.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                              </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProgramaViagens.php">Programa de Viagens<br />
                                            De malas Prontas</a></span></div></td>
                                          </tr>
                                        </table>                                          </td>
                                        <td width="41%" valign="top"><table width="190" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                  <tr>
                                                    <td height="92" valign="top"><a href="responsabilidadeSocialProgramaSuplementacao.php"><img src="img/responsabilidade4.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                  </tr>
                                              </table></td>
                                            </tr>
                                            <tr>
                                              <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProgramaSuplementacao.php">Programa de Suplementação Alimentar</a></span></div></td>
                                            </tr>
                                          </table>
                                          </td>
                                        <td width="30%" valign="top"><table width="190" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                              <tr>
                                                <td height="92" valign="top"><a href="responsabilidadeSocialProgramaBemViver.php"><img src="img/responsabilidade3.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                              </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProgramaBemViver.php">Programa Bem Viver</a></span></div></td>
                                          </tr>
                                        </table></td>
                                      </tr>
                                      <tr>
                                        <td valign="top"><br />
                                          <table width="190" border="0" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                <tr>
                                                  <td height="92" valign="top"><a href="responsabilidadeSocialProjetoMulher.php"><img src="img/responsabilidade2.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProjetoMulher.php">Projeto Mulher</a></span></div></td>
                                          </tr>
                                        </table></td>
                                        <td valign="top"><br />
                                        <table width="190" border="0" align="center" cellpadding="0" cellspacing="0">
                                              <tr>
                                                <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                  <tr>
                                                    <td height="92" valign="top"><a href="responsabilidadeSocialProgramaSaudeBucal.php"><img src="img/responsabilidade5.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                  </tr>
                                                </table></td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProgramaSaudeBucal.php">Programa de Saúde Bucal</a></span></div></td>
                                              </tr>
                                        </table></td>
                                        <td valign="top"><br />
                                        <table width="190" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                  <tr>
                                                    <td height="92" valign="top"><a href="responsabilidadeSocialProgramaPapaiNoel.php"><img src="img/responsabilidade6.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                  </tr>
                                                </table></td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProgramaPapaiNoel.php">Programa Papai Noel</a></span></div></td>
                                              </tr>
                                        </table></td></tr>
                                      <tr>
                                        <td valign="top"><br />
                                            <table width="190" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                  <tr>
                                                    <td height="92" valign="top"><a href="responsabilidadeSocialProgramaAlonguese.php"><img src="img/responsabilidade7.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                  </tr>
                                                </table></td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialProgramaAlonguese.php">Programa Alongue-se</a></span></div></td>
                                              </tr>
                                          </table></td>
                                        <td valign="top"><br />
                                            <table width="190" border="0" align="center" cellpadding="0" cellspacing="0">
                                              <tr>
                                                <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                  <tr>
                                                    <td height="92" valign="top"><a href="responsabilidadeSocialRelogiodeOuro.php"><img src="img/responsabilidade8.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                  </tr>
                                                </table></td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialRelogiodeOuro.php">Programa Relógio de Ouro</a></span></div></td>
                                              </tr>
                                          </table></td>
                                        <td valign="top"><br />
                                            <table width="190" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                  <tr>
                                                    <td height="92" valign="top"><a href="responsabilidadeSocialApoioCultural.php"><img src="img/responsabilidade9.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                  </tr>
                                                </table></td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="responsabilidadeSocialApoioCultural.php">Apoio a Projetos Culturais</a></span></div></td>
                                              </tr>
                                          </table></td>
                                      </tr>
                                    </table>
                                    <p><br />
                                    </p></td>
                                </tr>
                              
                          </table>
                            <br />
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
