﻿<?
if ($_SESSION['estaLogado'] == true) {
    die("<script>location='oportunidadesCopy.php';</script>");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link href="estilos.css" rel="stylesheet" type="text/css" />
        <title>Pedra Agroindustrial</title>
        <style>
            #aguarde {position:absolute;display:none;width:90px;z-index:998;border:1px solid #CCCCCC;background:white;margin-left:35px;margin-top:20px;}
            #aguarde DIV{position:relative;float:left;line-height:30px;margin-left:5px;height:30px;}
            #aguarde DIV IMG{margin-left:7px;margin-top:7px;}
        </style>
        <script src="files/connecting.js" type="text/javascript"></script>
        <script src="files/commom.js" type="text/javascript"></script>
        <script>
            window.onload = function (){
                var body = document.body,
                html = document.documentElement;

                var height = document.getElementById("login-box").offsetHeight;
                height += 100;
                setInterval(function() {
                    parent.postMessage(height,"https://www.pedraagroindustrial.com.br/");
                },1000);
            };

            function verifica_email(str_email){
                if (str_email.search(/^[a-zA-Z0-9]+[_a-zA-Z0-9-]*(\.[_a-z0-9-]+)*@[a-z?G0-9]+(-[a-z?G0-9]+)*(\.[a-z?G0-9-]+)*(\.[a-z]{2,4})$/) != -1) return true;
                else return false;
            }
            function verifica1(){
                var f = document.f_com;
                var email = f.email;
                if(email.value.replace(/\ /g, "") == "" || !verifica_email(email.value)){
                    alert("Digite o e-mail");
                    email.focus();
                    return true;
                }
                var senha = f.senha;
                if(senha.value.replace(/\ /g, "") == ""){
                    alert("Digite a senha");
                    senha.focus();
                    return true;
                }
                f.submit();
            }
            function verifica2(){
                var f = document.f_sem;
                var email = f.email;
                if(email.value.replace(/\ /g, "") == "" || !verifica_email(email.value)){
                    alert("Digite o e-mail");
                    email.focus();
                    return true;
                }
                f.submit();
            }

            var ESQUECI = {
                'show':function(){

                    $('tab_a').style.display = 'none';
                    $('tab_b').style.display = 'block';
                    $('email').value = '';
                    $('senha').value = '';
                },
                'hide':function(){
                    $('tab_a').style.display = 'block';
                    $('tab_b').style.display = 'none';
                    $('id_username').value = '';
                    $('aguarde').style.display = 'none';
                },
                'send':function(){
                    var erro = '';
                    var myPost = '';
		
                    myPost += 'acao=1&';

                    var f = $('id_username');
                    var v = f.value;
                    if(v.isWhite()){
                        alert("Digite o CPF.");
                        f.focus();
                        return false;
                    }else{
                        myPost += 'cpf='+v+'&';
                    }

                    if (myPost.length>1) { myPost = myPost.substring(0, (myPost.length-1)) }
		
                    $('aguarde').style.display = 'block';
		
                    var myCallback={
                        success:function(o){
                            var xml = new xmlParse();
                            xml.setObjAjax(o);
                            xml.setChildName('contato');
                            xml.parse();

                            $('aguarde').style.display = 'none';
				
                            if (xml.getAttByName('acaoret',0) == 1) {
                                ESQUECI.hide();
                                alert("Cadastro ainda não está liberado para acessar o sistema.");
                                return false;
                            }
                            if (xml.getAttByName('acaoret',0) == 2){
                                ESQUECI.hide();
                                $('erro_email').style.display = "block";
                                return false;
                            }					
                            if (xml.getAttByName('acaoret',0) == 3) {
                                ESQUECI.hide();
                                alert("A senha foi enviada para o e-mail cadastrado.");
                                return false;
                            }
                            if (xml.getAttByName('acaoret',0) == 4) {
                                ESQUECI.hide();
                                alert("Senha não enviada.\nTente novamente mais tarde, por favor.");
                                return false;
                            }
                            if (xml.getAttByName('acaoret',0) == 5) {
                                alert("CNPJ / CPF não encontrado.");
                                $('id_username').focus();
                                return false;
                            }
                        },
                        failure:function(){
                            ESQUECI.hide();
                            alert("Erro ao tentar enviar senha.\nTente novamente mais tarde, por favor.");
                            return false;
                        }
                    };
                    Connect.asyncRequest('POST', "envia_senha.php", myCallback, myPost);
                }
            }
        </script>
    </head>

    <body>
        <div class="login-container">
            <?
            if ($_GET['erro'] == 1) {
                ?>
                <script>alert("E-mail ou senha inválidos!\nTente novamente, por favor.")</script>
                <?
            }
            if ($_GET['erro'] == 2) {
                ?>
                <script>
                    alert("Você já está inscrito neste processo seletivo.");
                    location="oportunidadescopy.php";
                </script>
                <?
            }
            if ($_GET['erro'] == 3) {
                ?>
                <script>
                    alert("O e-mail informado já existe!");
                </script>
                <?
            }
            ?>
            
            <div class="login-box" id="login-box">
                <form action="verLogin.php?acao=login&irpara=<?= $_GET['irpara'] ?>&idp=<?= $_GET['idp'] ?>" name="f_com" method="post">
                    <div id="tab_a">
                        <h1>Já sou cadastrado<p>digite seu e-mail e senha</p></h1>
                        <label for="">E-mail</label>
                        <input name="email" type="text" id="email"/>
                        <label for="">Senha</label>
                        <input name="senha" type="password" id="senha"/>
                        <input type="button" value="Continuar" class="button--green" onclick="verifica1();" />
                        <p><a href="javascript:ESQUECI.show();" class="link">Esqueci minha senha</a></p>
                    </div>

                    <div style="display: none;" id="tab_b">
                        <p>Digite seu CPF (sem pontuação)<br>e clique em continuar.</p>
                        <input name="id_username" type="text" id="id_username" size="25" maxlength="20" />
                        <input type="button" value="Continuar" class="button--green" onclick="ESQUECI.send();"/>
                        <input type="button" value="Cancelar" class="button--green" onclick="ESQUECI.hide();" />
                        <div id="aguarde">
                            <div><img src="img/loading.gif" border=0></div>
                            <div class="textoCinza">Aguarde</div>
                        </div> 
                    </div> 

                </form>
            </div>

            <div class="login-box">
                <form action="enviarCurriculocopy.php?acao=login&irpara=<?= $_GET['irpara'] ?>&idp=<?= $_GET['idp'] ?>" name="f_sem" style="margin:0px;" method="post">
                    <h1>Não sou cadastrado<p>preencha seu e-mail para iniciar o cadastro do seu currículo</p></h1>
                    <label for="">E-mail</label>
                    <input name="email" type="text" id="email" size="25" />
                    <input type="button" value="Continuar" class="button--green" onclick="verifica2();" />
                </form>
            </div>
        </div>
    </body>
</html>
