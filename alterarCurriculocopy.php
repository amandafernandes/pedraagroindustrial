<?
	@session_start();
	if ($_SESSION['estaLogado']!=true){
		?>
		<script>location='index.php';</script>
		<?
		exit;
	}
	require_once("inc/conn.php");
	
	$usr = pg_query("select * from rhcand_info where id_candidato='".$_SESSION['idUserLogado']."'");
	if(pg_num_rows($usr)==0){
		?>
		<script>location='index.php';</script>
		<?
		exit;
	}
		
	$usr = pg_fetch_assoc($usr);
	$_ALTCV = true;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
<script src="files/util.js" type="text/javascript"></script>
<script src="files/trabalheNovocopy.js" type="text/javascript"></script>
<script src="files/ajax.js" type="text/javascript"></script>
<script src="files/commom.js" type="text/javascript"></script>
<script>
    window.onload = function (){
        var body = document.body,
        html = document.documentElement;

        var height = Math.max( body.scrollHeight, body.offsetHeight, 
                    html.clientHeight, html.scrollHeight, html.offsetHeight );
        setInterval(function() {
            parent.postMessage(height,"https://www.pedraagroindustrial.com.br/");
        },1000);
    };
    function msgerro(msg, foco){
    	alert(msg);
    	$(foco).focus();
    }
    function limpaForm(){
    	TRABALHE.reset();
    	CURSOS.reset();
    	EXPERIENCIA.reset();
    	$('curriculo').reset();
    }
    function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
        var sep = 0;
        var key = '';
        var i = j = 0;
        var len = len2 = 0;
        var strCheck = '0123456789';
        var aux = aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;
        if (whichCode == 13) return true;
        key = String.fromCharCode(whichCode); // Valor para o código da Chave
        if (strCheck.indexOf(key) == -1) return false; // Chave inválida
        len = objTextBox.value.length;
        for(i = 0; i < len; i++)
            if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
        aux = '';
        for(; i < len; i++)
            if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
        aux += key;
        len = aux.length;
        if (len == 0) objTextBox.value = '';
        if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
        if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
        if (len > 2) {
            aux2 = '';
            for (j = 0, i = len - 3; i >= 0; i--) {
                if (j == 3) {
                    aux2 += SeparadorMilesimo;
                    j = 0;
                }
                aux2 += aux.charAt(i);
                j++;
            }
            objTextBox.value = '';
            len2 = aux2.length;
            for (i = len2 - 1; i >= 0; i--)
            objTextBox.value += aux2.charAt(i);
            objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
        }
        return false;
    }
</script>
</head>

<body>
<?
if ($_SESSION['estaLogado']==true){
    include("menuRHcopy.php");
}
?>

        <div class="job-form">
         
  
            <p style="margin-bottom: 10px;"> Envie seu currículo:</p>
            <p>Campos com * são obrigatórios.</p>

            <ul class="job-form__tab">
               <li><span id="lbl_aba_1" class="abaativa" onclick="abreAba(1)"><b>1.</b> Documentação</span></li>
               <li><span id="lbl_aba_2" class="abainativa" onclick="abreAba(2)"><b>2.</b> Dados Pessoais</span></li>
               <li><span id="lbl_aba_3" class="abainativa" onclick="abreAba(3)"><b>3.</b> Dados Profissionais</span></li>
               <li><span id="lbl_aba_4" class="abainativa" onclick="abreAba(4)"><b>4.</b> Formação </span></li>
               <li><span id="lbl_aba_5" class="abainativa" onclick="abreAba(5)"><b>5.</b> Anexar currículo</span></li>
            </ul>
            
            <form action="editarCurriculo.php" method="post" name="curriculo" id="curriculo" enctype="multipart/form-data">
                <input type="hidden" value="alt" name="acao" id="acao" />
                <input type="hidden" value="<?= $irpara ?>" name="irpara" id="irpara" />
                <input type="hidden" value="<?= $idp ?>" name="idp" id="idp" />
                <div class="job-form__tab-content">

                <!--TAB - 1 -->                   
                    <div id="aba_1" class="tabContent" style="">
                     <div class="form-group sm">
                        <label for="exampleInputEmail1">*RG:</label>
                        <input name="rg" type="text" class="form-control" id="rg" size="20" value="<?=$usr['numero_rg']?>" />
                        <input name="idcandidato" type="hidden" id="idcandidato" value="<?=$_GET['id']?>" />
                     </div>
                     <div class="form-group sm">
                        <label for="exampleInputEmail1">Data Expedição:</label>
                        <input name="expedicao" type="text" class="form-control" id="expedicao" size="15" value="<?=($usr['dt_expedicao_rg']!='')?dataeuabr($usr['dt_expedicao_rg']):''?>" />
                     </div>
                     <div class="form-group sm">
                        <label for="exampleInputEmail1">*CPF:</label>
                        <input name="cpf" type="text" class="form-control" id="cpf" size="25" value="<?=mascara($usr['cpf'].$usr['digito_cpf'],"###.###.###-##");?>" readonly />
                     </div>
                     <div class="form-group sm">
                        <label for="exampleInputEmail1">CNH:</label>
                        <input name="cnh" type="text" class="form-control" id="cnh" size="25" maxlength="12" value="<?=($usr['numero_cnh']!=0)?$usr['numero_cnh']:''?>" />
                     </div>
                     <div class="form-group xxs">
                        <label for="exampleInputEmail1">Categoria CNH:</label>
                        <input name="categoriacnh" type="text" class="form-control" id="categoriacnh" size="3" maxlength="3" value="<?=$usr['categ_cnh']?>" />
                     </div>
                    </div>
                <!--TAB  - 1 -->

                <!--TAB  - 2 -->
                    <div id="aba_2" class="tabContent" style="display: none;">
                        <div class="form-group md">
                            <label for="exampleInputEmail1">*Nome Completo:</label>
                            <input type="text" name="nomecompleto" type="text" class="form-control" id="nomecompleto" size="60" value="<?= $usr['nome_completo'] ?>" maxlength="40">
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">*Nome Mãe:</label>
                            <input name="nomemae" type="text" class="form-control" id="nomemae" size="60" value="<?=$usr['nome_mae']?>" maxlength="30" />
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">Nome Pai:</label>
                            <input name="nomepai" type="text" class="form-control" id="nomepai" size="60"  value="<?=$usr['nome_pai']?>" maxlength="30" />
                        </div>
                        <div class="form-group sm">
                            <label for="exampleInputEmail1">*Data Nascimento:</label>
                            <input name="nascimento" type="text" class="form-control" id="nascimento" size="15" value="<?=dataeuabr($usr['dt_nascimento'])?>" onkeyup="return mask('##/##/####',event,this);"/>
                        </div>
                        <div class="form-group sm">
                            <label for="exampleInputEmail1">Sexo:</label>
                            <span class="select-container">
                                <select name="sexo" class="form-control" id="sexo">
                                    <option value="M" <?=($usr['sexo']=='M')?'selected="selected"':''?>>Masculino</option>
                                    <option value="F" <?=($usr['sexo']=='F')?'selected="selected"':''?>>Feminino</option>
                                </select>   
                            </span>
                            
                        </div>
                        <div class="form-group sm">
                            <label for="exampleInputEmail1">Estado Cívil:</label>
                            <span class="select-container">
                                <select name="estadocivil" class="form-control" id="estadocivil">
                                    <?
                                        $qryEstadoCivil = pg_query("SELECT * FROM rhestado_civil;");
                                        while($estadoCivil = pg_fetch_assoc($qryEstadoCivil)){?>
                                        <option value="<?=$estadoCivil['id_estado_civil'];?>" <? if($usr['estado_civil']==$estadoCivil['id_estado_civil']) { echo 'selected="selected"'; }?>><?=$estadoCivil['descricao'];?></option>
                                    <? }?>
                                </select>
                            </span>
                        </div>

                        <div class="form-group sm inline">
                            <label for="exampleInputEmail1">Logradouro</label>
                            <span class="select-container">
                                <select name="tplogradouro" class="form-control" id="tplogradouro">
                                <?
                                $qryTpLogradouro = pg_query("SELECT * FROM ustp_logradouro order by descricao asc;");
                                    while($tpLogradouro = pg_fetch_assoc($qryTpLogradouro)){
                                ?>
                                    <option value="<?=$tpLogradouro['id_tp_logradouro'];?>" <?=($tpLogradouro['id_tp_logradouro']==$usr['tp_logradouro'])?'selected':''?>><?=$tpLogradouro['descricao'];?></option>
                                <? 
                                }
                                ?>                                      
                                </select>
                            </span>
                        </div>
                        <div class="form-group sm inline">
                            <label for="exampleInputEmail1">Endereço</label>
                            <input name="endereco" type="text" class="form-control" id="rua" size="40" maxlength="45" value="<?=$usr['endereco']?>" />
                        </div>
                        <div class="form-group xxs inline">
                            <label for="exampleInputEmail1">Número</label>
                            <input name="numero" type="text" class="form-control" id="numero" size="10" maxlength="6" value="<?=$usr['numero']?>" />
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">Complemento:</label>
                            <input name="complemento" type="text" class="form-control" id="complemento" size="30" maxlength="30" value="<?=$usr['complemento']?>" />
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">Bairro:</label>
                            <input name="bairro" type="text" class="form-control" id="bairro" size="60" value="<?=$usr['bairro']?>" maxlength="20" />
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">*Estado:</label>
                            <span class="select-container">
                                <select name="estado" class="form-control" id="estado" onchange="mudaEstado(this.value);">
                                    <option value="">Selecione</option>
                                    <?
                                    $qryEstados = pg_query("SELECT distinct unidade_fed FROM uscidade order by unidade_fed asc;");
                                    while($estados = pg_fetch_assoc($qryEstados)){
                                        ?>
                                        <option value="<?=$estados['unidade_fed'];?>" <? if($usr['estado']==$estados['unidade_fed']){echo 'selected="selected"';} ?>><?=$estados['unidade_fed'];?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </span>
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">*Cidade:</label>
                            <span class="select-container">
                                <select name="cidade" class="form-control" id="cidade">
                                        <?
                                        $qryCidade = pg_query("SELECT * FROM uscidade where unidade_fed='".strtoupper($usr['estado'])."' order by nome asc;");
                                        while($cidade = pg_fetch_assoc($qryCidade)){
                                            ?>
                                            <option value="<?=$cidade['id_cidade'];?>" <? if($usr['cidade']==$cidade['id_cidade']){echo 'selected="selected"';} ?>><?=$cidade['nome'];?></option>
                                            <?
                                        }
                                        ?>
                                </select>
                            </span>
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">Cep:</label>
                            <input name="cep" type="text" class="form-control" id="cep" size="15" value="<?=($usr['cep']!=0)?$usr['cep']:''?>" />
                        </div>
                        <div class="form-group md">
                            <label for="exampleInputEmail1">*Telefone Preferencial:</label>
                            <input name="fonepreferencial" type="text" class="form-control" id="fonepreferencial" size="15" value="<?=mascara($usr['ddd_fone'].$usr['fone'],"(##)####-####")?>" />
                        </div>
                        <div class="form-group md">
                            <label>*Celular:</label>
                            <input name="celular" type="text" class="form-control" id="celular" size="15" value="<?=($usr['celular']!='')?mascara($usr['ddd_celular'].$usr['celular'],"(##)####-####"):''?>" />
                        </div>
                        <div class="form-group md">
                            <label>*E-mail:</label>
                            <input name="email" type="text" class="form-control" id="email" value="<?=$usr['email']?>" size="60" maxlength="100" readonly />
                        </div>
                        <div class="form-group md">
                            <label>*Senha:</label>
                            <input name="senha" type="password" class="form-control" id="senha" size="15" maxlength="10" />

                            <span>Preencha o campo somente se quiser alterar a senha.</span>
                        </div>
                        <div class="form-group md">
                            <label>
                            *Pessoa Portadora de Necessidades Especiais?
                            </label>
                            <span class="select-container">
                                <select name="pne" class="form-control" id="pne" onchange="chkpne();">
                                        <option value="S" <? if($usr['pne']=="S"){echo 'selected="selected"';} ?>>SIM</option>
                                        <option value="N" <? if($usr['pne']=="N"){echo 'selected="selected"';} ?>>NÃO</option>
                                </select>
                            </span>
                        </div>
                        <div class="form-group md">
                            <label>Tipo de Necessidade Especial</label>
                            <span class="select-container">
                                <select name="tipopne" class="form-control" id="tipopne" <?=($usr['pne']=='N')?'disabled':''?>>
                                      <option value="">Selecione</option>
                                      <option value="a" <? if($usr['tipo_pne']=="a"){echo 'selected="selected"';} ?>>Auditiva</option>
                                      <option value="v" <? if($usr['tipo_pne']=="v"){echo 'selected="selected"';} ?>>Visual</option>
                                      <option value="f" <? if($usr['tipo_pne']=="f"){echo 'selected="selected"';} ?>>Física</option>
                                      <option value="m" <? if($usr['tipo_pne']=="m"){echo 'selected="selected"';} ?>>Mental</option>
                                </select>
                            </span>
                        </div>
                    </div>
                <!-- TAB 2 -->

                <!-- TAB 3 -->
                <div id="aba_3" style="display:none;">
                   <?
                      $sql = pg_query("
                          SELECT 
                              * 
                          FROM 
                              rhcand_info_adicional 
                          WHERE 
                              candidato='".$usr['id_candidato']."'
                      ");
                      ?>
                        <input type="hidden" id="qtdInformacao" name="qtdInformacao" value="<?=(pg_num_rows($sql)>0)?pg_num_rows($sql):1?>" />  
                        <div id="templateInformacao" style="display:none;">
                            <div id="objInformacao#n#">
                                <h2>Informação #n#</h2>

                                    <div class="form-group md">
                                        <label for="">*Área de Atuação:</label>
                                        <span class="select-container">
                                            <select name="areaatuacao#n#" class="form-control" id="areaatuacao#n#" onchange="mudaAreaAtuacao(this.value, '#n#');">
                                                <option value="">Selecione</option>
                                                <?$qryAreaAtuacao = pg_query("SELECT * FROM rhtp_area_atuacao_cand order by nm_area_atuacao;");
                                                   while($areaAtuacao = pg_fetch_assoc($qryAreaAtuacao)){?>
                                                <option value="<?=$areaAtuacao['id_tp_area_atuacao_cand'];?>">
                                                   <?=$areaAtuacao['nm_area_atuacao'];?>
                                                </option>
                                                <? }?>
                                            </select>
                                        </span>
                                    </div>

                                    <div class="form-group md">
                                        <label for="">Cargo Pretendido:</label>
                                        <span class="select-container">
                                            <select name="cargopretendido#n#" class="form-control" id="cargopretendido#n#">
                                                <option value="">Selecione a área</option>
                                            </select>
                                        </span>
                                   </div>

                            </div>
                        </div>
                        <div id="contentInformacao" style="overflow:hidden;">
                            <?
                               $i = 0;
                               if(pg_num_rows($sql) >= 1){
                                   while ($arr = pg_fetch_assoc($sql)){
                                       $i++;
                                       ?>
                                <div id="objInformacao<?=$i?>">
                                    <h2>Informação <?=$i?></h2>

                                    <div class="form-group md">
                                        <label for="">*Área de Atuação:</label>
                                        <span class="select-container">
                                         <select name="areaatuacao<?=$i?>" class="form-control" id="areaatuacao<?=$i?>" onchange="mudaAreaAtuacao(this.value, '<?=$i?>');">
                                            <option value="">Selecione</option>
                                            <?
                                               $qryAreaAtuacao = pg_query("SELECT * FROM rhtp_area_atuacao_cand order by nm_area_atuacao;");
                                               while($areaAtuacao = pg_fetch_assoc($qryAreaAtuacao)){?>
                                            <option value="<?=$areaAtuacao['id_tp_area_atuacao_cand'];?>" <? if($arr['area_atuacao']==$areaAtuacao['id_tp_area_atuacao_cand']){
                                               echo 'selected="selected"';} ?>><?=$areaAtuacao['nm_area_atuacao'];?></option>
                                            <? }?>
                                         </select>
                                        </span>
                                    </div>

                                    <div class="form-group md">
                                        <label for="">*Cargo Pretendido:</label>
                                   
                                        <span class="select-container">
                                           <?
                                              $qrycargo =  pg_query("
                                                  SELECT
                                                      public.rharea_atuacao_cargo.area_atuacao,
                                                      public.rhcargo.id_cargo,
                                                      public.rhcargo.descricao
                                                  FROM
                                                      public.rharea_atuacao_cargo
                                                      Inner Join public.rhcargo ON public.rharea_atuacao_cargo.cargo = public.rhcargo.id_cargo
                                                  WHERE public.rharea_atuacao_cargo.area_atuacao='".$arr['area_atuacao']."'
                                                  ORDER BY descricao ASC;
                                              ");
                                              ?>
                                           <select name="cargopretendido<?=$i?>" class="form-control" id="cargopretendido<?=$i?>">
                                           <option value="">Selecione a área</option>
                                           <?
                                              while($cargos = pg_fetch_assoc($qrycargo)){
                                                  ?>
                                           <option value="<?=$cargos['id_cargo']?>" <? if($cargos['id_cargo']==$arr['cargo_pretendido']){echo 'selected="selected"';} ?>><?=$cargos['descricao']?></option>
                                           <?
                                              }
                                              ?>
                                           </select>
                                        </span>
                                   </div>

                                </div>
                                <?
                                   }
                                   }else{
                                   ?>
                                <div id="objInformacao1">
                                    <h2>Informação 1</h2>
                                    <div class="form-group md">
                                        <label for="">*Área de Atuação:</label>
                                      
                                        <span class="select-container">
                                         <select name="areaatuacao1" class="form-control" id="areaatuacao1" onchange="mudaAreaAtuacao(this.value, '1');">
                                            <option value="">Selecione</option>
                                            <?
                                               $qryAreaAtuacao = pg_query("SELECT * FROM rhtp_area_atuacao_cand order by nm_area_atuacao;");
                                                   while($areaAtuacao = pg_fetch_assoc($qryAreaAtuacao)){?>
                                            <option value="<?=$areaAtuacao['id_tp_area_atuacao_cand'];?>"><?=$areaAtuacao['nm_area_atuacao'];?></option>
                                            <? }?>
                                         </select>
                                        </span>
                                    </div>
                                   <div class="form-group md">
                                        <label for="">*Cargo Pretendido:</label>
                                        <span class="select-container">
                                         <select name="cargopretendido1" class="form-control" id="cargopretendido1">
                                            <option value="">Selecione a área</option>
                                         </select>
                                        </span>
                                   </div>
                                </div>
                                <?
                                   }
                                   ?>
                            </div>

                            <div class="btn-add">
                                <input type="button" name="button" id="btAddInformacao" value="Adicionar Informação" class="btn"  onclick="INFORMACAO.push();" />
                                <input type="button" name="button" id="btRemoverInformacao" value="Retirar Informação" class="btn" style="display:none;" onclick="INFORMACAO.shift();" />                       
                            </div>
   
                            <div class="form-group sm">
                                <label for="atualmente">Atualmente esta trabalhando?</label>
                                <span class="select-container">
                                    <select name="trabalhando" class="form-control" id="trabalhando">
                                        <option value="S" <? if($usr['fg_esta_trabalhando']=="S"){echo 'selected="selected"';} ?>>SIM</option>
                                        <option value="N" <? if($usr['fg_esta_trabalhando']=="N"){echo 'selected="selected"';} ?>>NÃO</option>
                                    </select>
                                </span>
                            </div>
                            <div class="form-group sm">
                                <label for="">Você já trabalhou em alguma empresa do grupo?</label>
                                <span class="select-container">
                                    <select name="trabalhounogrupo" class="form-control" id="trabalhounogrupo">
                                        <option value="S" <? if($usr['fg_trabalhou_emp_grupo']=="S"){echo 'selected="selected"';} ?>>SIM</option>
                                        <option value="N" <? if($usr['fg_trabalhou_emp_grupo']=="N"){echo 'selected="selected"';} ?>>NÃO</option>
                                    </select>
                                </span>
                            </div>
                            <div class="form-group sm">
                                <label for="">Aceita transferência para outra cidade?</label>
                                <span class="select-container">
                                    <select name="transferenciacidade" class="form-control" id="transferenciacidade">
                                        <option value="S" <? if($usr['fg_aceita_transf']=="S"){echo 'selected="selected"';} ?>>SIM</option>
                                        <option value="N" <? if($usr['fg_aceita_transf']=="N"){echo 'selected="selected"';} ?>>NÃO</option>
                                    </select>
                                </span>
                            </div>
                            <div class="form-group sm">
                                <label for="">Aceita revezamento de turno?</label>
                                <span class="select-container">
                                    <select name="revezamento" class="form-control" id="revezamento">
                                        <option value="S" <? if($usr['revezamento_turno']=="S"){echo 'selected="selected"';} ?>>SIM</option>
                                        <option value="N" <? if($usr['revezamento_turno']=="N"){echo 'selected="selected"';} ?>>NÃO</option>
                                    </select>
                                </span>
                            </div>

                        </div>


                <!-- TAB 3 -->

            <!-- TAB 4 -->
            <div id="aba_4" style="display:none;" class="tabContent">
                
                <h2>Formação Acadêmica </h2>                  
                <div class="form-group md">
                    <label for="">*Grau de instrução</label>
                    <span class="select-container">
                        <select name="formacaoNivelAcademico" class="form-control" id="formacaoNivelAcademico">
                            <option value="">Selecione o grau de instrução</option>
                            <?
                            $qtyGrauInstrucao = pg_query("SELECT * FROM rhgrau_instrucao;");
                            while($grauInstrucao = pg_fetch_assoc($qtyGrauInstrucao)){?>
                            <option value="<?=$grauInstrucao['id_grau_instrucao'];?>" <? if($usr['grau_instrucao']==$grauInstrucao['id_grau_instrucao']){echo 'selected="selected"';} ?>><?=$grauInstrucao['descricao'];?></option>
                            <? }?>
                        </select>
                    </span>
                </div>

                <?
                    $sql = pg_query("
                        SELECT
                            rhcand_curso.curso,
                            rhcand_curso.fg_status,
                            rhcand_curso.descricao
                        FROM
                            rhcurso
                            Inner Join rhcand_curso ON rhcand_curso.curso = rhcurso.id_curso
                        WHERE
                            (rhcurso.tp_curso = '1' OR rhcurso.tp_curso = '2' OR rhcurso.id_curso = 9998) AND
                            candidato='".$usr['id_candidato']."'
                    ");
                ?>                                      
                <input type="hidden" id="qtdFormacao" name="qtdFormacao" value="<?=(pg_num_rows($sql)>0)?pg_num_rows($sql):1?>" />
                <div id="templateFormacao" style="display:none;">
                    <div id="objFormacao#n#">
                        <h3>Formação #n#</h3>
                        
                        <div class="form-group md">
                            <label for="">Curso:</label>

                            <span class="select-container">
                                <select name="formacaoCurso#n#" class="form-control" id="formacaoCurso#n#" onchange="chkCurComp2('#n#')">
                                    <option value="">Selecione</option>
                                    <? $qryFormacaoCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso='1' OR tp_curso='2' ORDER BY descricao ASC;"); 
                                    while($formacaoCurso = pg_fetch_assoc($qryFormacaoCurso)){ ?>
                                        <option value="<?=$formacaoCurso['id_curso'];?>"><?=$formacaoCurso['descricao'];?></option><? }?>
                                        <option value="9998">OUTROS</option>
                                </select>
                            </span>
                        </div>

                        
                          
                        <div id="txtFormacaoCurso#n#" style="display:none;">
                            <div class="form-group md">
                                <label for="">Situação:</label>
                                <span class="select-container">
                                    <select name="formacaoComplemento#n#" class="form-control" id="formacaoComplemento#n#">
                                        <option value="c">Completo</option>
                                        <option value="i">Incompleto</option><option value="a">Andamento</option>
                                    </select>
                                </span>
                            </div>
                        </div>
                         
                        

        
                        <div id="txtOutrosFormacao#n#" style="display:none;">
                            <div class="form-group md">
                                <label for="">Outros Cursos:</label>
                                <span class="select-container">
                                    <input name="outrosFormacaoCurso#n#" type="text" class="form-control" id="outrosFormacaoCurso#n#" size="60" maxlength="40" />
                                </span>
                            </div>
                        </div>
                            

                    </div>
                </div>

                <div id="contentFormacao" style="overflow:hidden;">
                         <?  
                         $i = 0;
                         if(pg_num_rows($sql) >= 1){
                            while($arr = pg_fetch_assoc($sql)) { 
                                $i++;
                         ?>

                            <div id="objFormacao<?=$i?>">
                                <h3>Formação <?=$i?></h3>
                                <div class="form-group md">
                                    <label for="">Curso:</label>
                                    
                                    <span class="select-container">
                                        <select name="formacaoCurso<?=$i?>" class="form-control" id="formacaoCurso<?=$i?>" onchange="chkCurComp2('<?=$i?>')">
                                            <option value="">Selecione</option>
                                            <?
                                            $qryFormacaoCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso='1' OR tp_curso='2' ORDER BY descricao ASC;");
                                            while($formacaoCurso = pg_fetch_assoc($qryFormacaoCurso)){
                                                ?>
                                                <option value="<?=$formacaoCurso['id_curso'];?>"  <? if($formacaoCurso['id_curso']==$arr['curso']){echo 'selected="selected"';} ?>><?=$formacaoCurso['descricao'];?></option>
                                                <?
                                            }
                                            ?>
                                            <option value="9998" <? if($arr['curso']=='9998'){echo 'selected="selected"';} ?>>OUTROS</option>
                                        </select>
                                    </span>
                                </div>
                                


                                <div id="txtFormacaoCurso<?=$i?>" style="display:;">
                                    <div class="form-group md">
                                        <label for="">Situação:</label>
                                        
                                        <span class="select-container">
                                            <select name="formacaoComplemento<?=$i?>" class="form-control" id="formacaoComplemento<?=$i?>">
                                            <option value="c" <? if($arr['fg_status']=="c"){echo 'selected="selected"';} ?>>Completo</option>
                                            <option value="i" <? if($arr['fg_status']=="i"){echo 'selected="selected"';} ?>>Incompleto</option>
                                            <option value="a" <? if($arr['fg_status']=="a"){echo 'selected="selected"';} ?>>Andamento</option>
                                            </select>
                                        </span>
                                        
                                    </div>
                                    
                                </div>
                                   

                        
                                <div id="txtOutrosFormacao<?=$i?>" style="display:<?=($arr['curso']=='9998')?'block':'none'?>;">
                                    <div class="form-group md">
                                        <label for="">Outros Cursos:</label>
                                         <input name="outrosFormacaoCurso<?=$i?>" type="text" class="form-control" id="outrosFormacaoCurso<?=$i?>" size="60" maxlength="40" value="<?=$arr['descricao']?>" />
                                    </div>
                                </div>
                                    

                            
                            </div>
                                <? 
                                    }
                                    } else {
                                ?>

                                <div id="objFormacao1">
                                    <h3>Formação <?=$i?>Formação 1</h3>
                                    
                                    <div class="form-group md">
                                        <label for="">Curso:</label>
                                        
                                        <span class="select-container">
                                            <select name="formacaoCurso1" class="form-control" id="formacaoCurso1" onchange="chkCurComp2('1')">
                                                <option value="">Selecione</option>
                                                <?
                                                $qryFormacaoCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso='1' OR tp_curso='2' ORDER BY descricao ASC;");
                                                while($formacaoCurso = pg_fetch_assoc($qryFormacaoCurso)){?>
                                                <option value="<?=$formacaoCurso['id_curso'];?>"><?=$formacaoCurso['descricao'];?></option>
                                                <? }?>
                                                <option value="9998">OUTROS</option>
                                            </select>
                                        </span>
                                    </div>

                                    <div id="txtFormacaoCurso1" style="display:none;">
                                        <div class="form-group md">
                                            <label for="">Situação:</label>
                                             
                                             <span class="select-container">
                                                <select name="formacaoComplemento1" class="form-control" id="formacaoComplemento1">
                                                    <option value="c">Completo</option>
                                                    <option value="i">Incompleto</option>
                                                    <option value="a">Andamento</option>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                        

                               
                                    <div id="txtOutrosFormacao1" style="display:none;">
                                        <div class="form-group md">
                                            <label for="">Outros Cursos:</label>

                                                 
                                            <input name="outrosFormacaoCurso1" type="text" class="form-control" id="outrosFormacaoCurso1" size="60" maxlength="40" />
                                            
                                        </div>
                                    </div>
                                        

                                </div>
                                <?
                            }
                            ?>
            </div>

            <div class="btn-add">
                <input type="button" name="button" id="btAddFormacao" value="Adicionar Formação" class="btn" onclick="TRABALHE.push();" />
                <input type="button" name="button" id="btRemoverFormacao" value="Retirar Formação" class="btn" style=" display:<?=($i>1)?'block':'none'?>;" onclick="TRABALHE.shift();" />                     
            </div>
           
            <h2>CURSOS COMPLEMENTARES (TREINAMENTOS, IDIOMAS, INFORMÁTICA...)</h2>    

            
                <?
                    $sql = pg_query("
                        SELECT
                            rhcand_curso.curso,
                            rhcand_curso.fg_status,
                            rhcand_curso.descricao
                        FROM
                            rhcurso
                            Inner Join rhcand_curso ON rhcand_curso.curso = rhcurso.id_curso
                        WHERE
                            ((rhcurso.tp_curso != '1' AND rhcurso.tp_curso != '2') OR rhcurso.id_curso = 9999) AND
                            candidato='".$usr['id_candidato']."'
                    ");
                ?>
                <input type="hidden" id="qtdCursos" name="qtdCursos" value="<?=(pg_num_rows($sql)>0)?pg_num_rows($sql):1?>" />
                <div id="templateCursos" style="display:none;">
                    <div id="objCurso#n#" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <h3>Curso #n#</h3>
                        <div class="form-group md">
                            <label for="">Curso:</label>
                            <span class="select-container">
                                <select name="cursosCurso#n#" class="form-control" id="cursosCurso#n#" onchange="chkCurComp('#n#')">
                                    <option value="">Selecione</option>
                                    <? $qryCursosCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso!='1' AND tp_curso!='2' ORDER BY descricao ASC;"); while($cursosCurso = pg_fetch_assoc($qryCursosCurso)){?>
                                    <option value="<?=$cursosCurso['id_curso'];?>"><?=$cursosCurso['descricao'];?></option><? } ?>
                                    <option value="9999">OUTROS</option>
                                </select>
                            </span>
                        </div>

                        <div id="txtCursosCurso#n#" style="display:none;">
                            <div class="form-group md">
                                <label for="">Situação:</label>
                                <span class="select-container">
                                    <select name="cursosCompleto#n#" class="form-control" id="cursosCompleto#n#">
                                        <option value="c">Completo</option><option value="i">Incompleto</option>
                                        <option value="a">Andamento</option>
                                    </select>
                                </span>
                            </div>
                        </div>
                                   
                        <div id="txtOutrosCursos#n#" style="display:none;">
                            <div class="form-group md">
                                <label for="">Outros Cursos:</label>                               
                                <input name="outrosCursos#n#" type="text" class="form-control" id="outrosCursos#n#" size="60" maxlength="40" />                               
                            </div>
                        </div>
                               
                    </div>
                </div>

                <div id="contentCursos" style="overflow:hidden;">
                <?
                $i = 0;
                if(pg_num_rows($sql) >= 1){
                    while($arr = pg_fetch_assoc($sql)) {
                        $i++;
                        ?>

                        <div id="objCurso<?=$i?>" cellpadding="0" cellspacing="0" border="0" width="100%">
                            <h3>Curso <?=$i?></h3>
                            
                            <div class="form-group md">
                                <label for="">Curso:</label>
                              
                              <span class="select-container">
                                <select name="cursosCurso<?=$i?>" class="form-control" id="formacaoCurso<?=$i?>">
                                  <option value="">Selecione</option>
                                    <?
                                    $qryCursosCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso!='1' AND tp_curso!='2' ORDER BY descricao ASC;");
                                    while($cursosCurso = pg_fetch_assoc($qryCursosCurso)){?>
                                    <option value="<?=$cursosCurso['id_curso'];?>" <? if($cursosCurso['id_curso']==$arr['curso']){echo 'selected="selected"';} ?>><?=$cursosCurso['descricao'];?></option>
                                    <? }?>
                                    <option value="9999" <? if($arr['curso']=='9999'){echo 'selected="selected"';} ?>>OUTROS</option>
                                    </select>
                                </span>

                            </div>
                            
                            <div id="txtCursosCurso<?=$i?>" style="display:;">
                                <div class="form-group md">
                                   
                                    <label>Situação:</label>
                                    <span class="select-container">
                                        <select name="cursosCompleto<?=$i?>" class="form-control" id="formacaoCurso<?=$i?>">
                                            <option value="c" <? if($arr['fg_status']=='c'){echo 'selected="selected"';} ?>>Completo</option>
                                            <option value="i" <? if($arr['fg_status']=='i'){echo 'selected="selected"';} ?>>Incompleto</option>
                                            <option value="a" <? if($arr['fg_status']=='a'){echo 'selected="selected"';} ?>>Andamento</option>
                                        </select>
                                    </span>
                                    
                                </div>
                            </div>
                                
                            
                            <div id="txtOutrosCursos<?=$i?>" style="display:<?=($arr['curso']=='9999')?'block':'none'?>;">
                                <div class="form-group md">
                                    <label>Outros Cursos:</label>
                                 
                                    <input name="outrosCursos<?=$i?>" type="text" class="form-control" id="outrosCursos<?=$i?>" size="60" maxlength="40" value="<?=$arr['descricao']?>" />
                                </div>
                            </div>
                                
                                                            
                        </div>
                            <? 
                            }
                        } else {
                            ?>
                            <div id="objCurso1">
                                <h3>Curso 1</h3>
                                
                                <div class="form-group md">
                                    <label for="">Curso:</label>
                                    
                                    <span class="select-container">
                                    <select name="cursosCurso1" class="form-control" id="cursosCurso1" onchange="chkCurComp('1')">
                                    <option value="">Selecione</option>
                                    <?
                                    $qryCursosCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso>'2' ORDER BY descricao ASC;");
                                    while($cursosCurso = pg_fetch_assoc($qryCursosCurso)){?>
                                    <option value="<?=$cursosCurso['id_curso'];?>"><?=$cursosCurso['descricao'];?></option>
                                    <? }?>
                                    <option value="9999">OUTROS</option>
                                    </select>
                                    </span>
                                </div>

                                
                                <div id="txtCursosCurso1" style="display:none;">
                                    <div class="form-group md">
                                        <label for="">Situação:</label>
                                     
                                        <span class="select-container">
                                            <select name="cursosCompleto1" class="form-control" id="cursosCompleto1">
                                                <option value="c">Completo</option>
                                                <option value="i">Incompleto</option>
                                                <option value="a">Andamento</option>
                                            </select>
                                        </span>
                                
                                    </div>
                                </div>
                                   

                               
                                <div id="txtOutrosCursos1" style="display:none;">
                                    <div class="form-group md">
                                        <label for="">Outros Cursos:</label>                                     
                                        <input name="outrosCursos1" type="text" class="form-control" id="outrosCursos1" size="60" maxlength="40" />
                                    </div>
                                </div>

                            </div>
                                <?
                            }
                            ?>
                </div>
                <div class="btn-add">
                    <input type="button" name="button" id="btAddCursos" value="Adicionar Curso" class="btn" onclick="CURSOS.push();"  />
                    <input type="button" name="button" id="btRemoverCursos" value="Retirar Curso" class="btn" style="display:<?=($i>1)?'block':'none'?>;" onclick="CURSOS.shift();" />                   
                </div>

                <h2>EXPERIÊNCIA PROFISSIONAL</h2>
                  
                
                   <?
                    $sql = pg_query("SELECT * FROM rhcand_experiencia where candidato='".$usr['id_candidato']."'");
                   ?>
                   <input type="hidden" id="qtdExperiencia" name="qtdExperiencia" value="<?=(pg_num_rows($sql)>0)?pg_num_rows($sql):1?>" />
                    <div id="templateExperiencia" style="display:none;">
                        <div id="objExperiencia#n#">
                            <h3>Experiência #n#</h3>
                            <div class="form-group md">
                                <label for="">Nome da Empresa:</label>
                                 <input name="experienciaEmpresa#n#" type="text" class="form-control" id="experienciaEmpresa#n#" size="60" maxlength="40" />
                            </div>
                            <div class="form-group md">
                                <label for="">Admissão:</label>
                                <input name="experienciaAdmissao#n#" type="text" class="form-control" id="experienciaAdmissao#n#" size="15" onkeyup="return mask('##/##/####',event,this);" />  
                            </div>

                            <div class="form-group md">
                                <label for="">Demissão:</label>
                                <input name="experienciaDemissao#n#" type="text" class="form-control" id="experienciaDemissao#n#" size="15" onkeyup="return mask('##/##/####',event,this);" />
                            </div>

                            <div class="form-group md">
                                <label for="">Último Cargo:</label>                                
                                <span class="select-container">
                                    <select name="experienciaCargo#n#" class="form-control" id="experienciaCargo#n#">
                                        <option value="">Selecione</option><? $qryCargos = pg_query("SELECT * FROM rhcargo order by descricao;"); while($cargo = pg_fetch_assoc($qryCargos)){ ?><option value="<?=$cargo['id_cargo'];?>"><?=$cargo['descricao'];?></option><? }?>
                                    </select>
                                </span>
                            </div>

                            <div class="form-group md">
                                <label for="">Último Salário:</label>
                                <input name="experienciaSalario#n#" type="text" class="form-control" id="experienciaSalario#n#" size="15" onkeypress="return(MascaraMoeda(this,'.',',',event))" maxlength="8" />
                            </div>
                            <div class="form-group md">
                                <label for="">Trabalhos Desenvolvidos:</label>
                                <textarea cols=100 name="experienciaTrabalhos#n#" class="form-control" id="experienciaTrabalhos#n#" style="height:50px;"></textarea>                       
                            </div>
                        </div>
                    </div>


                    <div id="contentExperiencia" style="overflow:hidden;">
                     <? 
                     $i = 0;
                     if(pg_num_rows($sql) >= 1){
                         while($arr = pg_fetch_assoc($sql)) { 
                            $i++;
                             ?>

                            <div id="objExperiencia<?=$i?>">
                                <h2>Experiência <?=$i?></h2>
                               
                                <div class="form-group md">
                                    <label for="">Nome da Empresa:</label>                             
                                    <input name="experienciaEmpresa<?=$i?>" type="text" class="form-control" id="experienciaEmpresa<?=$i?>" size="60" value="<?=$arr['nome_empresa']?>" />
                                </div>
                                <div class="form-group md">
                                    <label for="">Admissão:</label>
                                    <input name="experienciaAdmissao<?=$i?>" type="text" class="form-control" id="experienciaAdmissao<?=$i?>" size="15" onkeyup="return mask('##/##/####',event,this);" value="<?=($arr['dt_admissao']!='')?dataeuabr($arr['dt_admissao']):''?>" />
                                </div>

                                <div class="form-group md">
                                    <label for="">Demissão:</label>
                                    <input name="experienciaDemissao<?=$i?>" type="text" class="form-control" id="experienciaDemissao<?=$i?>" size="15" onkeyup="return mask('##/##/####',event,this);" value="<?=($arr['dt_demissao']!='')?dataeuabr($arr['dt_demissao']):''?>" />
                                </div>

                                <div class="form-group md">
                                    <label for="">Último Cargo:</label>
                                    
                                    <span class="select-container">
                                        <select name="experienciaCargo<?=$i?>" class="form-control" id="experienciaCargo<?=$i?>">
                                             <option value="">Selecione</option>
                                               <?
                                                $qryCargos = pg_query("SELECT * FROM rhcargo;");
                                                while($cargo = pg_fetch_assoc($qryCargos)){?>
                                                <option value="<?=$cargo['id_cargo'];?>" <? if($arr['ult_cargo']==$cargo['id_cargo']){echo 'selected="selected"'; }?> ><?=$cargo['descricao'];?></option>
                                                <? }?>
                                        </select>
                                    </span>
                                </div>

                                <div class="form-group md">
                                    <label for="">Último Salário:</label>
                                    <input name="experienciaSalario<?=$i?>" type="text" class="form-control" id="experienciaSalario<?=$i?>" size="15" onkeypress="return(MascaraMoeda(this,'.',',',event))" value="<?=($arr['ultimo_salario']!='')?number_format($arr['ultimo_salario'], 2, ",", "."):''?>" />
                                </div>
                                
                                <div class="form-group md">
                                    <label for="">Trabalhos Desenvolvidos:</label>
                                    <textarea cols=100 name="experienciaTrabalhos<?=$i?>" class="form-control" id="experienciaTrabalhos<?=$i?>" style="height:50px;"><?=$arr['trab_desenvolvido']?></textarea>
                                </div>
                            
                            </div>
                        <?
                        }
                     }else{
                         ?>
                            <div id="objExperiencia1">
                                <h3>Experiência 1</h3>
                                <div class="form-group md">
                                    <label for="">Nome da Empresa:</label>
                                    <input name="experienciaEmpresa1" type="text" class="form-control" id="experienciaEmpresa1" size="60" maxlength="40" />
                                </div>
                                           

                                <div class="form-group md">
                                    <label for="">Admissão:</label>
                                    <input name="experienciaAdmissao1" type="text" class="form-control" id="experienciaAdmissao1" size="15" onkeyup="return mask('##/##/####',event,this);" />
                                </div> 
                                

                                <div class="form-group md">
                                    <label for="">Demissão:</label>
                                    <input name="experienciaDemissao1" type="text" class="form-control" id="experienciaDemissao1" size="15" onkeyup="return mask('##/##/####',event,this);" />
                                </div>

                                <div class="form-group md">
                                    <label for="">Último Cargo:</label>
                                    
                                    <span class="select-container">
                                        <select name="experienciaCargo1" class="form-control" id="experienciaCargo1">
                                        <option value="">Selecione</option>
                                        <?
                                        $qryCargos = pg_query("SELECT * FROM rhcargo order by descricao;");
                                        while($cargo = pg_fetch_assoc($qryCargos)){?>
                                        <option value="<?=$cargo['id_cargo'];?>"><?=$cargo['descricao'];?></option>
                                        <? }?>
                                        </select>
                                    </span>
                                </div>
                                <div class="form-group md">
                                    <label for="">Último Salário:</label>
                                    <input name="experienciaSalario1" type="text" class="form-control" id="experienciaSalario1" size="15" onkeypress="return(MascaraMoeda(this,'.',',',event))" maxlength="8" />
                                </div>
                                <div class="form-group md">
                                    <label for="">Trabalhos Desenvolvidos:</label>
                                    <textarea cols=100 name="experienciaTrabalhos1" class="form-control" id="experienciaTrabalhos1" style="height:50px;"></textarea>
                                </div>
                                                  
                                </div>
                             <?
                         }
                            ?>
                    </div>
                    
                    <div class="btn-add">
                        <input type="button" name="button" id="btAddExperiencia" value="Adicionar Experiência" class="btn" onclick="EXPERIENCIA.push();" />
                        <input type="button" name="button" id="btRemoverExperiencia" value="Retirar Experiência" class="btn" style="display:none;" onclick="EXPERIENCIA.shift();" />
                   </div>
                      
                    <div class="form-group md">
                        <h2>DADOS COMPLEMENTARES</h2> 
                        <?
                            $sql = pg_query("SELECT * FROM rhcand_dados_compl where candidato='".$usr['id_candidato']."'");
                            $dados = pg_fetch_assoc($sql);
                        ?>  
                        <textarea name="complementares" cols="100" rows="10" class="form-controlTextArea" id="complementares"><?=$dados['dados_complementares']?></textarea>
                    </div>   
                    
            </div>
            <!-- TAB 4 -->

            <!-- TAB 5 -->    
            <div id="aba_5" style="display:none;">
                <div class="form-group md">
                    <table>
                        <tr>
                        <td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin:8px;">Anexe seu currículo em formato .DOC ou .PDF</div></td>
                        </tr>

                        <tr>
                        <td height="30" colspan="2"><div align="left"><i>Selecione um arquivo somente se desejar alterar o existente</i></div></td>
                        </tr>                             
                        <?
                        $arq = '';
                        $isarq = false;
                        $cpf = $usr['cpf'].$usr['digito_cpf'];
                        $extval = array('pdf', 'doc');
                        for ($t=0; $t<count($extval); $t++){
                        $arq = 'curriculos/'.$cpf.".".$extval[$t];
                        if (is_file($arq)) {
                        $isarq = true;                                          
                        break;
                        }
                        }

                        if($isarq){
                        ?>
                        <tr>
                        <td height="30" colspan="2"><div align="left"><i>Arquivo atual:</i> <A href="<?=$arq?>" target="_blank"><?=$arq?></a></div></td>
                        </tr>
                        <?
                        } else {
                        ?>
                        <tr>
                        <td height="30" colspan="2"><div align="left"><i>Nenhum arquivo até o momento</i></div></td>
                        </tr>
                        <?
                        }
                        ?>
                        <tr>
                        <td width="16%" height="25"><div align="right">Currículo:</div></td>
                        <td width="84%" height="30" style="padding-left:6px;"><input name="arquivo" type="file" class="formularioPadrao" id="arquivo"/></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--TAB 5 -->


        </form>


        </div>

            <div class="job-form__btn">
                <p>Passo:&nbsp;<span id="qaba">1</span> / 5&nbsp;&nbsp;&nbsp;</p><br/>
                <div class="job-form__next">
                    <span class="btn" onclick="proximaAba('add');" name="proximoBtn" id="proximoBtn">
                        <span class="btn-text">Avançar</span>
                        <span class="btn-arrow">
                        <i class="icon icon-svg_arrow-right"></i>
                        </span>
                    </span>

                    <button class="btn disabled" onclick="enviarForm();" name="enviaBtn" id="enviaBtn">
                        <span class="btn-text">Finalizar</span>
                        <span class="btn-arrow">
                        <i class="icon icon-svg_arrow-right"></i>
                        </span>
                    </button>
                </div>
                <div class="job-form__back">
                    <span class="btn btn-back disabled" onclick="proximaAba('del');" name="anteriorBtn" id="anteriorBtn">
                        <span class="btn-arrow">
                        <i class="icon icon-svg_arrow-left"></i>
                        </span>
                        <span class="btn-text">Voltar</span>
                    </span>
                </div>
            </div>

     </div>  


</body>
</html>
