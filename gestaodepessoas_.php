﻿<?require_once("inc/conn.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div style="width:970px; margin:0 auto;">
    <div style="position:absolute; z-index:999; width:126px; height:126px; padding:15px 0 0 750px; margin:0 auto;"><img class="pngFix" src="img/selo.png" width="126" height="126" alt="80 anos" /></div> <div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td valign="top"><span style="font-size:18px; font-weight:normal; color:#566336;">Gestão de Pessoas</span>
<?
if ($_SESSION['estaLogado']==true){
	include("menuRH.php");
}
?>

<Br><br>
                                
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="65%" valign="top"><p>Valorizar e fortalecer a relação com seus funcionários são premissas para o bom desempenho empresarial e é o compromisso da Pedra Agroindustrial.</p>
                                          <p><br />
                                          </p>
                                          <p> O grupo acredita na força de uma equipe qualificada e comprometida, por isso, oferece oportunidade de crescimento profissional através de progressão na carreira e benefícios educacionais.<br />
                                          	<br />
                                          </p>
                                          <p>Os treinamentos são executados visando o aprimoramento e desenvolvimento profissional com ênfase na qualidade, segurança e bem-estar dos funcionários. Anualmente todos participam de atividades de treinamento como cursos, palestras, seminários e visitas técnicas.<br />
                                              <br />
                                          </p>
                                          <p>O programa de bolsa de estudo incentiva a formação técnica e universitária, e a participação em cursos de especialização. Assim como programas de iniciação profissional, como jovem aprendiz, e programas de estágio e trainee oferecem oportunidade a jovens para ingressarem no mercado de trabalho.</p>
                                          <p><br />
                                            Todas essas ações contribuem para o perfil profissional comprometido e de qualidade que é a marca dos funcionários do grupo Pedra Agroindustrial.<br />
                                            <br />
                                            </p>
                                          <table width="95%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td width="37%"><div align="center"><a href="trabalheConosco.php"><img src="img/trabalheConoscoTitulo.gif" alt="" width="127" height="75" border="0" /></a></div></td>
                                              <td width="13%"><div align="center"><img src="img/dividorTitulo.gif" alt="" width="46" height="95" /></div></td>
                                              <td width="50%"><div align="center"><a href="oportunidades.php"><img src="img/VagasOportunidadesTitulo.gif" alt="" width="183" height="75" border="0" /></a></div></td>
                                            </tr>
                                          </table>
                                          <p><br />  
                                              <br />
                                          </p></td>
                                        <td width="35%" style="padding-left:18px;">


										<?
										$sql = "SELECT fotos_gestao.* FROM fotos_gestao order by id";

										$rs = pg_query($sql);
										
										while($rr = pg_fetch_assoc($rs)){
											?>
											<table width="217" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos.gif">
											<tr>
												<td height="135" valign="top" style="padding-left:2px; padding-top:2px;"><img src="adminsite/uploads/gestao/<?=$rr['imagem']?>" alt="" width="211" height="130" border="0" /></td>
											</tr>
											</table>
											<br />
											<?
										}
										?>


										  </td>
                                      </tr>
                                    </table>
                                    <p>&nbsp;</p>
                                    </td>
                                </tr>
                              
                          </table>
                            </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
