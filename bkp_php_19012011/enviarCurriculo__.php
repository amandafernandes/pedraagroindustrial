<?
	require_once("inc/conn.php");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="files/util.js" type="text/javascript"></script>
<script src="files/trabalheNovo.js" type="text/javascript"></script>
<script src="files/ajax.js" type="text/javascript"></script>
<script src="files/commom.js" type="text/javascript"></script>
<script>
function msgerro(msg, foco, aba){
	alert(msg);
	abreAba(aba);
	$(foco).focus();
}
function limpaForm(){
	TRABALHE.reset();
	CURSOS.reset();
	EXPERIENCIA.reset();
	$('curriculo').reset();
}
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return false;
}
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; max-width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px;"><p><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">&nbsp;Enviar seu currículo</span><br />
                              </p>
                            <p>&nbsp;</p>
							&nbsp;Campos com * são obrigatórios.
							<p>&nbsp;</p>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td><table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td><table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td height="30" background="img/abaEsq.gif" style="width:8px;">&nbsp;</td>
                                        <td height="30" background="img/abaMeio.gif" ><div align="center"><span id="lbl_aba_1" class="abaativa" onclick="abreAba(1)">1-Documentação</span></div></td>
                                        <td height="30" background="img/abaDir.gif" style="width:11px;" style="background-position:top center;">&nbsp;</td>
                                      </tr>
                                    </table></td>
                                    <td><table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td height="30" background="img/abaEsq.gif" style="width:8px;">&nbsp;</td>
                                        <td height="30" background="img/abaMeio.gif"><div align="center"><span id="lbl_aba_2" class="abainativa" onclick="abreAba(2)">2-Dados Pessoais</span></div></td>
                                        <td height="30" background="img/abaDir.gif" style="width:11px;" style="background-position:top center;">&nbsp;</td>
                                      </tr>
                                    </table></td>
                                    <td><table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td height="30" background="img/abaEsq.gif" style="width:8px;">&nbsp;</td>
                                        <td height="30" background="img/abaMeio.gif"><div align="center"><span id="lbl_aba_3" class="abainativa" onclick="abreAba(3)">3-Dados Profissionais</span></div></td>
                                        <td height="30" background="img/abaDir.gif" style="width:11px;" style="background-position:top center;">&nbsp;</td>
                                      </tr>
                                    </table></td>
                                    <td><table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td height="30" background="img/abaEsq.gif" style="width:8px;">&nbsp;</td>
                                        <td height="30" background="img/abaMeio.gif"><div align="center"><span id="lbl_aba_4" class="abainativa" onclick="abreAba(4)">4-Formação</span></div></td>
                                        <td height="30" background="img/abaDir.gif" style="width:11px;" style="background-position:top center;">&nbsp;</td>
                                      </tr>
                                    </table></td>
                                    <td><table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td height="30" background="img/abaEsq.gif" style="width:8px;">&nbsp;</td>
                                        <td height="30" background="img/abaMeio.gif"><div align="center"><span id="lbl_aba_5" class="abainativa" onclick="abreAba(5)">5-Anexar currículo</span></div></td>
                                        <td height="30" background="img/abaDir.gif" style="width:11px;" style="background-position:top center;">&nbsp;</td>
                                      </tr>
                                    </table></td>
                                  </tr>
								  <tr>
									<td colspan=5 height=42 background="img/fundoAba.gif">
									&nbsp;
									</td>
								</tr>
                                </table>
                                  </td>
                              </tr>
                            </table>


<br>

<form action="gravarCurriculo.php" method="post" name="curriculo" id="curriculo" onsubmit="return false;" enctype="multipart/form-data">
<input type="hidden" value="cad" name="acao" id="acao" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:-40px;">
<tr>
	<td width="67%" valign="top">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="16%" height="25">&nbsp;</td>
			<td width="84%" height="25" style="padding-right:47px; padding-top:6px;">
				
				<div align="right"><span style="padding-top:4px; ">
					Passo:&nbsp;<span id="qabaT">1</span> / 5&nbsp;&nbsp;&nbsp;
					<input type="button" onclick="proximaAba('del');" name="anteriorBtnT" id="anteriorBtnT" value="Anterior" class="botaoPadrao" disabled />
					<input type="button" onclick="proximaAba('add');" name="proximoBtnT" id="proximoBtnT" value="Próximo" class="botaoPadrao" />
					<input type="button" onclick="enviarForm();" name="enviaBtnT" id="enviaBtnT" value="Enviar" class="botaoPadrao" />
				</span></div>
			</td>
		</tr>
		</table>

	
		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="aba_1">
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin:8px;">DOCUMENTAÇÃO</div><br /></td>
		</tr>
		<tr>
			<td width="16%" height="30"><div align="right">RG:</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><input name="rg" type="text" class="formularioPadrao" id="rg" size="20" maxlength="14" /></td>
		</tr>
		<tr>
			<td height="30"><div align="right">Data Expedição:</div></td>
			<td height="30" style="padding-left:6px;"><input name="expedicao" type="text" class="formularioPadrao" id="expedicao" size="15" /></td>
		</tr>
		<tr>
			<td height="30"><div align="right">*CPF:</div></td>
			<td height="30" style="padding-left:6px;"><input name="cpf" type="text" class="formularioPadrao" id="cpf" size="25" /></td>
		</tr>
		<tr>
			<td height="30"><div align="right">CNH:</div></td>
			<td height="30" style="padding-left:6px;"><input name="cnh" type="text" class="formularioPadrao" id="cnh" size="25" maxlength="12" /></td>
		</tr>
		<tr>
			<td height="30"><div align="right">Categoria CNH:</div></td>
			<td height="30" style="padding-left:6px;"><input name="categoriacnh" type="text" class="formularioPadrao" id="categoriacnh" size="3" maxlength="3" /></td>
		</tr>
		</table>




		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="aba_2" style="display:none;">
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin:8px;">DADOS PESSOAIS</div></td>
		</tr>
		<tr>
			<td width="16%" height="25"><div align="right">*Nome Completo:</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><input name="nomecompleto" type="text" class="formularioPadrao" id="nomecompleto" size="60" value="<?=$usr['nome_completo']?>" maxlength="40" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">*Nome Mãe:</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><input name="nomemae" type="text" class="formularioPadrao" id="nomemae" size="60" value="<?=$usr['bome_mae']?>" maxlength="30" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">Nome Pai:</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><input name="nomepai" type="text" class="formularioPadrao" id="nomepai" size="60" value="<?=$usr['nome_pai']?>" maxlength="30" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">*Data Nascimento:</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><input name="nascimento" type="text" class="formularioPadrao" id="nascimento" size="15" value="<?=$usr['dt_nascimento']?>" onkeyup="return mask('##/##/####',event,this);" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">Sexo:</div></td>
			<td width="84%" height="30" style="padding-left:6px;">
				<select name="sexo" class="formularioPadrao" id="sexo">
					<option value="M">Masculino</option>
					<option value="F">Feminino</option>
				</select>                                    
			</td>
		</tr>
		<tr>
		<td height="25"><div align="right">Estado Cívil:</div></td>
			<td height="30" style="padding-left:6px;">
				<select name="estadocivil" class="formularioPadrao" id="estadocivil">
					<?
					$qryEstadoCivil = pg_query("SELECT * FROM rhestado_civil;");
					while($estadoCivil = pg_fetch_assoc($qryEstadoCivil)){?>
					<option value="<?=$estadoCivil['id_estado_civil'];?>"><?=$estadoCivil['descricao'];?></option>
					<? }?>
				</select>                                    
			</td>
		</tr>
		<tr>
			<td height="25"><div align="right">Logradouro:</div></td>
			<td height="30" style="padding-left:6px;">
				<select name="tplogradouro" class="formularioPadrao" id="tplogradouro">
				<?
				$qryTpLogradouro = pg_query("SELECT * FROM ustp_logradouro order by descricao asc;");
				while($tpLogradouro = pg_fetch_assoc($qryTpLogradouro)){
					?>
					<option value="<?=$tpLogradouro['id_tp_logradouro'];?>" <?=($tpLogradouro['id_tp_logradouro']==1)?'selected':''?>><?=$tpLogradouro['descricao'];?></option>
					<? 
				}
				?>										
				</select>
				&nbsp;&nbsp;Endereço: <input name="endereco" type="text" class="formularioPadrao" id="endereco" size="40" maxlength="45" />
				&nbsp;&nbsp;Número: <input name="numero" type="text" class="formularioPadrao" id="numero" size="10" maxlength="6" />
			</td>
		</tr>
		<tr>
			<td height="25"><div align="right">Complemento:</div></td>
			<td height="30" style="padding-left:6px;">
			<input name="complemento" type="text" class="formularioPadrao" id="complemento" size="30" maxlength="30" />
			</td>
		</tr>
		<tr>
			<td height="25"><div align="right">Bairro:</div></td>
			<td height="30" style="padding-left:6px;"><input name="bairro" type="text" class="formularioPadrao" id="bairro" size="60" maxlength="20" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">*Estado:</div></td>
			<td height="30" style="padding-left:6px;">
				<select name="estado" class="formularioPadrao" id="estado" onchange="mudaEstado(this.value);">
				<option value="">Selecione</option>
				<?
				$qryEstados = pg_query("SELECT distinct unidade_fed FROM uscidade  order by unidade_fed asc;");
				while($estados = pg_fetch_assoc($qryEstados)){
					?>
					<option value="<?=$estados['unidade_fed'];?>"><?=$estados['unidade_fed'];?></option>
					<? 
				}
				?>										
				</select>
				&nbsp;&nbsp;*Cidade:
				<select name="cidade" class="formularioPadrao" id="cidade">
					<option value="">Selecione a UF</option>
				</select>  
			</td>
		</tr>
		<tr>
			<td height="25"><div align="right">CEP:</div></td>
			<td height="30" style="padding-left:6px;"><input name="cep" type="text" class="formularioPadrao" id="cep" size="15" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">*Fone Preferencial:</div></td>
			<td height="30" style="padding-left:6px;"><input name="fonepreferencial" type="text" class="formularioPadrao" id="fonepreferencial" size="15" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">Celular:</div></td>
			<td height="30" style="padding-left:6px;"><input name="celular" type="text" class="formularioPadrao" id="celular" size="15" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">*E-Mail:</div></td>
			<td height="30" style="padding-left:6px;"><input name="email" type="text" class="formularioPadrao" id="email" value="<?=$_POST['email']?>" maxlength="100" size="60" /></td>
		</tr>
		<tr>
			<td height="25"><div align="right">*Senha:</div></td>
			<td height="30" style="padding-left:6px;"><input name="senha" type="password" class="formularioPadrao" id="senha" size="15" maxlength="10" /></td>
		</tr>
		<tr>
			<td height="25" colspan=2>
				Pessoa Portadora de Necessidades Especiais?&nbsp;&nbsp;<select name="pne" class="formularioPadrao" id="pne" onchange="chkpne();">
				<option value="S">SIM</option>
				<option value="N" selected="selected">NÃO</option>
				</select>
			</td>
		</tr>
		<tr>
			<td height="25" colspan=2>
				Tipo de Necessidade Especial:&nbsp;&nbsp;<select name="tipopne" class="formularioPadrao" id="tipopne" disabled>
				<option value="">Selecione</option>
				<option value="a">Auditiva</option>
				<option value="v">Visual</option>
				<option value="f">Física</option>
				<option value="m">Mental</option>
				</select>
			</td>
		</tr>
		</table>



		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="aba_3" style="display:none;">
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin:8px;">DADOS PROFISSIONAIS</div></td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<input type="hidden" id="qtdInformacao" name="qtdInformacao" value="1" />
				<div id="templateInformacao" style="display:none;"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="objInformacao#n#"><tr><td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="77%">Informação #n#</td></tr></table></td></tr><tr><td width="16%" height="25"><div align="right">Área de Atuação:</div></td><td width="84%" height="30" style="padding-left:6px;"><select name="areaatuacao#n#" class="formularioPadrao" id="areaatuacao#n#" onchange="mudaAreaAtuacao(this.value, '#n#');"><option value="">Selecione</option><?$qryAreaAtuacao = pg_query("SELECT * FROM rhtp_area_atuacao_cand order by nm_area_atuacao;");while($areaAtuacao = pg_fetch_assoc($qryAreaAtuacao)){?><option value="<?=$areaAtuacao['id_tp_area_atuacao_cand'];?>"><?=$areaAtuacao['nm_area_atuacao'];?></option><? }?></select></td></tr><tr><td height="25"><div align="right">Cargo Pretendido:</div></td><td height="30" style="padding-left:6px;"><select name="cargopretendido#n#" class="formularioPadrao" id="cargopretendido#n#"><option value="">Selecione a área</option></select></td></tr><tr><td height="25">&nbsp;</td><td height="30" style="padding-left:6px;">&nbsp;</td></tr></table></div>
				<div id="contentInformacao" style="overflow:hidden;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="objInformacao1">
						<tr>
							<td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="77%">Informação 1</td>
								</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="16%" height="25"><div align="right">Área de Atuação:</div></td>
							<td width="84%" height="30" style="padding-left:6px;">
								<select name="areaatuacao1" class="formularioPadrao" id="areaatuacao1" onchange="mudaAreaAtuacao(this.value, '1');">
								<option value="">Selecione</option>
								<?
								$qryAreaAtuacao = pg_query("SELECT * FROM rhtp_area_atuacao_cand order by nm_area_atuacao;");
									while($areaAtuacao = pg_fetch_assoc($qryAreaAtuacao)){?>
									<option value="<?=$areaAtuacao['id_tp_area_atuacao_cand'];?>"><?=$areaAtuacao['nm_area_atuacao'];?></option>
									<? }?>
								</select>                                    
							</td>
						</tr>
						<tr>
							<td height="25"><div align="right">Cargo Pretendido:</div></td>
							<td height="30" style="padding-left:6px;">
								<select name="cargopretendido1" class="formularioPadrao" id="cargopretendido1">
								<option value="">Selecione a área</option>
								</select>                                    
							</td>
						</tr>
						<tr>
							<td height="25">&nbsp;</td>
							<td height="30" style="padding-left:6px;">&nbsp;</td>
						</tr>
					</table>
				</div>                                    
			</td>
		</tr>
		<tr>
			<td height="25">&nbsp;</td>
			<td height="30" style="padding-left:6px;"><span style="padding-top:4px; ">
				<input type="button" name="button" id="btAddInformacao" value="Adicionar Informação" class="botaoPadrao" style="width:148px; margin-top:2px; float:left;" onclick="INFORMACAO.push();" />
				<input type="button" name="button" id="btRemoverInformacao" value="Retirar Informação" class="botaoPadrao" style="width:148px; margin-top:2px; float:left; margin-left:15px; display:none;" onclick="INFORMACAO.shift();" />
				</span>
			</td>
		</tr>
		<tr>
			<td height="25">&nbsp;</td>
			<td height="30" style="padding-left:6px;">&nbsp;</td>
		</tr>
		<tr>
			<td height="30" colspan="2"><div align="left">Atualmente esta trabalhando?</div></td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<span style="padding-left:6px;">
				<select name="trabalhando" class="formularioPadrao" id="trabalhando">
				<option value="S">SIM</option>
				<option value="N">NÃO</option>
				</select>
				</span>
			</td>
		</tr>
		<tr>
		<td height="30" colspan="2"><div align="left">Você já trabalhou em alguma empresa do grupo?</div></td>
		</tr>
		<tr>
		<td height="30" colspan="2">
			<span style="padding-left:6px;">
			<select name="trabalhounogrupo" class="formularioPadrao" id="trabalhounogrupo">
			<option value="S">SIM</option>
			<option value="N">NÃO</option>
			</select>
			</span>
		</td>
		</tr>
		<tr>
		<td height="30" colspan="2"><div align="left">Aceita transferência para outra cidade?</div></td>
		</tr>
		<tr>
		<td height="30" colspan="2">
			<span style="padding-left:6px;">
			<select name="transferenciacidade" class="formularioPadrao" id="transferenciacidade">
			<option value="S">SIM</option>
			<option value="N">NÃO</option>
			</select>
			</span>
		</td>
		</tr>
		<tr>
		<td height="30" colspan="2"><div align="left">Aceita revezamento de turno?</div></td>
		</tr>
		<tr>
		<td height="30" colspan="2">
			<span style="padding-left:6px;">
			<select name="revezamento" class="formularioPadrao" id="revezamento">
			<option value="S">SIM</option>
			<option value="N">NÃO</option>
			</select>
			</span>
		</td>
		</tr>
		</table>




		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="aba_4" style="display:none;">
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin:8px;">FORMAÇÃO ACADÊMICA</div></td>
		</tr>
		<tr>
			<td width="16%" height="25"><div align="right">Grau de instrução :</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><select name="formacaoNivelAcademico" class="formularioPadrao" id="formacaoNivelAcademico1">
				<?
				$qtyGrauInstrucao = pg_query("SELECT * FROM rhgrau_instrucao;");
				while($grauInstrucao = pg_fetch_assoc($qtyGrauInstrucao)){?>
				<option value="<?=$grauInstrucao['id_grau_instrucao'];?>"><?=$grauInstrucao['descricao'];?></option>
				<? }?>
			</select>
			</td>
		</tr>
		<tr>
			<td width="16%" height="25">&nbsp;</td>
			<td width="84%" height="30" style="padding-left:6px;">&nbsp;</td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<input type="hidden" id="qtdFormacao" name="qtdFormacao" value="1" />
				<div id="templateFormacao" style="display:none;"><table id="objFormacao#n#" cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="77%">Formação #n#</td></tr></table></td></tr><tr><td width="16%" height="25"><div align="right">Curso:</div></td><td width="84%" height="30" style="padding-left:6px;"><select name="formacaoCurso#n#" class="formularioPadrao" id="formacaoCurso#n#" onchange="chkCurComp2('#n#')"><option value="">Selecione</option><? $qryFormacaoCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso='1' OR tp_curso='2' ORDER BY descricao ASC;"); while($formacaoCurso = pg_fetch_assoc($qryFormacaoCurso)){ ?><option value="<?=$formacaoCurso['id_curso'];?>"><?=$formacaoCurso['descricao'];?></option><? }?><option value="9998">OUTROS</option></select></td></tr><tr><td colspan=2><table id="txtFormacaoCurso#n#" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;"><tr><td width="16%" height="25"><div align="right">Situação:</div></td><td width="84%" height="30" style="padding-left:6px;"><select name="formacaoComplemento#n#" class="formularioPadrao" id="formacaoComplemento#n#"><option value="c">Completo</option><option value="i">Incompleto</option><option value="a">Andamento</option></select></td></tr></table></td></tr><tr><td colspan=2><table id="txtOutrosFormacao#n#" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;"><tr><td height="25"><div align="right">Outros Cursos:</div></td><td height="30" style="padding-left:6px;"><input name="outrosFormacaoCurso#n#" type="text" class="formularioPadrao" id="outrosFormacaoCurso#n#" size="60" maxlength="40" /></td></tr></table></td></tr><tr><td height="25">&nbsp;</td><td height="30" style="padding-left:6px;">&nbsp;</td></tr></table></div>
				<div id="contentFormacao" style="overflow:hidden;">
					<table id="objFormacao1" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="77%">Formação 1</td>
								</tr>
							</table></td>
						</tr>
						<tr>
							<td width="16%" height="25"><div align="right">Curso:</div></td>
							<td width="84%" height="30" style="padding-left:6px;">
								<select name="formacaoCurso1" class="formularioPadrao" id="formacaoCurso1" onchange="chkCurComp2('1')">
									<option value="">Selecione</option>
									<?
									$qryFormacaoCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso='1' OR tp_curso='2' ORDER BY descricao ASC;");
									while($formacaoCurso = pg_fetch_assoc($qryFormacaoCurso)){?>
									<option value="<?=$formacaoCurso['id_curso'];?>"><?=$formacaoCurso['descricao'];?></option>
									<? }?>
									<option value="9998">OUTROS</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<table id="txtFormacaoCurso1" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;">
								<tr>
									 <td width="16%" height="25"><div align="right">Situação:</div></td>
									 <td width="84%" height="30" style="padding-left:6px;"><select name="formacaoComplemento1" class="formularioPadrao" id="formacaoComplemento1">
								<option value="c">Completo</option>
								<option value="i">Incompleto</option>
								<option value="a">Andamento</option>
								</select></td>
								</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td colspan=2>
								<table id="txtOutrosFormacao1" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;">
								<tr>
									 <td height="25"><div align="right">Outros Cursos:</div></td>
									 <td height="30" style="padding-left:6px;"><input name="outrosFormacaoCurso1" type="text" class="formularioPadrao" id="outrosFormacaoCurso1" size="60" maxlength="40" /></td>
								</tr>
								</table>
							</td>
						</tr>

						<tr>
						  <td height="25">&nbsp;</td>
						  <td height="30" style="padding-left:6px;">&nbsp;</td>
						</tr>
					</table>
				</div>                                    
			</td>
		</tr>
		<tr>
			<td height="25">&nbsp;</td>
			<td height="30" style="padding-left:6px;"><span style="padding-top:4px; ">
			<input type="button" name="button" id="btAddFormacao" value="Adicionar Formação" class="botaoPadrao" style="width:148px; margin-top:2px; float:left;" onclick="TRABALHE.push();" />
			<input type="button" name="button" id="btRemoverFormacao" value="Retirar Formação" class="botaoPadrao" style="width:148px; margin-top:2px; float:left; margin-left:15px; display:none;" onclick="TRABALHE.shift();" />
			</span>
		</td>
		</tr>
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin-bottom:8px; margin-top:20px;">CURSOS COMPLEMENTARES (TREINAMENTOS, IDIOMAS, INFORMÁTICA...)</div></td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<input type="hidden" id="qtdCursos" name="qtdCursos" value="1" />
				<div id="templateCursos" style="display:none;"><table id="objCurso#n#" cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="77%">Curso #n#</td></tr></table></td></tr><tr><td width="16%" height="25"><div align="right">Curso:</div></td><td width="84%" height="30" style="padding-left:6px;"><select name="cursosCurso#n#" class="formularioPadrao" id="cursosCurso#n#" onchange="chkCurComp('#n#')"><option value="">Selecione</option><? $qryCursosCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso!='1' AND tp_curso!='2' ORDER BY descricao ASC;"); while($cursosCurso = pg_fetch_assoc($qryCursosCurso)){?><option value="<?=$cursosCurso['id_curso'];?>"><?=$cursosCurso['descricao'];?></option><? } ?><option value="9999">OUTROS</option></select></td></tr><tr><td colspan=2><table id="txtCursosCurso#n#" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;"><tr><td width="16%" height="25"><div align="right">Situação:</div></td><td width="84%" height="30" style="padding-left:6px;"><select name="cursosCompleto#n#" class="formularioPadrao" id="cursosCompleto#n#"><option value="c">Completo</option><option value="i">Incompleto</option><option value="a">Andamento</option></select></td></tr></table></td></tr><tr><td colspan=2><table id="txtOutrosCursos#n#" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;"><tr><td height="25"><div align="right">Outros Cursos:</div></td><td height="30" style="padding-left:6px;"><input name="outrosCursos#n#" type="text" class="formularioPadrao" id="outrosCursos#n#" size="60" maxlength="40" /></td></tr></table></td></tr><tr><td height="25">&nbsp;</td><td height="30" style="padding-left:6px;">&nbsp;</td></tr></table></div>
				<div id="contentCursos" style="overflow:hidden;">
					<table id="objCurso1" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="77%">Curso 1</td>
								</tr>
							</table></td>
						</tr>
						<tr>
						  <td width="16%" height="25"><div align="right">Curso:</div></td>
						  <td width="84%" height="30" style="padding-left:6px;"><select name="cursosCurso1" class="formularioPadrao" id="cursosCurso1" onchange="chkCurComp('1')">
							<option value="">Selecione</option>
							<?
							$qryCursosCurso = pg_query("SELECT * FROM rhcurso WHERE tp_curso>'2' ORDER BY descricao ASC;");
							while($cursosCurso = pg_fetch_assoc($qryCursosCurso)){?>
								<option value="<?=$cursosCurso['id_curso'];?>"><?=$cursosCurso['descricao'];?></option>
							<? }?>
							<option value="9999">OUTROS</option>
							</select></td>
						</tr>

						<tr>
							<td colspan=2>
								<table id="txtCursosCurso1" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;">
								<tr>
									 <td width="16%" height="25"><div align="right">Situação:</div></td>
									 <td width="84%" height="30" style="padding-left:6px;"><select name="cursosCompleto1" class="formularioPadrao" id="cursosCompleto1">
								<option value="c">Completo</option>
								<option value="i">Incompleto</option>
								<option value="a">Andamento</option>
								</select></td>
								</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td colspan=2>
								<table id="txtOutrosCursos1" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;">
								<tr>
									 <td height="25"><div align="right">Outros Cursos:</div></td>
									 <td height="30" style="padding-left:6px;"><input name="outrosCursos1" type="text" class="formularioPadrao" id="outrosCursos1" size="60" maxlength="40" /></td>
								</tr>
								</table>
							</td>
						</tr>
						<tr>
						  <td height="25">&nbsp;</td>
						  <td height="30" style="padding-left:6px;">&nbsp;</td>
						</tr>
					</table>
				</div>                                    
			</td>
		</tr>
		<tr>
			<td height="25">&nbsp;</td>
			<td width="84%" height="30" style="padding-left:6px;"><span style="padding-top:4px; ">
				<input type="button" name="button" id="btAddCursos" value="Adicionar Curso" class="botaoPadrao" style="width:148px; margin-top:2px; float:left;" onclick="CURSOS.push();" />
				<input type="button" name="button" id="btRemoverCursos" value="Retirar Formação" class="botaoPadrao" style="width:148px; margin-top:2px; float:left; margin-left:15px; display:none;" onclick="CURSOS.shift();" />
				</span>
			</td>
		</tr>
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin-bottom:8px; margin-top:20px;">EXPERIÊNCIA PROFISSIONAL</div></td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<input type="hidden" id="qtdExperiencia" name="qtdExperiencia" value="1" />
				<div id="templateExperiencia" style="display:none;"><table id="objExperiencia#n#" cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="77%">Experiência #n#</td></tr></table></td></tr><tr><td width="16%" height="25"><div align="right">Nome da Empresa:</div></td><td width="84%" height="30" style="padding-left:6px;"><input name="experienciaEmpresa#n#" type="text" class="formularioPadrao" id="experienciaEmpresa#n#" size="60" maxlength="40" /></td></tr><tr><td height="25"><div align="right">Admissão:</div></td><td height="30" style="padding-left:6px;"><input name="experienciaAdmissao#n#" type="text" class="formularioPadrao" id="experienciaAdmissao#n#" size="15" onkeyup="return mask('##/##/####',event,this);" /></td></tr><tr><td height="25"><div align="right">Demissão:</div></td><td height="30" style="padding-left:6px;"><input name="experienciaDemissao#n#" type="text" class="formularioPadrao" id="experienciaDemissao#n#" size="15" onkeyup="return mask('##/##/####',event,this);" /></td></tr><tr><td height="25"><div align="right">Último Cargo:</div></td><td height="30" style="padding-left:6px;"><select name="experienciaCargo#n#" class="formularioPadrao" id="experienciaCargo#n#"><option value="">Selecione</option><? $qryCargos = pg_query("SELECT * FROM rhcargo order by descricao;"); while($cargo = pg_fetch_assoc($qryCargos)){ ?><option value="<?=$cargo['id_cargo'];?>"><?=$cargo['descricao'];?></option><? }?></select></td></tr><tr><td height="25"><div align="right">Último Salário:</div></td><td height="30" style="padding-left:6px;"><input name="experienciaSalario#n#" type="text" class="formularioPadrao" id="experienciaSalario#n#" size="15" onkeypress="return(MascaraMoeda(this,'.',',',event))" maxlength="8" /></td></tr><tr><td height="30" colspan="2"><div align="left">Trabalhos Desenvolvidos:</div></td></tr><tr><td height="30" colspan="2"><span style="padding-left:6px;"><textarea cols=100 name="experienciaTrabalhos#n#" class="formularioPadrao" id="experienciaTrabalhos#n#" style="height:50px;"></textarea></span></td></tr></table></div>
				<div id="contentExperiencia" style="overflow:hidden;">
					<table id="objExperiencia1" cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="30" colspan="2" bgcolor="#E2E2CB" style="font-weight:bold; font-size:13px; color:#787507; padding-left:8px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="77%">Experiência 1</td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="16%" height="25"><div align="right">Nome da Empresa:</div></td>
						<td width="84%" height="30" style="padding-left:6px;"><input name="experienciaEmpresa1" type="text" class="formularioPadrao" id="experienciaEmpresa1" size="60" maxlength="40" /></td>
					</tr>
					<tr>
						<td height="25"><div align="right">Admissão:</div></td>
						<td height="30" style="padding-left:6px;"><input name="experienciaAdmissao1" type="text" class="formularioPadrao" id="experienciaAdmissao1" size="15" onkeyup="return mask('##/##/####',event,this);" /></td>
					</tr>
					<tr>
						<td height="25"><div align="right">Demissão:</div></td>
						<td height="30" style="padding-left:6px;"><input name="experienciaDemissao1" type="text" class="formularioPadrao" id="experienciaDemissao1" size="15" onkeyup="return mask('##/##/####',event,this);" /></td>
					</tr>
					<tr>
						<td height="25"><div align="right">Último Cargo:</div></td>
						<td height="30" style="padding-left:6px;">
							<select name="experienciaCargo1" class="formularioPadrao" id="experienciaCargo1">
							<option value="">Selecione</option>
							<?
							$qryCargos = pg_query("SELECT * FROM rhcargo order by descricao;");
							while($cargo = pg_fetch_assoc($qryCargos)){?>
							<option value="<?=$cargo['id_cargo'];?>"><?=$cargo['descricao'];?></option>
							<? }?>
							</select>
						</td>
					</tr>
					<tr>
						<td height="25"><div align="right">Último Salário:</div></td>
						<td height="30" style="padding-left:6px;"><input name="experienciaSalario1" type="text" class="formularioPadrao" id="experienciaSalario1" size="15" onkeypress="return(MascaraMoeda(this,'.',',',event))" maxlength="8" /></td>
					</tr>
					<tr>
						<td height="30" colspan="2"><div align="left">Trabalhos Desenvolvidos:</div></td>
					</tr>
					<tr>
						<td height="30" colspan="2"><span style="padding-left:6px;">
							<textarea cols=100 name="experienciaTrabalhos1" class="formularioPadrao" id="experienciaTrabalhos1" style="height:50px;"></textarea></span>
						</td>
					</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td height="25">&nbsp;</td>
			<td width="84%" height="30" style="padding-left:6px;"><span style="padding-top:4px; ">
			<input type="button" name="button" id="btAddExperiencia" value="Adicionar Experiência" class="botaoPadrao" style="width:148px; margin-top:2px; float:left;" onclick="EXPERIENCIA.push();" />
			<input type="button" name="button" id="btRemoverExperiencia" value="Retirar Formação" class="botaoPadrao" style="width:148px; margin-top:2px; float:left; margin-left:15px; display:none;" onclick="EXPERIENCIA.shift();" />
			</span>
			</td>
		</tr>  
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin-bottom:8px; margin-top:20px;">DADOS COMPLEMENTARES </div></td>
		</tr>
		<tr>
			<td height="30" colspan="2"><span style="padding-left:6px;">
				<textarea name="complementares" cols="100" rows="10" class="formularioPadraoTextArea" id="complementares"></textarea></span>
			</td>
		</tr>
		</table>



                                               
		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="aba_5" style="display:none;">
		<tr>
			<td height="30" colspan="2"><div style="font-size:15px; font-weight:normal; color:#566336; margin:8px;">Anexe seu currículo em formato .DOC ou .PDF</div></td>
		</tr>
		<tr>
			<td width="16%" height="25"><div align="right">Currículo:</div></td>
			<td width="84%" height="30" style="padding-left:6px;"><input name="arquivo" type="file" class="formularioPadrao" id="arquivo"/></td>
		</tr>
		</table>


		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="16%" height="25">&nbsp;</td>
			<td width="84%" height="25" style="padding-right:47px; padding-top:6px;">
				
				<div align="right"><span style="padding-top:4px; ">
					Passo:&nbsp;<span id="qaba">1</span> / 5&nbsp;&nbsp;&nbsp;
					<input type="button" onclick="proximaAba('del');" name="anteriorBtn" id="anteriorBtn" value="Anterior" class="botaoPadrao" disabled />
					<input type="button" onclick="proximaAba('add');" name="proximoBtn" id="proximoBtn" value="Próximo" class="botaoPadrao" />
					<input type="button" onclick="enviarForm();" name="enviaBtn" id="enviaBtn" value="Enviar" class="botaoPadrao" />
				</span></div>
			</td>
		</tr>
		</table>




	</td>
</tr>
</table>
</form>
							<br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;" align="center"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
