﻿<?
@session_start();
require_once("inc/conn.php");

if ($_SESSION['estaLogado']!=true){
	die("<script>location='gestaodepessoas.php';</script>");
}

$_PROCSEL = true;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<?
if ($_GET['erro'] == 1) {
	?><script>alert('Inscrição efetuada com sucesso.');</script><?
}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; max-width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Participação em Processos Seletivos</span>
                          

<?
include("menuRH.php");
?>
<br>

							<?
							$rs = pg_query("
							SELECT
								rhtd_unidade.nm_unidade as nomeunidade,
								rhtd_vaga_disponivel.processo_seletivo as idprocesso,
								rhtd_vaga_disponivel.titulo_vaga as titulo,
								rhtd_vaga_disponivel.data_divulgacao as data,
								rhtd_vaga_disponivel.qtde_vagas as qtdevagas,
								rhtd_vaga_disponivel.descricao_vaga as descricao,
								rhtd_vaga_disponivel.conhecimentos as conhecimentos,
								rhtd_vaga_disponivel.observacoes as obs,
								rhtd_vaga_disponivel.remuneracao as remuneracao,
								rhtp_area_atuacao_cand.nm_area_atuacao as nomeareaatuacao,
								rhcand_proc_seletivo.status as status 
							FROM
								rhtd_unidade
								Inner Join rhtd_vaga_disponivel ON rhtd_vaga_disponivel.unidade = rhtd_unidade.id_unidade
								Inner Join rhtp_area_atuacao_cand ON rhtp_area_atuacao_cand.id_tp_area_atuacao_cand = rhtd_vaga_disponivel.area_atuacao 
								Inner Join rhcand_proc_seletivo ON rhcand_proc_seletivo.processo_seletivo = rhtd_vaga_disponivel.processo_seletivo 
							WHERE 
								rhcand_proc_seletivo.candidato = ".$_SESSION['idUserLogado']."
							ORDER BY
								rhtd_vaga_disponivel.data_divulgacao DESC
							");
							if(pg_num_rows($rs)){
								while($rr=pg_fetch_assoc($rs)){
									?>
									
									<?=dataeuabr($rr['data'])?><br />
									<div style="font-size:14px; color:#4f6718; line-height:15px;"><?=$rr['titulo']?></div>                            
									<strong style="line-height:20px;">Unidade:</strong> <?=$rr['nomeunidade']?><br />
									<strong style="line-height:20px;">Área de Atuação:</strong> <?=$rr['nomeareaatuacao']?><br />
									<strong style="line-height:20px;">Remuneração:</strong> R$<?=number_format($rr['remuneracao'], 2, ',', '');?><br />
									<strong style="line-height:20px;">Quantidade de Vagas:</strong> <?=$rr['qtdevagas']?><br />
									<strong style="line-height:20px;">Descrição da Vaga:</strong> <br />
									<?=$rr['descricao']?> <br />
									<strong style="line-height:20px;">Conhecimento:</strong><br />
									<?=$rr['conhecimentos']?><br />
									<strong style="line-height:20px;">Observações:</strong><br />
									<?=$rr['obs']?><br />
									<?=$rr['status']?><br />
									<img src="img/linhaVerticalBranco.gif" alt="" width="650" height="1" /><br />
									<br />

									<?
								}
							}
							?>	 


                          
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;" align="center"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
