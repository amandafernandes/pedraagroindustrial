<?
	require_once("inc/conn.php");

	//em bytes => para bits: (bytes x 8)
	//usado para limitar o resultado do CURL
	define(KB, 1024);
	define(MB, 1048576);
	define(GB, 1073741824);

	ini_set("upload_max_filesize", (10*MB));
	ini_set("post_max_size", (11*MB));
	ini_set("max_execution_time", 120000);

	set_time_limit(0);
	ignore_user_abort();

	$_PATH_FILES = "curriculos/";
	$_EXT_FILES_ACCEPT = array(".pdf", ".doc", ".docx", ".jpg");

	function alertjs($msg){
		echo "<script>alert('".addslashes($msg)."');</script>";
	}

	function limpaArray($a){
		unset($a);
		$a = array();
		return $a;
	}

	function preparaSqlInsert($fld, $tbl){
		$s = "INSERT INTO ".$tbl." (";
		$t = count($fld);
		for ($i=0; $i<$t; $i++) {
			$s .= $fld[$i][0].(($i<($t-1))?', ':'');
		}
		$s .= ") VALUES (";
		for ($i=0; $i<$t; $i++) {
			$s .= "'".$fld[$i][1]."'".(($i<($t-1))?', ':'');
		}
		$s .= ");";
		return $s;
	}

	function preparaSqlUpdate($fld, $tbl, $fldalt){
		$s = "UPDATE ".$tbl." SET ";
		$t = count($fld);
		for ($i=0; $i<$t; $i++) {
			$s .= $fld[$i][0]."='".$fld[$i][1]."'".(($i<($t-1))?', ':'');
		}
		$s .= " WHERE ".$fldalt[0]."=".$fldalt[1];
		return $s;
	}

	function limpacpf($cpf){
		return str_replace(".","",str_replace("-","",$cpf));
	}

	function todecimal($v){
		return ($v!='')?str_replace(",",".",str_replace(".","",$v)):'';
	}

	function converteTel($tel,$tipo){
		$tel = str_replace("-","", $tel);
		$tel = str_replace("(","", $tel);
		$tel = explode(")",$tel);
		
		$t['ddd'] = $tel[0];
		$t['fone'] = $tel[1];
		return $t[$tipo];
	}


	$fields = array();


	if(isset($_POST['acao']) && $_POST['acao']=='alt'){
		
		$id = $_SESSION['idUserLogado'];
		$cpf = limpacpf($_POST['cpf']);

		// ARQUIVO
		if ($_FILES["arquivo"]['name'] != ''){
			$arqvExt = explode(".",$_FILES['arquivo']['name']);
			$arqvExt = ".".$arqvExt[1];
			$frm_arq = $cpf.$arqvExt;
			if(!in_array($arqvExt, $_EXT_FILES_ACCEPT)) {
				?>
				<script>parent.msgerro('O arquivo escolhido não é permitido!','arquivo',5);</script>
				<?
				exit;
			}
			if(!move_uploaded_file($_FILES['arquivo']['tmp_name'], $_PATH_FILES.$frm_arq)){
				?>
				<script>parent.msgerro('Erro ao tentar enviar o arquivo.\nTente novamente, por favor!','arquivo',5);</script>
				<?
				exit;
			}
		}

		// DOCUMENTAÇÃO / DADOS PESSOAIS
		//array_push($fields, array('cpf', substr($cpf, 0, -2)));
		//array_push($fields, array('digito_cpf', substr($cpf, -2)));
		array_push($fields, array('nome_completo', $_POST['nomecompleto']));
		array_push($fields, array('nome_mae', $_POST['nomemae']));
		array_push($fields, array('dt_nascimento', databreua($_POST['nascimento'])));
		array_push($fields, array('cidade', $_POST['cidade']));
		array_push($fields, array('estado', $_POST['estado']));
		$fone = $_POST['fonepreferencial'];
		array_push($fields, array('ddd_fone', converteTel($fone,'ddd')));
		array_push($fields, array('fone', converteTel($fone,'fone')));
		//array_push($fields, array('email', $_POST['email']));
		if ($_POST['senha'] != ''){
			array_push($fields, array('senha', md5($_POST['senha'])));
		}
		array_push($fields, array('nome_pai', $_POST['nomepai']));
		array_push($fields, array('sexo', $_POST['sexo']));
		array_push($fields, array('estado_civil', $_POST['estadocivil']));
		array_push($fields, array('endereco', $_POST['endereco']));
		array_push($fields, array('bairro', $_POST['bairro']));
		$cep = ($_POST['cep']=='')?0:str_replace("-","",$_POST['cep']);
		array_push($fields, array('cep',$cep));
		if ($_POST['celular']!='') {
			$celular = $_POST['celular'];
			array_push($fields, array('celular', converteTel($celular,'fone')));
			array_push($fields, array('ddd_celular', converteTel($celular,'ddd')));
		}
		array_push($fields, array('numero_rg', $_POST['rg']));
		if ($_POST['expedicao']!='') {
			array_push($fields, array('dt_expedicao_rg', databreua($_POST['expedicao'])));
		}
		$cnh = ($_POST['cnh']=='')?0:$_POST['cnh'];
		array_push($fields, array('numero_cnh', $cnh));
		array_push($fields, array('categ_cnh', $_POST['categoriacnh']));
		array_push($fields, array('grau_instrucao', $_POST['formacaoNivelAcademico']));
		array_push($fields, array('pne', $_POST['pne']));
		array_push($fields, array('tipo_pne', $_POST['tipopne']));
		array_push($fields, array('tp_logradouro', $_POST['tplogradouro']));
		array_push($fields, array('numero', $_POST['numero']));
		array_push($fields, array('complemento', $_POST['complemento']));
		array_push($fields, array('fg_esta_trabalhando', $_POST['trabalhando']));
		array_push($fields, array('fg_trabalhou_emp_grupo', $_POST['trabalhounogrupo']));
		array_push($fields, array('fg_aceita_transf', $_POST['transferenciacidade']));
		array_push($fields, array('revezamento_turno', $_POST['revezamento']));

		$sql = preparaSqlUpdate($fields, 'public.rhcand_info', array('id_candidato', $id));
		pg_query($sql);

		/*************************************/

		//INFOS PROFISSIONAIS
		pg_query("DELETE FROM rhcand_info_adicional WHERE candidato='".$id."';");
		$i=1;
		while(isset($_POST['areaatuacao'.$i])){
			if($_POST['areaatuacao'.$i]!=""){
				$fields = limpaArray($fields);

				array_push($fields, array('candidato', $id));
				array_push($fields, array('area_atuacao', $_POST['areaatuacao'.$i]));
				array_push($fields, array('cargo_pretendido', $_POST['cargopretendido'.$i]));
				array_push($fields, array('experiencia', $_POST['experiencia'.$i]));

				$sql = preparaSqlInsert($fields, 'rhcand_info_adicional');
				pg_query($sql);
			}
			$i++;		
		}

		/*************************************/
		
		//FORMAÇÃO ACADEMICA
		pg_query("DELETE FROM rhcand_curso WHERE candidato='".$id."';");
		$i=1;
		while(isset($_POST['formacaoCurso'.$i])){
			if($_POST['formacaoCurso'.$i]!=""){
				$fields = limpaArray($fields);

				array_push($fields, array('candidato', $id));
				array_push($fields, array('curso', $_POST['formacaoCurso'.$i]));
				array_push($fields, array('fg_status', $_POST['formacaoComplemento'.$i]));
				array_push($fields, array('descricao', $_POST['outrosFormacaoCurso'.$i]));

				$sql = preparaSqlInsert($fields, 'rhcand_curso');
				pg_query($sql);
			}
			$i++;
		}

		/*************************************/
		
		//CURSOS COMPLEMENTARES
		$i=1;
		while(isset($_POST['cursosCurso'.$i])){
			if($_POST['cursosCurso'.$i]!=""){
				$fields = limpaArray($fields);

				array_push($fields, array('candidato', $id));
				array_push($fields, array('curso', $_POST['cursosCurso'.$i]));
				array_push($fields, array('fg_status', $_POST['cursosCompleto'.$i]));
				array_push($fields, array('descricao', $_POST['outrosCursos'.$i]));

				$sql = preparaSqlInsert($fields, 'rhcand_curso');
				pg_query($sql);
			}
			$i++;
		}

		/*************************************/

		//EXPERIENCIA PROFISSIONAL
		pg_query("DELETE FROM rhcand_experiencia WHERE candidato='".$id."';");
		$i=1;
		while(isset($_POST['experienciaEmpresa'.$i])){
			if($_POST['experienciaEmpresa'.$i]!=""){
				$fields = limpaArray($fields);

				array_push($fields, array('candidato', $id));
				array_push($fields, array('nome_empresa', $_POST['experienciaEmpresa'.$i]));
				if ($_POST['experienciaAdmissao'.$i] != '') {
					array_push($fields, array('dt_admissao', databreua($_POST['experienciaAdmissao'.$i])));
				}
				if ($_POST['experienciaDemissao'.$i] != '') {
					array_push($fields, array('dt_demissao', databreua($_POST['experienciaDemissao'.$i])));
				}
				if ($_POST['experienciaCargo'.$i] != '') {
					array_push($fields, array('ult_cargo', $_POST['experienciaCargo'.$i]));
				}
				if ($_POST['experienciaSalario'.$i] != '') {
					array_push($fields, array('ultimo_salario', todecimal($_POST['experienciaSalario'.$i])));
				}
				array_push($fields, array('trab_desenvolvido', $_POST['experienciaTrabalhos'.$i]));

				$sql = preparaSqlInsert($fields, 'rhcand_experiencia');
				pg_query($sql);
			}
			$i++;		
		}


		$complementares = $_POST['complementares'];
		pg_query("DELETE FROM rhcand_dados_compl WHERE candidato='".$id."';");
		pg_query("INSERT INTO rhcand_dados_compl values ('".$id."', '".$complementares."');");


		@session_unregister('rhNome');
		@session_register('rhNome');				
		$_SESSION['rhNome'] = $_POST['nomecompleto'];

		?>
		<script>
			alert('Currículo alterado com sucesso!');
			parent.location='alterarCurriculo.php';
		</script>
		<?
	}
	
?>