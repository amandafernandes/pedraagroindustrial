﻿<?
define(KB, 1024);
define(MB, 1048576);
define(GB, 1073741824);

session_start();

if ($_SESSION['idioma']=='') {
	$idioma = 'BR';
} else {
	$idioma = $_SESSION['idioma'];
}

require_once("inc/conn.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px;"><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Jornal Observador</span><br />
                            <br />
						  <? 
							$rs = pg_query("select * from observador WHERE idioma = '".$idioma."' order by nome desc");
							if(pg_num_rows($rs)){
								while($rr=pg_fetch_assoc($rs)){
									?>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
									  <tr>
										<td valign="top"><table width="170" border="0" cellpadding="0" cellspacing="0" style="margin-right:13px;">
											<tr>
											  <td height="212" style="background-color:#d5d2b1;"><div align="center"><img src="adminsite/uploads/observador/<?=$rr['thumb'];?>" alt="" width="160" height="202" border="0" /></div></td>
											</tr>
										</table></td>
										<td valign="top" style="padding-top:5px;"><div style="font-size:15px; color:#566336; margin-top:0px; margin-bottom:4px;"><?=$rr['nome'];?></div>
											<p><img src="img/edicaoTitulo.gif" alt="" width="81" height="17" style="margin-top:5px; margin-bottom:4px;" /></p>
										  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(img/fundoObservadorChamadas.gif); height:102px; width:465px;">
											  <tr>
												<td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
													  <td valign="top" style="padding-left:2px; padding-right:15px; padding-top:10px;"><div style="font-size:13px; color:#4f6718; line-height:15px;"><?=$rr['tituloe'];?></div>
														<?=$rr['resenhae'];?></td>
													  <td valign="top" style="padding-left:15px; padding-right:15px; padding-top:10px;"><div style="font-size:13px; color:#8d620c; line-height:15px;"><?=$rr['tituloc'];?></div>
														<?=$rr['resenhac'];?></td>
													  <td valign="top" style="padding-left:20px; padding-right:0px; padding-top:10px;"><div style="font-size:13px; color:#922c07; line-height:15px;"><?=$rr['titulod'];?></div>
														<?=$rr['resenhad'];?></td>
													</tr>
												</table></td>
											  </tr>
											</table>
										  <img src="img/versaoDownload.gif" alt="" width="109" height="20" /><br />
										  <a href="adminsite/uploads/observador/<?=$rr['arquivo'];?>" target="_blank">Download do Arquivo - PDF - <strong><?=number_format((filesize("adminsite/uploads/observador/".$rr['arquivo'])/MB), 2, ",", "")?>MB</strong></a></td>
									  </tr>
									</table>
									<br />
									<br />
									<?
								}
							}
						?>					 
					 </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
