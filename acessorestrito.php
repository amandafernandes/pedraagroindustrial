<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
<script>
function verifica_email(str_email){
if (str_email.search(/^[a-zA-Z0-9]+[_a-zA-Z0-9-]*(\.[_a-z0-9-]+)*@[a-z?G0-9]+(-[a-z?G0-9]+)*(\.[a-z?G0-9-]+)*(\.[a-z]{2,4})$/) != -1) return true;
else return false;
}
function verifica1(){
	var f = document.f_ar;
	var login = f.login;
	if(login.value.replace(/\ /g, "") == ""){
		alert("Digite o login");
		login.focus();
		return true;
	}
	var senha = f.senha;
	if(senha.value.replace(/\ /g, "") == ""){
		alert("Digite a senha");
		senha.focus();
		return true;
	}	
	f.submit();
}
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;">
		<div style="width:970px; margin:0 auto;">
			<? require_once("topo.html"); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td width="0" align="center" valign="top" style="padding-top:32px; padding-left:17px; padding-right:15px;">
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(img/acessoRestritoLogin.jpg); background-repeat:no-repeat; width:228px; height:183px;">
                                <tr>
                                  <td valign="top" style="padding-left:45px; padding-top:73px;"><form action="acessorestrito_valida.php" name="f_ar" style="margin:0px;" method="post">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td colspan="2" align="left">Usuário</td>
                                        </tr>
                                      <tr>
                                        <td colspan="2" align="left"><input name="login" type="text" class="formularioPadrao" id="login" size="25" /></td>
                                        </tr>
                                      <tr>
                                        <td colspan="2" align="left">Senha:</td>
                                        </tr>
                                      <tr>
                                        <td width="46%" align="left" valign="middle"><input name="senha" type="password" class="formularioPadrao" id="senha" size="14" /></td>
                                        <td width="54%" align="left" valign="top"><span style="padding-top:4px; ">
                                          <input type="button" value="Continuar" class="botaoPadrao" style="width:60px; margin-top:2px; cursor:pointer;" onclick="verifica1();" />
                                        </span></td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                                        </table>                            </td>
                        </tr>
                      </table></form></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
