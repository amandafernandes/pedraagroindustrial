﻿<?
	session_start();
	session_destroy();
	$AUTH = false;
	require_once('topo.php');
?>
	<!-- END Header -->
	<div class="breadcrumbs"><a href="index.php">Início</a></div>
	<!-- Content -->
	<div id="content" class="colM">
		<h1>Sessão encerrada</h1>
		<p>Obrigado por visitar nossa administração.</p>
		<p><a href="index.php">Acessar novamente</a></p>
		<br class="clear" />
	</div>
	<!-- END Content -->
	<div id="footer"></div>
</div>
<!-- END Container -->
</body>
</html>