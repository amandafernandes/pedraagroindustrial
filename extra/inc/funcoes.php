<?

function hasPermission($cn){
	global $DB;
	
	$SQL = "SELECT * FROM auth_user WHERE id = ".$_SESSION['adminAuthUserId']." AND is_superuser = 1";
	$qValidaSuperUser = $DB->query($SQL);
	
	if($DB->numRows($qValidaSuperUser) == 1) return true;
	
	$SQL = "SELECT * FROM auth_permission WHERE codname = '".$cn."'";
	$qGetPermission = $DB->query($SQL);
	
	$arrPermission = $DB->fetchAssoc($qGetPermission);
	
	$SQL = "SELECT * FROM auth_user_user_permissions WHERE user_id = ".$_SESSION['adminAuthUserId']." AND permision_id = ".$arrPermission['id'];
	
	$qValidaPermission = $DB->query($SQL);
	
	if($DB->numRows($qValidaPermission) == 1)return true;
	
	return false;	
	
}

function hasPermissionReport($id, $file, $filial){
	global $DB;

	//verifica se relatorio existe
	$SQL="
	SELECT
		relatorios.id as id
	FROM
		relatorios
	WHERE
		relatorios.id = ".$id."
		AND
		relatorios.arquivo = '".$file."'";
	$qValidaRelatorio = $DB->query($SQL);
	if($DB->numRows($qValidaRelatorio) == 0) return false;

	//verifica se filial existe
	$SQL="
	SELECT
		usfilial.id_filial as id
	FROM
		usfilial
	WHERE
		usfilial.id_filial = ".$filial;
	$qValidaFilial = $DB->query($SQL);
	if($DB->numRows($qValidaFilial) == 0) return false;

	//verifica se fornecedor tem permissao para ver o relatorio da filial
	$SQL="
	SELECT
		DISTINCT usfilial_acesso.filial_acesso
	FROM
		usfilial
		Inner Join usfilial_acesso ON usfilial.id_filial = usfilial_acesso.filial_acesso
	WHERE
		usfilial_acesso.filial_principal = ".$_SESSION['userextAuthUserId']." AND
		usfilial_acesso.filial_acesso = ".$filial." AND 
		usfilial_acesso.relatorio = ".$id;
	$qValidaUser = $DB->query($SQL);	
	if($DB->numRows($qValidaUser) == 0) return false;

	return true;	
}

function hasPermissionAdminReport($id, $file, $filial){
	global $DB;

	//verifica se relatorio existe
	$SQL="
	SELECT
		relatorios.id as id
	FROM
		relatorios
	WHERE
		relatorios.id = ".$id." AND
		relatorios.arquivo = '".$file."'";
	$qValidaRelatorio = $DB->query($SQL);
	if($DB->numRows($qValidaRelatorio) == 0) return false;

	//verifica se filial existe
	$SQL="
	SELECT
		usfilial.id_filial as id
	FROM
		usfilial
	WHERE
		usfilial.id_filial = ".$filial;
	$qValidaFilial = $DB->query($SQL);
	if($DB->numRows($qValidaFilial) == 0) return false;
	
	//verifica se admin tem permissao para ver o relatorio da filial
	$SQL = "SELECT * FROM auth_user WHERE id = ".$_SESSION['adminAuthUserId']." AND is_superuser = 1";
	$qValidaSuperUser = $DB->query($SQL);
	if($DB->numRows($qValidaSuperUser) == 1) {

		$SQL="
			SELECT
				usfilial.id_filial
			FROM
				usfilial
				Inner Join usfilial_acesso ON usfilial.id_filial = usfilial_acesso.filial_acesso
			WHERE
				usfilial_acesso.relatorio =  ".$id."
		";
		$qValidaAdmin = $DB->query($SQL);	
		if($DB->numRows($qValidaAdmin) == 0) return false;		

	} else {

		$SQL="
			SELECT
				usfilial.id_filial
			FROM
				usfilial
				Inner Join usfilial_acesso ON usfilial.id_filial = usfilial_acesso.filial_principal
				Inner Join auth_user_relatorios ON usfilial_acesso.relatorio = auth_user_relatorios.id_relatorio
			WHERE
				usfilial_acesso.relatorio = '".$id."' AND
				auth_user_relatorios.id_usuario =  '".$_SESSION['adminAuthUserId']."'
		";
		$qValidaAdmin = $DB->query($SQL);	
		if($DB->numRows($qValidaAdmin) == 0) return false;	

	}

	return true;	
}

function toDate($data, $formato){
	return strftime($formato, strtotime($data));
}

function getLastDayOfMonth($month, $year){
	return idate('d', mktime(0, 0, 0, ($month + 1), 0, $year));
}

function saveArquivo($f,$l){
	move_uploaded_file($f["tmp_name"], $l);
	chmod($l, 0777);
}

// Retorna a extens�o de um determinado arquivo
function getExt($in) {
	$arrFile = explode('.',$in);
	return strtolower($arrFile[count($arrFile)-1]);
}

function httpResponseRedirect($ulr){
	try{
		header('Location: '.$ulr);
	}catch(Exception $e){
		print("<script>location='".$ulr."';</script>");
	}
}

function write($v,$t){
	if(!$v) return $t;
	return $v;
}

// Fun��o que cria um id novo
function pg_next_id($seq) {
	$qry = pg_query("SELECT nextval('$seq') AS id;");
	$arr = pg_fetch_assoc($qry);
	return $arr['id'];
}

function limpaArray($a){
	unset($a);
	$a = array();
	return $a;
}

function enviaEmail($emailFrom, $titulo, $mensagem, $emailTo, $cc = '', $bcc = ''){
	$quebra = (strpos($_ENV['SERVER_SOFTWARE'], "Microsoft") === false)?"\n":"\r\n";

	$headers = "From: ".$emailFrom.$quebra;
	$headers .= "Content-Type: text/html; charset=UTF-8".$quebra;
	
	if ($cc != '') $headers .= "CC: ".$cc.$quebra;
	if ($bcc != '') $headers .= "BCC: ".$bcc.$quebra;
	
	if(!@mail($emailTo, $titulo, $mensagem, $headers)) return false;
	return true;
}

function _w($s){print("<pre>");print($s);print("</pre>");}
function _wa($a){print("<pre>");print_r($a);print("</pre>");}


function gera_senha_esqueci(){
	$t = substr(md5(strtotime('Now')), 0, 10);
	return array(md5($t), $t);
}


/*
relat�rios
*/
function dateformat($date){
	return implode("/",array_reverse(explode("-",$date)));
}
?>