<?
@session_start();

if ($SETTINGS['DBTYPE'] == "postgres"){
	$dbconn = @pg_connect("host=".$SETTINGS['DBHOST']." port=".$SETTINGS['DBPORT']." dbname=".$SETTINGS['DBNAME']." user=".$SETTINGS['DBUSER']." password=".$SETTINGS['DBPASS']);
	register_shutdown_function('pg_close');
}

$SETTINGS['dbconn'] = $dbconn;

?>