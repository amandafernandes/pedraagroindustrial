<?

require_once("config.php");
require_once("conn.php");

##################################################
# Versão 1.0b - MYSQL - PGSQL				     #
# Por Gabriel Galiaso - gabrielgaliaso@gmail.com #
##################################################
	
class GaDbQuery{
	function error(){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			return (pg_last_error());
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			return (mysql_error());
		}
	}
	
	function query($sql){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){			
			$qry = pg_query($sql)or die($this->error());
			return $qry;
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			$sql = str_replace("offset",",",$sql);
			
			$qry = mysql_query($sql)or die($this->error());
			return $qry;
		}
	}
	
	function limit($x,$y){	
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			 return " limit ".$x." offset ".$y;
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			 return " limit ".$x." , ".$y;
		}
	}
	
	function pg_insert_id($s){
		$qQry = $this->query("SELECT currval('$s') as id;");
		$ar = $this->fetchAssoc($qQry);
		return $ar['id'];
	}
	
	function isertId($s=""){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			return $this->pg_insert_id($s);
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			return mysql_insert_id();
		}
	}
	
	function numRows($querySet){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			return pg_num_rows($querySet);
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			return mysql_num_rows($querySet);
		}
	}
	
	function fetchAssoc($querySet){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			return pg_fetch_assoc($querySet);
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			return mysql_fetch_assoc($querySet);
		}
	}
	
	function escapeString($s){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			return pg_escape_string($s);
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			return mysql_escape_string($s);
		}
	}
	
	function like($c,$v){
		global $SETTINGS;
		
		if ($SETTINGS['DBTYPE'] == "postgres"){
			return $c." ILIKE '".$v."'";
		}
		
		if ($SETTINGS['DBTYPE'] == "mysql"){
			return $c." LIKE '".$v."'";
		}
	}
}

$DB = new GaDbQuery();

require_once("funcoes.php");
?>