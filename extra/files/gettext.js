/* gettext library */

var catalog = new Array();

function pluralidx(count) { return (count == 1) ? 0 : 1; }
catalog['6 a.m.'] = '6 da manh\u00e3';
catalog['Add'] = 'Adicionar';
catalog['Available %s'] = '%s dispon\u00edveis';
catalog['Calendar'] = 'Calend\u00e1rio';
catalog['Cancel'] = 'Cancelar';
catalog['Choose a time'] = 'Escolha uma hora';
catalog['Choose all'] = 'Escolher todos';
catalog['Chosen %s'] = '%s escolhido(s)';
catalog['Clear all'] = 'Limpar tudo';
catalog['Clock'] = 'Rel\u00f3gio';
catalog['Hide'] = 'Esconder';
catalog['January February March April May June July August September October November December'] = 'Janeiro Fevereiro Mar\u00e7o Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro';
catalog['Midnight'] = 'Meia-noite';
catalog['Noon'] = 'Meio-dia';
catalog['Now'] = 'Agora';
catalog['Remove'] = 'Remover';
catalog['S M T W T F S'] = 'D S T Q Q S S';
catalog['Select your choice(s) and click '] = 'Selecione sua(s) escolha(s) e clique ';
catalog['Show'] = 'Mostrar';
catalog['Sunday Monday Tuesday Wednesday Thursday Friday Saturday'] = 'Domingo Segunda Ter\u00e7a Quarta Quinta Sexta S\u00e1bado';
catalog['Today'] = 'Hoje';
catalog['Tomorrow'] = 'Amanh\u00e3';
catalog['Yesterday'] = 'Ontem';


function gettext(msgid) {
 var value = catalog[msgid];
 if (typeof(value) == 'undefined') {
 return msgid;
 } else {
 return (typeof(value) == 'string') ? value : value[0];
 }
}

function ngettext(singular, plural, count) {
 value = catalog[singular];
 if (typeof(value) == 'undefined') {
 return (count == 1) ? singular : plural;
 } else {
 return value[pluralidx(count)];
 }
}

function gettext_noop(msgid) { return msgid; }

function interpolate(fmt, obj, named) {
 if (named) {
 return fmt.replace(/%\(\w+\)s/g, function(match){return String(obj[match.slice(2,-2)])});
 } else {
 return fmt.replace(/%s/g, function(match){return String(obj.shift())});
 }
}