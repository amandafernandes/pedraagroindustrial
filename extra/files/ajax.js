// AJAX 2.0
var ajaxcontrol = {'version': '','itens': ['Msxml2.XMLHTTP.8.0','Msxml2.XMLHTTP.7.0','Msxml2.XMLHTTP.6.0','Msxml2.XMLHTTP.5.0','Msxml2.XMLHTTP.4.0','Msxml2.XMLHTTP.3.0','Msxml2.XMLHTTP.2.0','Msxml2.XMLHTTP','Microsoft.XMLHTTP']};
var ajax = function(pt) {
	this.itens = [];
	for (var k in pt) this[k] = pt[k];
	if (window.ActiveXObject) {
		if (ajaxcontrol.version == '') for (var i = 0; i < ajaxcontrol.itens.length; i++) {
			try { this.obj = new ActiveXObject(ajaxcontrol.itens[i]); ajaxcontrol.version = ajaxcontrol.itens[i]; break; } catch (e) { continue; }
			if (i >= (ajaxcontrol.itens.length-1)) return false;
		} else try { this.obj = new ActiveXObject(ajaxcontrol.version); } catch (e) { return false; }
	} else if (window.XMLHttpRequest) {
		ajaxcontrol.version = 'XMLHttpRequest';
		this.obj = new XMLHttpRequest();
	} else return false;
	return true;
};
ajax.prototype = {
	'method': 'POST',
	'itens': [],
	'push': function(k,v) { this.itens[k] = v; },
	'pop': function(k) { delete this.itens[k]; },
	'clear': function() { delete this.itens; this.itens = [] },
	'open': function() {
		this.aux = '';
		for (var i in this.itens) { if (this.aux != '') this.aux += '&'; this.aux += i+'='+encodeURIComponent(this.itens[i]); }
		this.obj.open(this.method, (this.method.toUpperCase() == 'GET')?(this.url+'?'+this.aux):this.url, true);
		var this_obj = this.obj;
		var this_ajax = this;
		with (this_obj) {
			setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			setRequestHeader("CharSet", "UTF-8");
			onreadystatechange = function () {
				try {
					this_ajax.onchange(this_obj.readyState);
					if (this_obj.readyState == 4) {
						this_ajax.onload(this_obj.status);
						if (this_obj.status == 200) {
							this_ajax.xml = this_obj.responseXML;
							this_ajax.text = this_obj.responseText;
							this_ajax.oncomplete();
						} else this_ajax.onerror(this_obj.status, this_obj.statusText);
					}
				} catch(e) {}
			};
			send((this_ajax.method.toUpperCase() == 'GET')?null:this_ajax.aux);
		}
	},
	//INI XML ACTIONS
	'strChildName': '',
	'setChildName': function() { this.strChildName = arguments[0]; },
	'getCountItens': function() { return this.xml.getElementsByTagName(this.strChildName).length;},
	'getAttByName': function() { try { return this.xml.getElementsByTagName(this.strChildName)[arguments[1]].getAttribute(arguments[0]); } catch (e) { return null; } },
	'getDataByName': function() { try { return this.xml.getElementsByTagName(this.strChildName)[arguments[0]].firstChild.data; } catch (e) { return null; } },
	//END XML ACTIONS
	'stop': function() { this.obj.abort(); },
	'oncomplete': function() {}, 'onload': function() {}, 'onchange': function() {}, 'onerror': function() {}
};

// JSON
var json = {
	'characs': {'\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"' : '\\"', '\\': '\\\\'},
	'decode': function(tx) { try { return eval('(' + tx + ')'); } catch(e) { return null; } },
	'encode': function(ob,wl) {
		var a, i, k, l, r = /["\\\x00-\x1f\x7f-\x9f]/g, v;
		switch (typeof ob) {
			case 'string': return r.test(ob)?'"' + ob.replace(r, function (a) { var c = json.characs[a]; if (c) return c; c = a.charCodeAt(); return '\\u00' + Math.floor(c/16).toString(16)+(c%16).toString(16); }) + '"' : '"' + ob + '"';
			case 'number': return isFinite(ob) ? String(ob) : 'null';
			case 'boolean':
			case 'null': return String(ob);
			case 'object': a = []; if (!ob) return 'null'; if (typeof ob.toJSON === 'function') return json.encode(ob.toJSON()); if (typeof ob.length === 'number' && !(ob.propertyIsEnumerable('length'))) { l = ob.length; for (i = 0; i < l; i += 1) a.push(json.encode(ob[i], wl) || 'null'); return '[' + a.join(',') + ']'; } if (wl) { l = wl.length; for (i = 0; i < l; i += 1) { k = wl[i]; if (typeof k === 'string') { v = json.encode(ob[k], wl); if (v) a.push(json.encode(k) + ':' + v); } } } else for (k in ob) if (typeof k === 'string') { v = json.encode(ob[k], wl); if (v) a.push(json.encode(k) + ':' + v); } return '{' + a.join(',') + '}';
		}
	},
	'parse': function (tx,ft) {
		var j;
		function walk(k,v) { var i, n; if (v && typeof v === 'object') for (i in v) if (Object.prototype.hasOwnProperty.apply(v, [i])) { n = walk(i, v[i]); if (n !== undefined) v[i] = n; } return ft(k,v); }
		if (/^[\],:{}\s]*$/.test(tx.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(:?[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) { j = eval('(' + tx + ')'); return typeof ft === 'function' ? walk('', j) : j; }
		throw new SyntaxError('parseJSON');
	}
};