﻿var ESQUECI = {
	'show':function(){
		$('id_password').disabled = true;
		$('id_password').style.backgroundColor = "#efefef";
		$('btn_acessar').style.display = 'none';
		$('bt_trocar').style.display = 'none';		
		$('bt_enviar').style.display = 'inline';
		$('bt_cancelar').style.display = 'inline';
		$('senha_nova').style.display = 'none';
		$('label_senha').innerHTML = 'Senha:';
		$('id_password').value = '';
		$('id_passwordn').value = '';
		$('id_email').value = '';
		$('email_confirm').style.display = 'inline';
		try{
			$('erro_txt').style.display = 'none';
		}catch(e){
		}
	},
	'hide':function(){
		$('id_password').disabled = false;
		$('id_password').style.backgroundColor = "#FFF";
		$('btn_acessar').style.display = 'inline';
		$('bt_enviar').style.display = 'none';
		$('bt_cancelar').style.display = 'none';
		$('erro_email').style.display = 'none';
		$('email_confirm').style.display = 'none';
		$('id_username').value = '';
		$('id_password').value = '';
		$('id_passwordn').value = '';
		$('id_email').value = '';
	},
	'send':function(){
		var erro = '';
		var myPost = '';
		
		myPost += 'acao=1&';

		var f = $('id_username');
		var v = f.value;
		if (v.isWhite()){
			alert("Digite o CNPJ / CPF.");
			f.focus();
			return false;
		}else{
			myPost += 'cpf='+v+'&';
		}
		
		var f = $('id_email');
		var v = f.value;
		if (v.isWhite()){
			alert("Digite o e-mail.");
			f.focus();
			return false;
		}else{
			myPost += 'email='+v+'&';
		}
		
		if (myPost.length>1) { myPost = myPost.substring(0, (myPost.length-1)) }
		
		$('aguarde').style.display = 'block';
		
		var myCallback={
			success:function(o){
				var xml = new xmlParse();
				xml.setObjAjax(o);
				xml.setChildName('contato');
				xml.parse();

				$('aguarde').style.display = 'none';
				
				if (xml.getAttByName('acaoret',0) == 1) {
					ESQUECI.hide();
					alert("Cadastro ainda não está liberado para acessar o sistema.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 2){
					ESQUECI.hide();
					$('erro_email').style.display = "block";
					return false;
				}					
				if (xml.getAttByName('acaoret',0) == 3) {
					ESQUECI.hide();
					alert("A senha foi enviada para o e-mail cadastrado.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 4) {
					ESQUECI.hide();
					alert("Senha não enviada.\nTente novamente mais tarde, por favor.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 5) {
					alert("Código de acesso não encontrado.");
					$('id_username').focus();
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 6) {
					ESQUECI.hide();
					alert("O e-mail informado nao confere com o e-mail\ncadastrado.\nEntre em contato com a área de relacionamento\ncom fornecedores e parceiros de sua unidade.");
					return false;
				}				
			},
			failure:function(){
				ESQUECI.hide();
				alert("Erro ao tentar enviar lembrete.\nTente novamente mais tarde, por favor.");
				return false;
			}
		};
		Connect.asyncRequest('POST', "envia_senha.php", myCallback, myPost);
	}
}

var TROCA = {
	'show':function(){
		$('id_password').disabled = false;
		$('id_password').style.backgroundColor = "#FFF";	
		$('btn_acessar').style.display = 'none';
		$('bt_trocar').style.display = 'inline';		
		$('bt_enviar').style.display = 'none';
		$('bt_cancelar').style.display = 'inline';
		$('senha_nova').style.display = 'inline';
		$('label_senha').innerHTML = 'Senha Atual:';
		$('id_password').value = '';
		$('id_passwordn').value = '';				
		try{
			$('erro_txt').style.display = 'none';
		}catch(e){
		}
	},
	'hide':function(){
		$('btn_acessar').style.display = 'inline';
		$('bt_trocar').style.display = 'none';
		$('bt_cancelar').style.display = 'none';
		$('erro_email').style.display = 'none';
		$('senha_nova').style.display = 'none';
		$('label_senha').innerHTML = 'Senha:';
		$('id_username').value = '';
		$('id_password').value = '';
		$('id_passwordn').value = '';
	},
	'send':function(){
		var erro = '';
		var myPost = '';
		
		myPost += 'acao=2&';

		var f = $('id_username');
		var v = f.value;
		if (v.isWhite()){
			alert("Digite o CNPJ / CPF.");
			f.focus();
			return false;
		}else{
			myPost += 'cpf='+v+'&';
		}

		var f = $('id_password');
		var v = f.value;
		if (v.isWhite()){
			alert("Digite a senha atual.");
			f.focus();
			return false;
		}else{
			myPost += 'senha='+v+'&';
		}
		
		var f = $('id_passwordn');
		var v = f.value;
		if (v.isWhite()){
			alert("Digite a nova senha.");
			f.focus();
			return false;
		}else if(v.length<6){
			alert("A senha deve conter no mínimo 6 dígitos.");
			f.focus();
			return false;		
		}else{
			myPost += 'senhan='+v+'&';
		}			

		if (myPost.length>1) { myPost = myPost.substring(0, (myPost.length-1)) }

		$('aguarde').style.display = 'block';
		var myCallback={
			success:function(o){
				var xml = new xmlParse();
				xml.setObjAjax(o);
				xml.setChildName('contato');
				xml.parse();

				$('aguarde').style.display = 'none';

				if (xml.getAttByName('acaoret',0) == 1) {
					TROCA.hide();
					alert("Cadastro ainda não está liberado para acessar o sistema.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 2) {
					TROCA.hide();
					alert("Senha alterada com sucesso.");
					return false;
				}					
				if (xml.getAttByName('acaoret',0) == 3) {
					TROCA.hide();
					alert("Senha não alterada.\nTente novamente mais tarde, por favor.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 4) {
					alert("Código de acesso ou senha atual não conferem.");
					$('id_username').focus();
					return false;
				}						
			},
			failure:function(){
				TROCA.hide();
				alert("Erro ao tentar enviar senha.\nTente novamente mais tarde, por favor.");
				return false;
			}
		};
		Connect.asyncRequest('POST', "envia_senha.php", myCallback, myPost);
	}
}
