function $(id){return document.getElementById(id);}


String.prototype.isWhite = function() {
	if (!isVoid(this)) {
		var value = this.replace(/^\s+/m,'').replace(/\s+$/m,'');
		return (value == '');
	}
	return true;
}


function isVoid(obj) {
	if (obj != null && typeof obj != 'undefined' && obj != "") return false;
	return true;
}

function verifica_email(str_email){
	if (str_email.search(/^[a-zA-Z0-9]+[_a-zA-Z0-9-]*(\.[_a-z0-9-]+)*@[a-z?G0-9]+(-[a-z?G0-9]+)*(\.[a-z?G0-9-]+)*(\.[a-z]{2,4})$/) != -1) return true;
	else return false;
}

var key = 0;
function soNumeroPress(evento){
	if (key == 0)	{	
		key = evento.keyCode;	
	}	
	if ((key == 8 || key == 13 || key == 9 || key == 71 || key == 46 || key  == 37  || key  == 39) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
		VerifiqueTAB=true; 
		return true;
	} else{
		return false;
	}
}

function soNumeroDown(evento){
	key = evento.keyCode;
	if ((key == 8 || key == 13 || key == 9 || key == 46) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)){
		VerifiqueTAB=true; 
		return true;
	} else {
		return false;
	}
}

VerifiqueTAB=true;
function Mostra(quem, tammax) {
	if ( (quem.value.length == tammax) && (VerifiqueTAB) ) {
		var i=0,j=0, indice=-1;
		for (i=0; i<document.forms.length; i++) {
			for (j=0; j<document.forms[i].elements.length; j++) {
				if (document.forms[i].elements[j].name == quem.name) {
					indice=i;
					break;
				}
			}
			if (indice != -1)
		         break;
		}
		for (i=0; i<=document.forms[indice].elements.length; i++) {
			if (document.forms[indice].elements[i].name == quem.name) {
				while ( (document.forms[indice].elements[(i+1)].type == "hidden") &&
						(i < document.forms[indice].elements.length) ) {
							i++;
				}
				document.forms[indice].elements[(i+1)].focus();
				VerifiqueTAB=false;
				break;
			}
		}
	}
}