var $ = function (a) { 
	return document.getElementById(a);
};
var $S = function (a) { return document.getElementById(a).style; };

var basic = {
	'getPos': function(e) {
		if (typeof e == 'string') e = $(e);
		var left = 0;
		var top = 0;
		while (e.offsetParent) {
			left += e.offsetLeft;
			top += e.offsetTop;
			e = e.offsetParent;
		}
		left += e.offsetLeft;
		top += e.offsetTop;
		return {x:left, y:top};
	},
	'getSize': function(e) {
		if (typeof e == 'string') e = $(e);
		return {x:e.offsetWidth, y:e.offsetHeight};
	},
	'getMousePos': function(e) {
		if (e.pageX || e.pageY) return {x:e.pageX, y:e.pageY};
		return {x:e.clientX+this.getScroll().x-document.body.clientLeft, y:e.clientY+this.getScroll().y-document.body.clientTop};
	},
	'getScroll': function() {
		if (self.pageXOffset) sX = self.pageXOffset; else if (document.documentElement && document.documentElement.scrollLeft) sX = document.documentElement.scrollLeft; else if (document.body) sX = document.body.scrollLeft;
		if (self.pageYOffset) sY = self.pageYOffset; else if (document.documentElement && document.documentElement.scrollTop) sY = document.documentElement.scrollTop; else if (document.body) sY = document.body.scrollTop;
		return {x:sX, y:sY};
	},
	'getDocSize': function() {
		return {x:document.body.offsetWidth, y:document.body.offsetHeight};
	},
	'getDocVisibleSize': function() {
		var _x, _y;
		if (window.innerWidth) {
			_x = window.innerWidth;
		} else if (document.documentElement && document.documentElement.clientWidth) {
			_x = document.documentElement.clientWidth;
		} else if (document.body) {
			_x = document.body.clientWidth;
		}
		if (window.innerHeight) {
			_y = window.innerHeight;
		} else if (document.documentElement && document.documentElement.clientHeight) {
			_y = document.documentElement.clientHeight;
		} else if (document.body) {
			_y = document.body.clientHeight;
		}
		return {x:_x, y:_y};
	},
	'zIndex': 1000,
	'bringToFront': function(e, type) {
		if (typeof e == 'string') e = $(e);
		e.style['zIndex'] = $D.basic.zIndex++;
	},
	'getDistance': function(p1, p2) { return Math.round(Math.sqrt(Math.pow(Math.abs(p2.x-p1.x),2)+Math.pow(Math.abs(p2.y-p1.y),2))); },
	'getKeyCode': function(ev) {
		ev = ev.browserEvent || ev;
		return ev.charCode || ev.keyCode || 0;
	},
	'addSelectValue': function(ob, vl, tx, sl) {
		var aux = document.createElement("OPTION");
		ob.appendChild(aux);
		aux.value = vl;
		aux.text = tx;
	}
};

var simpleSwfUpload = function(pt) {
	for (var k in pt) this[k] = pt[k];
	return new SWFUpload(this);
}

simpleSwfUpload.prototype = {
	'upload_url': "",
	'file_post_name': "pubFile",
	'file_size_limit': "10 MB",
	'file_types': "*.*",
	'file_types_description': "All Files",
	'file_upload_limit': "0",
	
	// Event handler settings
	'swfupload_loaded_handler': this.swfUploadLoaded,
	
	'file_dialog_start_handler': this.fileDialogStart,
	'file_dialog_complete_handler': this.fileDialogComplete,
	'file_queued_handler': this.fileQueued,
	
	//upload_start_handler : uploadStart,	// I could do some client/JavaScript validation here, but I don't need to.
	'upload_progress_handler': this.uploadProgress,
	'upload_error_handler': this.uploadError,
	'upload_success_handler': this.uploadSuccess,
	'upload_complete_handler': this.uploadComplete,

	// Button Settings
	'button_image_url': "img/admin/XPButtonUploadText_61x22.png",	// Relative to the SWF file
	'button_placeholder_id': "",
	'button_width': 61,
	'button_height': 22,
	
	// Flash Settings
	'flash_url': "swfupload.swf",

	'custom_settings': {
		'progress_target': "fsUploadProgress",
		'upload_successful': false
	},
	
	// Debug settings
	'debug': false,
	
	'swfUploadLoaded': function() {}, 'fileDialogStart': function() {}, 'fileDialogComplete': function() {}, 'uploadProgress': function() {}, 'uploadError': function() {}, 'uploadSuccess': function() {}, 'uploadComplete': function() {}, 'fileQueued': function() {}
}