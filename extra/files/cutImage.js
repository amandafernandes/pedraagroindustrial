uploadAndCutImageObj = new Object();
uploadAndCutImageObj.down = null;
uploadAndCutImageObj.zomm = 100;
uploadAndCutImage = function(campo,x,y,local) {
	local = local || '../uploads/thumbs/';
	var html = '';
	html += '<table cellpadding="0" cellspacing="0" border="0" style="background-image:url(img/admin/popRecorte/bg.gif); background-repeat:repeat-x; background-color:#f5f5f4; border:solid 2px #c4c4c0;">';
    html += '	<tr>';
    html += '	   	<td><img src="img/admin/popRecorte/logoRecorte.gif" style="margin:10px;" /></td>';
    html += '	</tr>';
    html += '	<tr>';
    html += '		<td width="300" style="padding-left:10px;">';
	html += '			<div style=" position:absolute; margin-left:195px; margin-top:0px;"><span id="btUploadCutImage" style="position:absolute"></span></div>';
	html += '           <input type="text" id="popCutImageFileName" style="background-image:url(\'img/admin/popRecorte/campoFoto.jpg\'); border:none; width:180px; height:19px; padding-top:2px; padding-left:5px; padding-right:5px;" />';
    html += '       </td>';
    html += '   </tr>';
    html += '   <tr>';
    html += '       <td style="padding-left:10px;padding-top:2px;">';
    html += '       	<img src="img/admin/popRecorte/zoom.jpg" /><img src="img/admin/popRecorte/in.jpg" style="cursor:pointer;" onclick="uploadAndCutImageZoom(5);" /><img src="img/admin/popRecorte/out.jpg" style="cursor:pointer;" onclick="uploadAndCutImageZoom(-5);"  />';
    html += '       </td>';
    html += '   </tr>';
    html += '   <tr>';
    html += '       <td style="padding-left:10px;padding-top:1px;padding-bottom:10px;">';
	html += '			<div>';
	html += '				<div style="overflow:hidden;width:'+x+'px;height:'+y+'px;border:solid 2px #c4c4c0;" id="divCutImage">';
	html += '					<img onmousedown="return uploadAndCutImageMouseDown(arguments[0]);" alt="" src="images/blank.gif" style="width:1px;height:1px;cursor:move;" id="imgCutImage" />';
	html += '				</div><img src="img/admin/popRecorte/cliqueArraste.gif" />';
	html += '			</div>';
    html += '       </td>';
    html += '    </tr>';
    html += '   <tr>';
    html += '       <td style="padding-left:10px;padding-bottom:10px;padding-right:10px;">';
	html += '			<img src="img/admin/popRecorte/salvar.gif" style="float:left;cursor:pointer;" onclick="cutImage();" /><img src="img/admin/popRecorte/cancelar.gif" style="float:right;cursor:pointer;" onclick="$S(\'spanCutImage\').display = \'none\';" />';
    html += '       </td>';
    html += '    </tr>';	
    html += '</table>';
	
	//html += '				<div style="overflow:hidden;width:'+x+'px;height:'+y+'px;border:solid 2px #c4c4c0;" id="divCutImage">';
	//html += '					<img onmousedown="return uploadAndCutImageMouseDown(arguments[0]);" alt="" src="images/blank.gif" style="width:1px;height:1px;cursor:move;" id="imgCutImage" />';
	//html += '				</div><img src="img/admin/popRecorte/cliqueArraste.gif" />';	
	
	try{
		$('spanCutImage').parentNode.removeChild($('spanCutImage'));
	}catch(e){ }
	
	var htmlObject = document.createElement('span');
	htmlObject.id = "spanCutImage";
	htmlObject.style.cssText = 'top:0px;left:0px;display:none;position:absolute;';
	htmlObject.innerHTML = html;
	
	$('DOM').parentNode.insertBefore(htmlObject,$('DOM'));
	
	var swfu = new simpleSwfUpload({
		'file_types': "*.jpg",
		'file_types_description': "Imagens JPG",
		'upload_url':'upload.php?destino=../uploads/temp/',
		'button_placeholder_id': "btUploadCutImage",									   
		'button_image_url': 'img/admin/popRecorte/btProcurar.gif',
		'button_width': 69,
		'button_height': 22
	});
	swfu.fileQueued = function(file){
		try {
			uploadAndCutImageObj.src = $('popCutImageFileName').value = file.name;
		}catch (e) {}
	}
	swfu.fileDialogComplete = function(numFilesSelected, numFilesQueued){
		if(numFilesSelected != 0){
			if(numFilesSelected == 1){
				try {
					swfu.startUpload();
				}catch (e) {}
			}else{
				alert('Selecione somente um arquivo!');
				this.cancelUpload();
			}
		}
	}
	swfu.uploadSuccess = function(file, serverData) {
		try{
			if (serverData === " ") {
				this.customSettings.upload_successful = false;
			} else {
				this.customSettings.upload_successful = true;
			}
		}catch(e) {}
	}
	swfu.uploadComplete = function(file) {
		try {
			if (this.customSettings.upload_successful) {
				this.setButtonDisabled(true);
				
				uploadAndCutImageObj.img.onload = uploadAndCutImagePre;
				uploadAndCutImageObj.img.src = '../uploads/temp/'+$('popCutImageFileName').value;
				
			} else {							
				$('popCutImageFileName').value = "";
			}
		} catch (e) {
		}
	}
		
	uploadAndCutImageObj.campo = campo;
	uploadAndCutImageObj.local = local;
	uploadAndCutImageObj.x = x;
	uploadAndCutImageObj.y = y;
};
uploadAndCutImageObj.img = new Image();
uploadAndCutImagePre = function() {
	if (uploadAndCutImageObj.img.width < uploadAndCutImageObj.x || uploadAndCutImageObj.img.height < uploadAndCutImageObj.y) {
		alert('O arquivo enviado é muito pequeno!\nO tamanho mínimo para esta imagem é de:<br /><b>'+uploadAndCutImageObj.x+'</b> x <b>'+uploadAndCutImageObj.y+'</b>');
		return false;
	}
	var obj = $('divCutImage');
	obj.scrollLeft = 0;
	obj.scrollTop = 0;
	$('imgCutImage').src = uploadAndCutImageObj.img.src;
	uploadAndCutImageObj.zomm = 100;
	$('imgCutImage').style.width = uploadAndCutImageObj.img.width+'px';
	$('imgCutImage').style.height = uploadAndCutImageObj.img.height+'px';
};

uploadAndCutImageMouseDown = function(ev) {
	ev = ev || event;
	var obj = $('divCutImage');
	var MC = basic.getMousePos(ev);
	uploadAndCutImageObj.down = true;
	uploadAndCutImageObj.difX = 0 - obj.scrollLeft - MC.x;
	uploadAndCutImageObj.difY = 0 - obj.scrollTop - MC.y;
	return false;
};
uploadAndCutImageMouseMove = function(ev) {
	var obj = $('divCutImage');
	var MC = basic.getMousePos(ev);
	if (uploadAndCutImageObj.down) {
		var newX = (MC.x + uploadAndCutImageObj.difX)*-1;
		var newY = (MC.y + uploadAndCutImageObj.difY)*-1;
		obj.scrollLeft = newX;
		obj.scrollTop = newY;
		return true;
	}
};
uploadAndCutImageZoom = function(tx) {
	uploadAndCutImageObj.zomm += tx;
	if (uploadAndCutImageObj.zomm > 100) uploadAndCutImageObj.zomm = 100;
	if (uploadAndCutImageObj.zomm < 10) uploadAndCutImageObj.zomm = 10;
	var newX = uploadAndCutImageObj.img.width*uploadAndCutImageObj.zomm/100;
	var newY = uploadAndCutImageObj.img.height*uploadAndCutImageObj.zomm/100;
	if (newX < uploadAndCutImageObj.x || newY < uploadAndCutImageObj.y) {
		uploadAndCutImageObj.zomm -= tx;
		return false;
	}
	$('imgCutImage').style.width = newX+'px';
	$('imgCutImage').style.height = newY+'px';
	var obj = $('divCutImage');
	obj.scrollLeft = obj.scrollLeft+1;
	obj.scrollLeft = obj.scrollLeft-1;
	obj.scrollTop = obj.scrollTop+1;
	obj.scrollTop = obj.scrollTop-1;
};
cutImage = function() {
	var obj = $('divCutImage');
	var ajaxCurImage = new ajax({
		'method': 'POST',
		'url': 'cutImageXML.php',
		'oncomplete': function() {
			this.setChildName('return');
			
			if (this.getAttByName('nova',0) != 0) {
				
				$(uploadAndCutImageObj.campo).value = this.getAttByName('nova',0);
				
			} else this.onerror();
			
			$S('spanCutImage').display = "none";
			delete ajaxCurImage;
		},
		'onerror': function(){
			alert('Não foi possivel salvar a imagem!\nFavor tentar novamente mais tarde.');
		}
	});
	ajaxCurImage.push('acao','cortar');
	ajaxCurImage.push('tamX',uploadAndCutImageObj.x);
	ajaxCurImage.push('tamY',uploadAndCutImageObj.y);
	ajaxCurImage.push('posX',obj.scrollLeft);
	ajaxCurImage.push('posY',obj.scrollTop);
	ajaxCurImage.push('zoom',uploadAndCutImageObj.zomm);
	ajaxCurImage.push('local',uploadAndCutImageObj.local);
	ajaxCurImage.push('imagem',uploadAndCutImageObj.src);
	ajaxCurImage.open();	
};