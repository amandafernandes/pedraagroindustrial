<?
	require_once("inc/auth.php");
	require_once("../inc/conn.php");	
	
	function writeTxt($arq,$txt){	
		if ($hd = fopen($arq,'a')) {
			fwrite($hd, $txt);
			fclose($hd);
		}
	}
	
	$sql1 = "SELECT
				id as Id,
				cadastro_tipo as Tipo,
				cad1nome as Nome,
				cad1cpf as CPF,
				cad1rg as RG,
				cad1nascimento as Nascimento,				
				cad1escolaridade as Escolaridade,
				cad1estadocivil as Civil,
				cad1filhos as Filhos,				
				cad1pai as Pai,
				cad1mae as Mae,
				cad2cep as CEP,
				cad2endereco as Endereco,
				cad2numero as Numero,				
				cad2complemento as Complemento,
				cad2moradesde as MoraDesDe,
				cad2bairro as Bairro,
				cad2idcidade as DNE,				
				cad2cidade as Cidade,
				cad2estado as Estado,
				cad2referencia as Referencia,
				cad2residencial as TelResidencial,
				cad2celular as TelCelular,
				cad2comercial as TelComercial,
				cad2ramal as Ramal,
				cad2email as Email,
				cad3titular as PrimeiroTitular,
				cad3cpf as CPFPrimeiroTitular,
				cad3segundotitular as SegundoTitular,
				cad3segundocpf as CEFSegundoTitular,
				cad3nomebanco as Banco,
				cad3agencia as Agencia,
				cad3conta as Conta,
				cad3dataabertura as DatadeAbertura,
				cad3limitecredito as LimiteCredito,
				cad3chequeespecial as ChequeEspecial,
				cad3talaocheque as TalaodeCheque,
				cad4rendamensal as Rendamensal,
				cad5trabalhaporconta as TrabalhaporConta,
				cad5tipotrabalho as TipoTrabalho,
				cad5quantotempo as QuantoTempo,
				cad5rendapropria as RendaPropria,
				cad5trabalhacomofuncionario as TrabalhaComoFuncionario,
				cad5empresa as Empresa,
				cad5cargo as Cargo,
				cad5admissao as Admissao,
				cad5rendafuncionario as RendaFuncionario,
				cad5telefone as TelefoneTabalhoFun,
				cad5vendeproduto as VendeProduto,
				cad5produtos as Produtos,
				cad5rendamensal as RendaMensal,
				cad6razao as RazaoSocial,
				cad6inicioatividades as IninicioAtividades,
				cad6cnpj as CNPJ,
				cad6inscricao as IE,
				cad6cep as CEPEmpresa,
				cad6endereco as EnderecoEmpresa,
				cad6numero as NumeroEmpresa,
				cad6complemento as ComplementoEmpresa,
				cad6bairro as BairroEmpresa,
				cad6idcidade as DNEEmpresa,
				cad6cidade as CidadeEmpresa,
				cad6estado as EstadoEmpresa,
				cad7nome as NomeMarido,
				cad7cpf as CPFMarido,
				cad7rg as RGMarido,
				cad7nascimento as NascimentoMarido,
				cad7escolaridade as EscolaridadeMarido,
				cad7trabalhaporconta as TrabalhaPorContaMarido,
				cad7tipotrabalho as TipoTrabalhoMarido,
				cad7quantotempo as QuantoTempoMarido,
				cad7rendaporconta as RendaporContaMarido,
				cad7trabalhacomofuncionario as TrabalhaComoFuncionarioMarido,
				cad7empresa as EmpresaMarido,
				cad7cargo as CargoMarido,
				cad7admissao as AdmissaoMarido,
				cad7rendafuncionario as RendaFuncionarioMarido,
				cad7telefone as TelefoneMarido,
				cad8ficousabendo as FicouSabendoMarido,
				cad9pagamento as MeioPagamento,
				date_format(data_cadastro,'%d/%d/%Y') as DataCadastro,
				ip as IP
		    FROM
				cadastro".$_SESSION['exSql']."
			LIMIT 1";
			
	$sql2 = "SELECT
				id,
				cadastro_tipo,
				cad1nome,
				cad1cpf,
				cad1rg,
				date_format(cad1nascimento,'%d%m%Y') as cad1nascimento,
				cad1escolaridade,
				cad1estadocivil,
				cad1filhos,				
				cad1pai,
				cad1mae,
				cad2cep,
				cad2endereco,
				cad2numero,				
				cad2complemento,
				cad2moradesde,
				cad2bairro,
				cad2idcidade,				
				cad2cidade,
				cad2estado,
				cad2referencia,
				cad2residencial,
				cad2celular,
				cad2comercial,
				cad2ramal,
				cad2email,
				cad3titular,
				cad3cpf,
				cad3segundotitular,
				cad3segundocpf,
				cad3nomebanco,
				cad3agencia,
				cad3conta,
				cad3dataabertura,
				cad3limitecredito,
				cad3chequeespecial,
				cad3talaocheque,
				cad4rendamensal,
				cad5trabalhaporconta,
				cad5tipotrabalho,
				cad5quantotempo,
				cad5rendapropria,
				cad5trabalhacomofuncionario,
				cad5empresa,
				cad5cargo,
				cad5admissao,
				cad5rendafuncionario,
				cad5telefone,
				cad5vendeproduto,
				cad5produtos,
				cad5rendamensal,
				cad6razao,
				cad6inicioatividades,
				cad6cnpj,
				cad6inscricao,
				cad6cep,
				cad6endereco,
				cad6numero,
				cad6complemento,
				cad6bairro,
				cad6idcidade,
				cad6cidade,
				cad6estado,
				cad7nome,
				cad7cpf,
				cad7rg,
				date_format(cad7nascimento,'%d%m%Y') as cad7nascimento,
				cad7escolaridade,
				cad7trabalhaporconta,
				cad7tipotrabalho,
				cad7quantotempo,
				cad7rendaporconta,
				cad7trabalhacomofuncionario,
				cad7empresa,
				cad7cargo,
				cad7admissao,
				cad7rendafuncionario,
				cad7telefone,
				cad8ficousabendo,
				cad9pagamento,
				date_format(data_cadastro,'%d%m%Y') as data_cadastro,
				ip as IP
		    FROM
				cadastro".$_SESSION['exSql'];			
			
		$qry1 = mysql_query($sql1)or die(mysql_error());
		$qry2 = mysql_query($sql2)or die(mysql_error());
		
		#print("<pre>");
		#$arr1 = print_r(mysql_fetch_assoc($qry1));
		
		$txtH = "";
		
		if(mysql_num_rows($qry2) > 0){
		
			$arr = mysql_fetch_assoc($qry1);
			
			foreach($arr as $key => $valor){
				$txtH .= $key."##";
			}
			
			$txtH.= chr(13).chr(10);
			
			while($arr2 = mysql_fetch_assoc($qry2)){
				foreach($arr2 as $key => $valor){
					$txtH .= utf8_encode($valor)."##";
				}
				$txtH .= chr(13).chr(10);
			}
		
		}
		
		$nomes = "cadastrados".date("dmYHis").".txt";		
		$file = "temp/".$nomes;
		
		$diretorio = "temp/"; // pega o endereco do diretorio
		$ponteiro  = opendir($diretorio); // ponteiro que ira percorrer a pasta
		
		while ($nome_itens = readdir($ponteiro)) { // monta o vetor com os itens da pasta
			$itens[] = $nome_itens;
		}
		
		sort($itens); // ordena o vetor de itens
		
		foreach ($itens as $listar) {  //percorre o vetor para fazer a separacao entre arquivos e pastas
			if ($listar!="." && $listar!=".."){ // retira os itens "./" e "../" para que retorne apenas pastas e arquivos
				if (is_dir($listar)) { // checa se � uma pasta
					$pastas[]=$listar; // caso VERDADEIRO adiciona o item ao vetor de pastas
				} else{
					$arquivos[]=$listar;// caso FALSO adiciona o item ao vetor de arquivos
				}
			}
		}
		
		if($arquivos != ''){
			foreach($arquivos as $arquivo){// lista os arquivos
				@unlink($diretorio.$arquivo);
			}	
		}

		writeTxt($file,$txtH);
		
		header("Content-type: application/save");
		header("Content-Length:".filesize($file));
		header('Content-Disposition: attachment; filename="' . $nomes . '"');        
		header('Expires: 0');
		header('Pragma: no-cache');
		readfile($file);
?>
