<?
	require_once("topo.php");
?>

	<!-- Content -->
	<div id="content" class="colMS">
		<h1>Relatórios Disponíveis</h1>
		<div id="content-main">
			<?
			if (count($_SESSION['userextFiliais']) == 0) {
				?>
				<div class="module">
					<table>
					<caption>
						Relatórios
					</caption>
					<tr>
						<th scope="row">Nenhum relatório liberado para este usuário</th>
					</tr>					
					</table>
				</div>
				<?
			} else {
				for ($i=0; $i<count($_SESSION['userextFiliais']); $i++){
					$rs = pg_query("SELECT * FROM usfilial WHERE id_filial = ".$_SESSION['userextFiliais'][$i]);
					$rr = pg_fetch_assoc($rs);
					?>
					<div class="module">
						<table>
						<caption>
							<?=$rr['nome']?>
						</caption>

						<?
						for ($j=0; $j<count($_SESSION['userextRelatorios'][$i]); $j++){
							$rsr = pg_query("SELECT * FROM relatorios WHERE id = ".$_SESSION['userextRelatorios'][$i][$j]);
							$rrr = pg_fetch_assoc($rsr);
							?>
							<tr>
								<th scope="row"><a href="relatorio.php?id=<?=$rrr['arquivo']?>&idr=<?=$rrr['id']?>&idf=<?=$_SESSION['userextFiliais'][$i]?>"><?=$rrr['nome']?></a></th>
								<td><a href="relatorio.php?id=<?=$rrr['arquivo']?>&idr=<?=$rrr['id']?>&idf=<?=$_SESSION['userextFiliais'][$i]?>" class="changelink">VER</a></td>
							</tr>
							<?
						}
						?>
						</table>
					</div>
					<?
				}
			}
			?>

			<div class="module">
				<table>
				<caption>
				Arquivos para Download
				</caption>

				<tr>
					<th scope="row"><a href="download.php?arquivo=nutricao.pdf" target="_blank">PROGRAMA DE FERTILIZAÇÕES DA CANA-DE-AÇÚCAR</a></th>
					<td><a href="arquivos/nutricao.pdf" class="changelink" target="_blank">VER</a></td>
				</tr>
				<tr>
					<th scope="row"><a href="download.php?arquivo=sphenophorus.pdf" target="_blank">PRAGAS DA CANA-DE-AÇÚCAR</a></th>
					<td><a href="arquivos/sphenophorus.pdf" class="changelink" target="_blank">VER</a></td>
				</tr>
				<tr>
					<th scope="row"><a href="download.php?arquivo=variedades_usina_da_pedra_usina_ibira.pdf" target="_blank">VARIEDADES DE CANA-DE-AÇÚCAR</a></th>
					<td><a href="arquivos/variedades_usina_da_pedra_usina_ibira.pdf" class="changelink" target="_blank">VER</a></td>
				</tr>
				<tr>
					<th scope="row"><a href="download.php?arquivo=normas_e_procedimentos_de_protecao_ao_meio_ambiente2.pdf" target="_blank">NORMAS E PROCEDIMENTOS DE PROTEÇÃO AO MEIO AMBIENTE</a></th>
					<td><a href="arquivos/normas_e_procedimentos_de_protecao_ao_meio_ambiente2.pdf" class="changelink" target="_blank">VER</a></td>
				</tr>				
				</table>
			</div>

			
		</div>
	</div>

<?
	require_once("rodape.php");
?>
