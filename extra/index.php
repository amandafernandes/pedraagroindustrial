﻿<?
	require_once("inc/config.php");
	require_once("inc/conn.php");
	require_once("inc/funcoes.php");
	require_once("inc/db_class.php");
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
		if($_GET['acao'] == "login"){
			$sql = "
				SELECT
					login_filial.liberado as liberado,
					usfilial.nome as nome,
					usfilial.id_filial as id_filial,
					usfilial.cgc_cpf as cnpjcpf,
					usfilial.digito as digito
				FROM
					usfilial
					Right Join login_filial ON usfilial.id_filial = login_filial.id_filial
				WHERE 
					usfilial.cgc_cpf || usfilial.digito = '".$DB->escapeString($_POST['username'])."' and usfilial.senha = md5('".$DB->escapeString($_POST['password'])."') and usfilial.senha <> '';";

			$qLogin = $DB->query($sql);
			
			$rels = array();
			$tmp = array();
			$fils = array();
			
			if($DB->numRows($qLogin) > 0){
				$arrLogin = $DB->fetchAssoc($qLogin);

				if ($arrLogin['liberado']==1){
				
					//pegando as empresas e relatorios liberados para o fornecedor atual
					$SQL = "
						SELECT
							DISTINCT usfilial_acesso.filial_acesso as filial_acesso
						FROM
							usfilial
							Inner Join usfilial_acesso ON usfilial.id_filial = usfilial_acesso.filial_acesso
						WHERE
							usfilial_acesso.filial_principal = ".$arrLogin['id_filial'];
					$rs = $DB->query($SQL);
					while($rr = $DB->fetchAssoc($rs)){
						array_push($fils, $rr['filial_acesso']);
					
						$tmp = limpaArray($tmp);

						$SQL2 = "SELECT * FROM usfilial_acesso WHERE filial_acesso = ".$rr['filial_acesso']." AND filial_principal = ".$arrLogin['id_filial'];
						$qrel = $DB->query($SQL2);
						while($rrel = $DB->fetchAssoc($qrel)){
							array_push($tmp, $rrel['relatorio']);
						}
						
						array_push($rels, $tmp);
					}
				
					$_SESSION['userextLogado'] = true;
					//$_SESSION['userextLogin'] = $arrLogin['login'];
					$_SESSION['userextNome'] = $arrLogin['nome'];
					$_SESSION['userextAuthUserId'] = $arrLogin['id_filial'];
					$_SESSION['userextFiliais'] = $fils;
					$_SESSION['userextRelatorios'] = $rels;
					$_SESSION['filialCNPJCPF'] = $arrLogin['cnpjcpf'];
					$_SESSION['filialDIGITO'] = $arrLogin['digito'];
					
					die("<script>location = 'home.php';</script>");
				} else {
					$error = "Por favor, entre com um código de acesso e senha corretos. Note que ambos os campos diferenciam maiúsculas e minúsculas.";
				}
			}else{
				$error = "Por favor, entre com um código de acesso e senha corretos. Note que ambos os campos diferenciam maiúsculas e minúsculas.";
			}
		}
	} else {
		$redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		header("Location: $redirect");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br" >
<head>
	<title><?=write($SETTINGS['APPNAME'], "PUB DESIGN - PHDJANGO")?></title>
	<link rel="stylesheet" type="text/css" href="files/login.css" />
	<script src="files/js/commom.js" type="text/javascript"></script>
	<script src="files/js/connecting.js" type="text/javascript"></script>
	<script src="files/js/index.js" type="text/javascript"></script>
	<style>
	#aguarde {position:relative; display:none; width:90px; z-index:998; border-style:solid; border-width:10px 1px 1px 1px; border-color:#CCCCCC; background:white}
	#aguarde DIV{position:relative;float:left;line-height:30px;margin-left:5px;height:30px;}
	#aguarde DIV IMG{margin-left:7px;margin-top:7px;}
	</style>
	<meta name="robots" content="NONE,NOARCHIVE" />
</head>
<body class="login">
<!-- Container -->
<div id="container">
	<!-- Header -->
	<div id="header">
		<div id="branding">
			<h1 align="center" id="site-name"><img src="img/<?=write($SETTINGS['LOGO'], "logo.jpg")?>" /></h1>
	  </div>
	</div>
	<!-- END Header -->
	<!-- Content -->
	<div id="content" class="colM">
		<? if($error != ""){ ?>
		<p class="errornote" id="erro_txt"><?=$error?></p>
		<? }?>
		<p class="errornote" id="erro_email" style="display:none;">Não foi encontrado email cadastrado em nossa base de dados para o envio de sua senha.<br>Entre em contato com a área de relacionamento com fornecedores e parceiros de sua unidade.</p>
		<div id="content-main">
			<form action="index.php?acao=login" method="post" id="login-form" onsubmit="return false;">
				<div class="form-row">
					<label for="id_username">Código Acesso:</label>
					<input type="text" name="username" id="id_username" />
				</div>
				<div class="form-row" id="email_confirm" style="display:none">
					<label for="id_email">E-mail:</label>
					<input type="text" name="mail" id="id_email" />
				</div>
				<div class="form-row">
					<label for="id_password"><span id="label_senha">Senha:</span></label>
					<input type="password" name="password" id="id_password" />
				</div>
				<div class="form-row" id="senha_nova" style="display:none">
					<label for="id_passwordn">Senha Nova:</label>
					<input type="password" name="passwordn" id="id_passwordn" />
				</div>
				<div class="submit-row">
					<input type="button" value="Acessar" id="btn_acessar" onclick="document.login-form.submit();" /><input type="button" value="Enviar" id="bt_enviar" style="display:none;" onclick="ESQUECI.send();" /><input type="button" value="Trocar" id="bt_trocar" style="display:none;" onclick="TROCA.send();" /><input type="button" value="Cancelar" id="bt_cancelar" style="display:none;" onclick="ESQUECI.hide();TROCA.hide();" />
				</div>
			</form>
			<hr>
			<center>
				<input type="button" value="Esqueci minha senha" style="width:150px;" onclick="ESQUECI.show();" />&nbsp;&nbsp;&nbsp;
				<input type="button" value="Trocar senha" style="width:100px;" onclick="TROCA.show();" />
				<div id="aguarde">
					<div><img src="img/loading.gif" border=0></div>
					<div class="textoCinza">Aguarde</div>
				</div>
				<script type="text/javascript">
					document.getElementById('id_username').focus()
				</script>
				<br><br>
				<a href="http://www.pedraagroindustrial.com.br/">Ir para o site</a>
			</center>
		</div>
		<br class="clear" />
	</div>
	<!-- END Content -->
	<div id="footer"></div>
</div>
<!-- END Container -->
</body>
</html>