<?		

	header('Content-Type: text/html; charset=utf-8');
	
	if ($AUTH != false) require_once("inc/auth.php");

	require_once("inc/db_class.php");
	
	//if($TYPE == 'FORM') require_once("inc/form_class.php");
	
	if($TYPE == 'FORM') $CSSBODY = 'change-form';
	
	if($TYPE == 'FORM') $JSADMIN = array('gettext.js','core.js','RelatedObjectLookups.js','urlify.js','calendar.js','DateTimeShortcuts.js','CollapsedFieldsets.js','swfupload.js','util.js','ajax.js','eventos.js','cutImage.js');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br" >
<head>
	<title><?=write($SETTINGS['APPNAME'], "PUB DESIGN - PHDJANGO")?></title>
	<link rel="stylesheet" type="text/css" href="files/dashboard.css" />
    <? if($TYPE == 'FORM') {?>
    <link rel="stylesheet" type="text/css" href="files/forms.css" />
    <? } else {?>
	<link rel="stylesheet" type="text/css" href="files/changelists.css" />
    <? }?>
<? for($i = 0; $i<count($JSADMIN); $i++){ ?>
	<SCRIPT src="files/<?=$JSADMIN[$i]?>" type="text/javascript"></SCRIPT>
<? }?>
<? for($i = 0; $i<count($JS); $i++){ ?>
	<SCRIPT src="files/js/<?=$JS[$i];?>" type="text/javascript"></SCRIPT>
<? } ?>
	<meta name="robots" content="NONE,NOARCHIVE" />
</head>


<body class="<?=write($CSSBODY,'dashboard');?>">

<!-- Container -->
<div id="container">

    
<!-- Header -->
<div id="header" style="background-image:url(img/fundoDemostrativos.gif); height:118px; background-repeat:repeat-x;">
	<div id="branding">
		<img src="img/<?=write($SETTINGS['LOGO'], "logo3.jpg")?>" style="margin-left:10px;" /><br />
	</div>

	<? if ($_SESSION['userextNome'] != "") { ?>
		<div id="user-tools">Bem vindo, <strong><?=$_SESSION['userextNome'];?></strong>. <a href="logout.php">Encerrar sessão</a><br><a href="../">Ir para o site</a></div>
	<? } ?>
</div>

    <!-- END Header -->
	<?
	if(isset($AREA) &&  $AREA!=''){
		?>
		<div class="breadcrumbs"><a href="home.php">Início</a> &rsaquo; <?=$AREA;?></div>
		<?
	}
	if($IDERROR){
		?>
		<ul class="messagelist"><li><?=$ERROS[$IDERROR];?></li></ul>
		<?
	}
	?>