<?
	require_once("topo.php");
?>

	<!-- Content -->
	<div id="content" class="colMS">
		<h1>Relatórios Disponíveis</h1>
		<div id="content-main">
			<?
			if (count($_SESSION['userextFiliais']) == 0) {
				?>
				<div class="module">
					<table>
					<caption>
						Relatórios
					</caption>
					<tr>
						<th scope="row">Nenhum relatório liberado para este usuário</th>
					</tr>					
					</table>
				</div>
				<?
			} else {
				for ($i=0; $i<count($_SESSION['userextFiliais']); $i++){
					$rs = pg_query("SELECT * FROM usfilial WHERE id_filial = ".$_SESSION['userextFiliais'][$i]);
					$rr = pg_fetch_assoc($rs);
					?>
					<div class="module">
						<table>
						<caption>
							<?=$rr['nome']?>
						</caption>

						<?
						for ($j=0; $j<count($_SESSION['userextRelatorios'][$i]); $j++){
							$rsr = pg_query("SELECT * FROM relatorios WHERE id = ".$_SESSION['userextRelatorios'][$i][$j]);
							$rrr = pg_fetch_assoc($rsr);
							?>
							<tr>
								<th scope="row"><a href="relatorio.php?id=<?=$rrr['arquivo']?>&idr=<?=$rrr['id']?>&idf=<?=$_SESSION['userextFiliais'][$i]?>"><?=$rrr['nome']?></a></th>
								<td><a href="relatorio.php?id=<?=$rrr['arquivo']?>&idr=<?=$rrr['id']?>&idf=<?=$_SESSION['userextFiliais'][$i]?>" class="changelink">VER</a></td>
							</tr>
							<?
						}
						?>
						</table>
					</div>
					<?
				}
			}
			?>

			<div class="module">
				<table>
				<caption>
				Arquivos para Download
				</caption>

				<?
				$rs = pg_query("
					select 
						a.id_arq_restr as id,
						a.nome as nome,
						a.fg_pub_emp as pemp,
						a.categ_arq as idcat,
						a.fg_pub_tp_filial as ptpfil,
						a.arquivo as arq,
						f.nome as nomecat
					from 
						adm_arq_restr a,
						adm_categ_arq f
					where 
						a.id_arq_restr in(
							select 
								b.arq_restr
							from 
								adm_filial_emp e, 
								usfilial d, 
								adm_arq_restr_empr b
							where 
								a.fg_pub_emp = 'N' and 
								b.arq_restr = a.id_arq_restr and 
								d.cgc_cpf = '".$_SESSION['filialCNPJCPF']."' and 
								d.digito = '".$_SESSION['filialDIGITO']."' and 
								e.filial = d.id_filial and 
								e.empresa_prop = b.empresa_prop
							union
								select 
									a.id_arq_restr
								where 
									a.fg_pub_emp = 'S'
						) and 
						a.id_arq_restr in(    
							select 
								c.arq_restr
							from 
								adm_filial_emp e, 
								usfilial d, 
								adm_arq_restr_tp_filial c
							where 
								a.fg_pub_tp_filial = 'N' and 
								c.arq_restr = a.id_arq_restr and 
								d.cgc_cpf = '".$_SESSION['filialCNPJCPF']."' and 
								d.digito = '".$_SESSION['filialDIGITO']."' and 
								e.filial = d.id_filial and 
								e.tp_filial = c.tp_filial
							union
								select 
									a.id_arq_restr
								where 
									a.fg_pub_tp_filial = 'S'
						)  and f.id_categ_arq = a.categ_arq
						Order By
							f.nome,
							a.nome
							
				");
				$n = pg_num_rows($rs);
				if ($n > 0) {
					$nomecat = '';
					while ($rr = pg_fetch_assoc($rs)) {
						if ($nomecat != $rr['nomecat']) {
							?>
							<tr>
								<td><b><?=strtoupper($rr['nomecat'])?></b></td>
							</tr>
							<?
							$nomecat = $rr['nomecat'];
						}
						?>
						<tr>
							<th scope="row"><a href="download2.php?arquivo=<?=$rr['arq']?>" target="_blank"><?=strtoupper($rr['nome'])?></a></th>
							<td><a href="../admin/arquivos/<?=$rr['arq']?>" class="changelink" target="_blank">VER</a></td>
						</tr>
						<?
					}
				} else {
					?>
					<tr>
						<th scope="row">Nenhum arquivo liberado para este usuário</th>
					</tr>	
					<?
				}
				?>
				</table>
			</div>

			
		</div>
	</div>

<?
	require_once("rodape.php");
?>
