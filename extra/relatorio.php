﻿<?	
	header('Content-Type: text/html; charset=utf-8');

	session_start();

	if(($_SESSION['adminLogado'] != true && $_SESSION['userextLogado'] != true) || $_GET['id']=='' || $_GET['idr']=='' || $_GET['idf']==''){
		session_destroy();
		die("<script>location = 'index.php';</script>");
	}

	$RELATORIO = $_GET['id'];
	$RELATORIO_ID = $_GET['idr'];
	$FORNECEDOR_ID = $_GET['idf'];

	require_once("inc/db_class.php");

	//se fornecedor estiver logado e tiver permissao
	if ($_SESSION['userextLogado'] == true && hasPermissionReport($RELATORIO_ID,$RELATORIO,$FORNECEDOR_ID)==false){
		die("Você não tem permissão para ver este relatório. Um log foi gravado e será avaliado. 1");
		exit;
	}
	
	if ($FORNECEDOR_ID!='ALL'){
		//se admin estiver logado e tiver permissao
		if($_SESSION['adminLogado'] == true && hasPermissionAdminReport($RELATORIO_ID,$RELATORIO,$FORNECEDOR_ID)==false){
			die("Você não tem permissão para ver este relatório. Um log foi gravado e será avaliado. 2");
			exit;
		}
	}

	include("../admin/topo_relatorio.php");
	
	$x = getExt($RELATORIO);
	
	if ($x == 'jpg') {
		?>
		<img src="../admin/relatorios/<?=$RELATORIO;?>" border="0" />
		<?
	} else {
		include("../admin/relatorios/".$RELATORIO);
	}
?>