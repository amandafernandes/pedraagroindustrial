﻿<?
session_start();

if ($_SESSION['idioma']=='') {
	$idioma = 'US';
} else {
	$idioma = $_SESSION['idioma'];
}

require_once("inc/conn.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;">
		<div style="width:970px; margin:0 auto;">
			<? require_once("topo.html"); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Press Room</span>

							<br />
							<br />
							<table width="646" border="0" cellpadding="0" cellspacing="0" background="img/fundoOpcoesLogin.gif">
							<tr>
								<td height="73" valign="top" style="padding-top:2px;">
									<table width="97%" border="0" align="right" cellpadding="0" cellspacing="0">
									<tr>
										<td height="32"><span style="font-size:13px; font-weight:bold;">  Select news by category  </span></td>
									</tr>
									<tr>
										<td height="35">
											<table width="97%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="100%" align=center>
													<form name="frmunidade" method="post" action="" style="margin:0px;">
													<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<select name="categoriaid" class="formularioPadrao">
																<option value="0" <?if(isset($_POST['categoriaid']) && $_POST['categoriaid']==0){echo 'selected';}?>>All categories</option>
																<?
																$rs = pg_query("SELECT * FROM categorias_releases WHERE idioma = '".$idioma."' ORDER BY titulo ASC");
																if(pg_num_rows($rs)){
																	while($rr=pg_fetch_assoc($rs)){
																		?>
																		<option value="<?=$rr['id']?>" <?if(isset($_POST['categoriaid']) && $_POST['categoriaid']==$rr['id']){echo 'selected';}?>><?=$rr['titulo']?></option>
																		<?
																	}
																}
																?>
															</select>
														</td>
														<td>&nbsp;<img src="img/botaoOpcaoLogin.gif" alt="" align=absmiddle style="cursor:pointer;" onclick="document.frmunidade.submit();" /></td>
													</tr>
													</table>
													</form>
												</td>
											</tr>
											</table>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>   
							<br /><br />


						  <?
							$rsi = pg_query("select categorias_releases.id from categorias_releases where categorias_releases.idioma = '".$idioma."'");
							while($rri = pg_fetch_assoc($rsi)){
								if ($idcats != '') $idcats .= ",";
								$idcats .= $rri['id'];
							}

							if ($_POST['categoriaid'] != 0) {
								$whereSQL = "WHERE idcategoria=".$_POST['categoriaid'];
							} else {
								$whereSQL = "WHERE idcategoria IN (".$idcats.")";
							}
							$rs = pg_query("select * from releases ".$whereSQL." order by data desc") ;
							if(pg_num_rows($rs)){
								while($rr=pg_fetch_assoc($rs)){
									?>

                            <table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
							  <?
								if ($rr['thumb'] != '') {
								?>
                                <td width="23%" valign="top"><table width="150" border="0" cellpadding="0" cellspacing="0" style="margin-right:13px;">
                                  <tr>
                                    <td height="110" style="background-color:#d5d2b1;"><div align="center"><a href="saladeimprensa2.php?id=<?=$rr['id'];?>"><img src="../adminsite/uploads/releases/<?=$rr['thumb'];?>" alt="" width="140" height="100" border="0" /></a></div></td>
                                  </tr>
                                </table>                                  </td>
								<?
								} else {
									?>
									<td width="2%" valign="top">&nbsp;</td>
									<?
								}
								?>
                                <td width="77%" valign="top" style="padding-top:5px;"><span style="font-size:13px; color:#a5a15a; line-height:12px;"><?=dataeuabr($rr['data']);?></span><br />
								<div style="font-size:15px; color:#566336; margin-top:0px; margin-bottom:4px;"><a href="saladeimprensa2.php?id=<?=$rr['id'];?>" class="linkSaladeImprensa"><?=$rr['titulo'];?></a></div>
								<?=$rr['resenha'];?>
								<div align="right" style="margin-top:8px; font-weight:bold;"><a href="saladeimprensa2.php?id=<?=$rr['id'];?>">  See full version  </a></div></td>
                              </tr>
                          </table>
                            <br />
									<?
								}
							}
						?>	

 
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
