<?
require_once("inc/connXML.php");

function lerArquivo($url){
	$sc = curl_init();
	curl_setopt($sc, CURLOPT_URL, $url);
	curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($sc, USERAGENT, "Secretaria do Meio Ambiente Estado de São Paulo! v0.1b");
	$conteudo = curl_exec($sc);
	curl_close($sc);
	return $conteudo;
}

print('<?xml version="1.0" encoding="UTF-8" ?>'.LINE);

print("<cidades>".LINE);

	$url = 'http://servicos.cptec.inpe.br/XML/cidade/'.$_GET['id'].'/previsao.xml';
	print(lerArquivo($url));

print("</cidades>".LINE);
?>