﻿<?
@session_start();
require_once("inc/conn.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
<style>
.cabecalho{
	width:100%;
	height:30px;
	background:#EFEFEF;
}
.cabecalho DIV{
	width:46px;
	height:30px;
	line-height:30px;
	border-right:1px solid #4f6718;
	float:left;
	font-size:13px;
	text-align:right;
}
.registros{
	width:100%;
	height:25px;
}
.registros DIV{
	width:46px;
	height:25px;
	line-height:25px;
	border-right:1px solid #4f6718;
	float:left;
	font-size:11px;
	text-align:right;
}
.total{
	width:100%;
	height:25px;
	background:#EFEFEF;
}
.total DIV{
	width:46px;
	height:25px;
	line-height:25px;
	border-right:1px solid #4f6718;
	float:left;
	font-size:11px;
	text-align:right;
}
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;">
		<div style="width:970px; margin:0 auto;">
			<? require_once("topo.html"); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; max-width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Rainfall</span><br>
                          	<br>
<br>


<table width="646" border="0" cellpadding="0" cellspacing="0" background="img/fundoOpcoesLogin.gif">
<tr>
	<td height="73" valign="top" style="padding-top:2px;">
		<table width="97%" border="0" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td height="32"><span style="font-size:13px; font-weight:bold;">Select the average per unit</span></td>
		</tr>
		<tr>
			<td height="35">
				<table width="97%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" align=center>
						<form name="frmunidade" method="get" action="" style="margin:0px;">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<select name="idemp" class="formularioPadrao" onchange="document.frmunidade.submit();">
<?
$rs = pg_query("
	SELECT
		id_empresa_prop as id,
		nome_filial as nm
	FROM usempresa_prop
    where id_empresa_prop in (1,11,31)
");
if(pg_num_rows($rs)){
	while($rr=pg_fetch_assoc($rs)){
		?>
		<option value="<?=$rr['id']?>" <?if(isset($_GET['idemp']) && $_GET['idemp']==$rr['id']){echo 'selected';}?>><?=$rr['nm']?></option>
		<?
	}
}																
?>
								</select>
							</td>
						</tr>
						</table>
						</form>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>   
<br /><br />




							<?
							$idatual = ($_GET['idemp']!='')?$_GET['idemp']:1;

							$rs = pg_query("
								SELECT
									usempresa_prop.nome_filial,
									SUM(cachuva.jan) as tjan,
									SUM(cachuva.fev) as tfev,
									SUM(cachuva.mar) as tmar,
									SUM(cachuva.abr) as tabr,
									SUM(cachuva.mai) as tmai,
									SUM(cachuva.jun) as tjun,
									SUM(cachuva.jul) as tjul,
									SUM(cachuva.ago) as tago,
									SUM(cachuva.sep) as tsep,
									SUM(cachuva.oct) as toct,
									SUM(cachuva.nov) as tnov,
									SUM(cachuva.dez) as tdez
								FROM
									cachuva
									Inner Join usempresa_prop ON usempresa_prop.id_empresa_prop = cachuva.empresa_prop
								WHERE
									usempresa_prop.id_empresa_prop =  ".$idatual." and
									(cachuva.ano >= ".(date("Y")-10)." and cachuva.ano < ".date("Y").")
								GROUP BY
									usempresa_prop.nome_filial
								LIMIT
									10 offset 0
							");
							$rr=pg_fetch_assoc($rs)
							?>
							
							
							<div style="font-size:14px; color:#4f6718; line-height:15px;"><?=$rr['nome_filial']?></div>
							<br>
							Média dos últimos 10 anos:<br>
							<div class="cabecalho">
								<div style="width:31px;">Ano&nbsp;</div>
								<div>JAN.&nbsp;</div>
								<div>FEV.&nbsp;</div>
								<div>MAR.&nbsp;</div>
								<div>ABR.&nbsp;</div>
								<div>MAI.&nbsp;</div>
								<div>JUN.&nbsp;</div>
								<div>JUL.&nbsp;</div>
								<div>AGO.&nbsp;</div>
								<div>SET.&nbsp;</div>
								<div>OUT.&nbsp;</div>
								<div>NOV.&nbsp;</div>
								<div>DEZ.&nbsp;</div>
								<div style="width:61px;">Total&nbsp;</div>
							</div>							
							<div class="total">
								<div style="width:31px; font-size:10px;">Média&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tjan']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tfev']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tmar']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tabr']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tmai']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tjun']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tjul']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tago']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tsep']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['toct']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tnov']/10),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($rr['tdez']/10),1,",",".")?>&nbsp;</div><?
									$total10 = ($rr['tjan']/10) + ($rr['tfev']/10) + ($rr['tmar']/10) + ($rr['tabr']/10) + ($rr['tmai']/10) + ($rr['tjun']/10) + ($rr['tjul']/10) + ($rr['tago']/10) + ($rr['tsep']/10) + ($rr['toct']/10) + ($rr['tnov']/10) + ($rr['tdez']/10);
								?><div style="width:61px;">&nbsp;<?=number_format($total10,1,",",".")?>&nbsp;</div>
							</div>							
							<Br>
							
							Média anual:<br>
							<?
							$rs = pg_query("
								SELECT
									usempresa_prop.nome_filial,
									cachuva.*
								FROM
									cachuva
									Inner Join usempresa_prop ON usempresa_prop.id_empresa_prop = cachuva.empresa_prop
								WHERE
									usempresa_prop.id_empresa_prop = ".$idatual." 
								ORDER BY
									usempresa_prop.nome_filial ASC, cachuva.ano DESC
							");
							$m1 = $m2 = $m3 = $m4 = $m5 = $m6 = $m7 = $m8 = $m9 = $m10 = $m11 = $m12 = $mt = $t = $t2 = $empant = 0;
							if(pg_num_rows($rs)){
								while($rr=pg_fetch_assoc($rs)){
									$tot_t = str_replace("'", "", str_replace(",", ".", str_replace(".", "", $rr['tot'])));
									if ($empant != $rr['empresa_prop']) {
										if ($t > 0) {
											?>
											<div class="total">
												<div style="width:31px; font-size:10px;">Média&nbsp;</div>
												<div>&nbsp;<?=number_format(($m1/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m2/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m3/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m4/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m5/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m6/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m7/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m8/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m9/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m10/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m11/$t2),1,",",".")?>&nbsp;</div>
												<div>&nbsp;<?=number_format(($m12/$t2),1,",",".")?>&nbsp;</div>
												<div style="width:61px;">&nbsp;<?=number_format(($mt/$t2),1,",",".")?>&nbsp;</div>
											</div>
											<br><br>
											<?
											$m1 = $m2 = $m3 = $m4 = $m5 = $m6 = $m7 = $m8 = $m9 = $m10 = $m11 = $m12 = $mt = $t2 = 0;
										}
										?>
										<div class="cabecalho">
											<div style="width:31px;">Ano&nbsp;</div>
											<div>JAN.&nbsp;</div>
											<div>FEV.&nbsp;</div>
											<div>MAR.&nbsp;</div>
											<div>ABR.&nbsp;</div>
											<div>MAI.&nbsp;</div>
											<div>JUN.&nbsp;</div>
											<div>JUL.&nbsp;</div>
											<div>AGO.&nbsp;</div>
											<div>SET.&nbsp;</div>
											<div>OUT.&nbsp;</div>
											<div>NOV.&nbsp;</div>
											<div>DEZ.&nbsp;</div>
											<div style="width:61px;">Total&nbsp;</div>
										</div>
										<?
									}
									?>
									<div class="registros" style="background:<?=(($t%2)==0)?'#DBDBDB':''?>">
										<div style="width:31px;"><?=$rr['ano']?>&nbsp;</div>
										<div><?=number_format($rr['jan'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['fev'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['mar'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['abr'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['mai'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['jun'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['jul'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['ago'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['sep'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['oct'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['nov'],1,",",".")?>&nbsp;</div>
										<div><?=number_format($rr['dez'],1,",",".")?>&nbsp;</div>
										<div style="width:61px;"><?=number_format($tot_t,1,",",".")?>&nbsp;</div>
									</div>
									<?
									$m1 += $rr['jan'];
									$m2 += $rr['fev'];
									$m3 += $rr['mar'];
									$m4 += $rr['abr'];
									$m5 += $rr['mai'];
									$m6 += $rr['jun'];
									$m7 += $rr['jul'];
									$m8 += $rr['ago'];
									$m9 += $rr['sep'];
									$m10 += $rr['oct'];
									$m11 += $rr['nov'];
									$m12 += $rr['dez'];
									$mt += $tot_t;
									$empant = $rr['empresa_prop'];
									$t++;
									$t2++;
								}
							}
							?>	 
							<div class="total">
								<div style="width:31px; font-size:10px;">Média&nbsp;</div>
								<div>&nbsp;<?=number_format(($m1/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m2/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m3/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m4/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m5/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m6/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m7/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m8/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m9/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m10/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m11/$t2),1,",",".")?>&nbsp;</div>
								<div>&nbsp;<?=number_format(($m12/$t2),1,",",".")?>&nbsp;</div>
								<div style="width:61px;">&nbsp;<?=number_format(($mt/$t2),1,",",".")?>&nbsp;</div>
							</div>
							<br><br>
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;" align="center"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
