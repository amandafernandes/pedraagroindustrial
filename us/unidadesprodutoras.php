<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;">
		<div style="width:970px; margin:0 auto;">
			<? require_once("topo.html"); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Producing Units    <br />
                          </div>                          
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="51%" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','285','src','swf/usinaPedra','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaPedra' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="285">
                                  <param name="movie" value="swf/usinaPedra.swf" />
                                  <param name="quality" value="high" />
                                  <param name="wmode" value="transparent" />
                                  <embed src="swf/usinaPedra.swf" width="340" height="285" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                </object>
</noscript></td>
                                <td width="49%" valign="top" style="padding-left:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;"> Usina da Pedra (Pedra Sugarmill) </div>
                                  <p style="margin-bottom: 6px"> The Group's first unit, it was the cradle of agroindustrial development in the heartland of the State of São Paulo. <br />
                                  	Located on Fazenda da Pedra, in Serrana, in the Ribeirão Preto region, it was purchased in 1931 by Pedro Biagi, an Italian immigrant, who came to Brazil with his family in the late 19th century. These immigrants and their descendants made history! The 1929 crash created a differentiated environment and caused sugarcane to soon stand out among the alternatives to coffee plantations. It was within this context that the Pedra Sugarmill began to produce. In its first harvest in 1931, it produced 3,600 bags of sugar. Along the time and with the work of their partners and employees, it grew. In the 1970's, with the Proálcool Program, it attained an important status in the Brazilian agroindustrial scenario. It was modernized and in the 80's it started its expansion process, becoming the Pedra Agroindustrial S/A Group in 2007. </p>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                    <tr>
                                      <td width="20%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;"> Address </td>
                                      <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="45" /></div></td>
                                      <td width="77%" style="line-height:13px; padding-left:5px;">Fazenda da Pedra<br />
                                        <strong> In town:</strong> Serrana - SP<br />
                                        <strong>CEP:</strong> 14.150.000 - <strong>Caixa Postal</strong> 2</td>
                                    </tr>
                                  </table></td>
                              </tr>
                            </table>                            
                            <br />
                            <br />
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top" style="padding-right:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;"> Buriti Sugarmill </div>
                                  <p style="margin-bottom: 6px"> The Buriti Sugarmill started its implementation in 1993, when Pedra Sugarmill acquired the Apediá Distillery, located in Vilhena, in the State of Rondônia. The distillery was disassembled and its equipment transferred to São Luiz da Esplanada farm, in Buritizal, State of São Paulo.<br />
                                  	The equipment began to me assembled in 1994 and the unit started its operations in 1995, crushing approximately 140,000 tons of cane and producing 12.2 million liters of hydrated alcohol. </p>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="21%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;"> Address </td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="45" /></div></td>
                                        <td width="76%" style="line-height:13px; padding-left:5px;">Fazenda São Luiz da Esplanada <br />
                                          <strong> In town:</strong> Buritizal – SP<br />
                                          <strong>CEP:</strong> 14.570.000 - <strong>Caixa Postal </strong>2</td>
                                      </tr>
                                  </table></td>
                                <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','285','src','swf/usinaBuriti','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaBuriti' ); //end AC code
                                </script>
                                    <noscript>
                                      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="285">
                                      <param name="movie" value="swf/usinaBuriti.swf" />
                                      <param name="quality" value="high" />
                                      <param name="wmode" value="transparent" />
                                      <embed src="swf/usinaBuriti.swf" width="340" height="285" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                    </object>
                                  </noscript></td>
                              </tr>
                            </table>
                            <br />
                            <br />
                            <br />
							<!--
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','285','src','swf/usinaIbira','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaIbira' ); //end AC code
                            </script>
                                    <noscript>
                                      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="285">
                                      <param name="movie" value="swf/usinaIbira.swf" />
                                      <param name="quality" value="high" />
                                      <param name="wmode" value="transparent" />
                                      <embed src="swf/usinaIbira.swf" width="340" height="285" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                    </object>
                                  </noscript></td>
                                <td valign="top" style="padding-left:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;"> Ibirá Sugarmill </div>
                                  <p> Located in the municipality of Santa Rosa do Viterbo, in the State of São Paulo, the company was leased by the Pedra Group in 1998 for a 17-year period. In that same year, it carried out its first harvest as Ibirá Sugarmill by crushing approximately 735,000 tons of cane and producing 1.06 million bags of sugar and 30.6 million liters of alcohol. Today, having been integrated into the group for over a decade, it presents a new profile, the result of investments which increase its production capacity year after year. </p>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="22%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;"> Address </td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="45" /></div></td>
                                        <td width="75%" style="line-height:13px; padding-left:5px;">Pavimentação Asfáltica SRV 322 , KM 03<br />
                                          <strong> In town:</strong> Santa Rosa de Viterbo - SP<br />
                                          <strong>CEP:</strong> 14.270.000 - <strong>Caixa Postal </strong>11</td>
                                      </tr>
                                  </table></td>
                              </tr>
                            </table>
                            <br />
                            <br />
                            <br />
							-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','285','src','swf/usinaIpe','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaIpe' ); //end AC code
                                </script>
                                    <noscript>
                                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="285">
                                      <param name="movie" value="swf/usinaIpe.swf" />
                                      <param name="quality" value="high" />
                                      <param name="wmode" value="transparent" />
                                      <embed src="swf/usinaIpe.swf" width="340" height="285" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                    </object>
                                  </noscript></td>
                                <td valign="top" style="padding-left:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;"> Ipê Sugarmill </div>
                                   	<p>  Located in Nova Independência, in the northeastern region of the State of São Paulo, it is the Group's newest unit. Its implementation began in 2004 with the first sugarcane plantations and its industrial complex was started up in April 2008. Fitted with a modern agroindustrial plant, all the cane processed by the unit is mechanically harvested.  </p>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="21%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;"> Address </td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="60" /></div></td>
                                        <td width="76%" valign="middle" style="line-height:13px; padding-left:5px;">Rod. Gal. Euclides de Oliveira<br />
                                          Figueiredo. 
                                          Km. 167 <br />
                                          <strong>In town: </strong>Nova Independência - SP<br />
                                          <strong>CEP:</strong> 16.940.000 - <strong>Caixa Postal</strong> 2</td>
                                      </tr>
                                  </table></td>
                              </tr>
                            </table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?PHP 
				  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
