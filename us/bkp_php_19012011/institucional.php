<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<style type="text/css">
<!--
.style1 {
	font-size: 13px;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td valign="top"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Institutional</div>
                                    <p><span lang="EN-US" xml:lang="EN-US">Pedra Agroindustrial is an agribusiness company operating since 1931 to build a scenario of economic, social, and environmental development to improve people's lives.</span></p>
                                    <p><span lang="EN-US" xml:lang="EN-US"><br />
                                    	Its commitment is for the continuous development of quality standards in all production processes so that the result of the work performed, in addition to technical and productivity gains, minimizes environmental impacts and adds value to the communities where it operates.</span></p></td>
                                <td valign="top"><div align="right"><img src="img/melhorandoaVIda.gif" alt="" width="222" height="130" style="margin-top:12px; margin-right:0px; margin-left:25px;" /></div></td>
                              </tr>
                              <tr>
                                <td colspan="2" valign="top"><p><br />
                                	<span lang="EN-US" xml:lang="EN-US">Its main activity is the production of alcohol, sugar, and electric energy from sugarcane. It has four producing units, all located in the State of São Paulo: Usina da Pedra, in Serrana; Usina Buriti, in Buritizal; Usina Ibirá, in Santa Rosa de Viterbo and Usina Ipê, in Nova Independência.</span></p>
                                    <p><span lang="EN-US" xml:lang="EN-US"><br />
                                    	A member of Copersucar, an organization which gathers a group of sugar and alcohol companies, the Pedra Agroindustrial Group preserves its production autonomy while participating in an organization which operates throughout the sugar and bionergy business chain.</span></p>
                                    <p><br />
                                    		<br />
                                    	</p></td>
                              </tr>
                          </table>
                            <table width="655" border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td height="330" style="background-color:#d5d2b1;"><div align="center"><img src="img/FotoInstitucional.jpg" alt="" width="645" height="320" /></div></td>
                              </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><table width="90%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td valign="top"><div style="font-size:18px; font-weight:normal; color:#766b2d; margin-bottom:8px;">Business  </div>
                                          <span style="font-size:14px; font-weight:bold;">Renewable Energy  </span><br />
                                        The production of energy from sugarcane is an alternative for the sustainable development of the planet and contributes to build the future we desire. This is our job.  <br />
                                                <br /></td>
                                    </tr>
                                    <tr>
                                      <td valign="top" style="padding-right:15px;"><div style="font-size:18px; font-weight:normal; color:#aa9a40; margin-bottom:8px;">  Mission  </div>
                                          To operate in the area of renewable energy with competitiveness, respecting the environment and contributing to social development.  <br />
                                        <br /></td>
                                    </tr>
                                    <tr>
                                      <td valign="top"><div style="font-size:18px; font-weight:normal; color:#cda71e; margin-bottom:8px;">  Vision  </div>
                                          Participating in the forefront<br />
                                        of the industry's development. <br />
                                        <br /></td>
                                    </tr>
                                    <tr>
                                      <td valign="top" style="padding-right:15px;"><div style="font-size:18px; font-weight:normal; color:#8E9A3A; margin-bottom:8px;">Code of Ethics</div>
                                        <a href="pdf/codigo.pdf" target="_blank"><img src="img/codigoEtica.jpg" alt="" width="92" height="136" border="0" style="float:left; margin-right:10px;" /></a><br />
                                        Download our Code of Ethics<br />
                                        <br />
                                        <span class="style1"><a href="pdf/codigo.pdf" target="_blank">Download (PDF)</a></span><br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br /></td>
                                    </tr>
                                </table></td>
                                <td valign="top"><div style="padding-right:28px;">
                                    <div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">  Principles  </div>
                                  <p><span style="font-size:14px; font-weight:bold;">  Integrity - present in our actions  </span><br />
                                    Our integrity permeates our relationship with all the stakeholders involved in our activities. The stable, enduring relationships we cultivate are the result from this principle that strengthens the company and values its achievements.  <br />
                                    <br />
                                    <span style="font-size:14px; font-weight:bold;">  Productivity - a permanent challenge  </span><br />
                                      Promoting productivity gains, aiming at growth and competitiveness, is the challenge that makes us stronger.   <br />
                                    <br />
                                    <span style="font-size:14px; font-weight:bold;">  Soundness - a commitment with the future  </span><br />
                                    Pedra Agroindustrial is solidified by its eyes on the future. It is more than seven decades of dedication to thousands of people, from various generations, who believe in the creative force of work.  <br />
                                    <br />
                                    <span style="font-size:14px; font-weight:bold;">  Unity - a team spirit  </span><br />
                                    Collaboration and team work are of fundamental importance for corporate development. Bringing together people of various ages, with different experiences, and different life stories in a productive manner is a driver of the Group's actions.  <br />
                                    <br />
                                    </p>
                                </div></td>
                              </tr>
                            </table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
