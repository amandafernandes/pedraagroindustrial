<?
session_start();

if ($_SESSION['idioma']=='') {
	$idioma = 'US';
} else {
	$idioma = $_SESSION['idioma'];
}

require_once("inc/conn.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td width="63%" valign="top" style="padding-right:12px;">

<?
if ($_GET['tipo']==1){
	?>
	<div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Environment  </div>
	<p><span lang="EN-US" xml:lang="EN-US">Programs which prioritize product quality and the reduction of environmental impacts are constantly devised and developed by the company.</span></p>
	<p><span lang="EN-US" xml:lang="EN-US">Meeting the environmental legislation and, whenever possible, exceeding it is a premise that yields innovations and ensures sustainability of the business.</span></p>
	<p><span lang="EN-US" xml:lang="EN-US">All the Group's activities are monitored and controlled in compliance with the legislation in force and specific programs for sustainable development.</span></p>
	<p><span lang="EN-US" xml:lang="EN-US"> <br />
	</span><span lang="EN-US" xml:lang="EN-US">The proper disposal of industrial waste, monitoring of water quality, reutilization of industrial waters, reduction and control of gas emissions, molecular sieve, a technology used to produce anhydrous alcohol to eliminate polluting chemicals, as well as conservation of the soil and water resources, reduction of atmospheric emissions, rational use of agrochemicals, and biological control, all these actions are a priority in the company's agricultural activities. A partnership between the Pedra and Balbo groups develops PHB, the biodegradable plastic extracted from sugar which decomposes within 180 days.</span></p>
	<?
} else {
	?>
	<div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Social Responsibility  </div>
	<p><span lang="EN-US" xml:lang="EN-US">Fostering the development of the communities where it operates is Pedra Agroindustrial Group's commitment. Its ethical conduct, the valuation and strengthening of the relationship with its varied public are the mainstay of its operation.</span></p>
	<p><span lang="EN-US" xml:lang="EN-US">In order to foster people's well-being and growth, the company provides a wide range of programs in the areas of health, education, culture, sports and leisure, which benefit the employees, their families, and their communities.</span></p>
	<p><span lang="EN-US" xml:lang="EN-US">All the group's employees and their families have access to medical and hospital care (insurance plan), dental services and medication.</span></p>
	<p><span lang="EN-US" xml:lang="EN-US">By investing in social projects, the company stands out in terms of child and adolescent care. This conduct, together with the ban on child labor, has made the Pedra Agroindustrial Group a Children-Friendly Company since 1999.</span></p>
	<?
}
?>
					</td>
                  </tr>

	<tr>
		<td valign="top" style="padding-right:12px;">
			<br />
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">

			<tr>

			<?
			$_SQL = "
				SELECT * FROM temas WHERE idioma = '".$idioma."' AND tipo = ".$_GET['tipo']." ORDER BY id
			";
			$rs = pg_query($_SQL);
			$i=0;
			while($rr=pg_fetch_assoc($rs)){
				if (($i%3)==0) {
					?><tr><?
					if ($i<pg_num_rows($rs)) {
						?></tr><?
					}
				}

				$_SQL = "
					SELECT imagem FROM fotos_temas WHERE idtema = ".$rr['id']." ORDER BY id LIMIT 1 OFFSET 0
				";

				$rsi = pg_query($_SQL);
				$rri = pg_fetch_assoc($rsi);
				?>
				<td width="33%" valign="top">
					<table width="190" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="212" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos.gif">
							<tr>
								<td height="137" valign="top"><a href="temas_interna.php?id=<?=$rr['id']?>&tipo=<?=$_GET['tipo']?>"><img src="../adminsite/uploads/temas/<?=$rri['imagem']?>" alt="" width="206" height="130" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="temas_interna.php?id=<?=$rr['id']?>&tipo=<?=$_GET['tipo']?>"><?=$rr['titulo']?></a></span></div></td>
					</tr>
					</table>
					<br>
				</td>
				<?
				$i++;
			}
			?>

			</tr>

			</table>
		
		</td>
	</tr>
	</table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
