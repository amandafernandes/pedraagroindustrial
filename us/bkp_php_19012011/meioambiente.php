<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td width="63%" valign="top" style="padding-right:12px;"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Meio Ambiente</div>
                                    <p>Programas  que priorizam a qualidade dos produtos e a redução dos impactos ao Meio  Ambiente são constantemente pensados e desenvolvidos na empresa. <br />
Atender a  legislação ambiental e, quando possível, se antecipar a ela, é uma premissa que  gera inovações e garante a sustentabilidade do negócio. <br />
Todas as  atividades do grupo são monitoradas e controladas segundo a legislação vigente  e Programas específicos para o desenvolvimento sustentável. <br />
<br />
A destinação  adequada dos resíduos industriais, o monitoramento da qualidade de água, a  reutilização das águas industriais, a redução e controle de emissão de gases, a  Peneira Molecular, uma tecnologia utilizada na produção do Álcool Anidro que  elimina o uso de produtos químicos poluentes, assim como a conservação do solo  e dos recursos hídricos, a redução de emissões atmosféricas, o uso racional de  defensivos agrícolas, e o controle biológico, estão entre as ações priorizadas  nas atividades agrícolas da empresa. Uma parceria entre os grupos Pedra e Balbo, desenvolve o PHB, o plástico  biodegradável extraído a partir do açúcar que se decompõe em até 180 dias.</p>                                  
                                    </td>
                                </tr>
                              <tr>
                                <td valign="top" style="padding-right:12px;"><br />
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td width="33%" valign="top"><table width="190" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                <tr>
                                                  <td height="92" valign="top"><a href="meioambienteFlorestal.php"><img src="img/MeioAbmienteHome1.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="meioambienteFlorestal.php">Programa de Recomposição Florestal das Áreas de Preservação Permanente</a></span></div></td>
                                          </tr>
                                      </table></td>
                                      <td width="33%" valign="top"><table width="190" border="0" align="center" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                <tr>
                                                  <td height="92" valign="top"><a href="meioambienteviveiroMudas.php"><img src="img/MeioAbmienteHome2.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="meioambienteviveiroMudas.php">Viviero de <br />
                                              Mudas Nativas</a></span></div></td>
                                          </tr>
                                      </table></td>
                                      <td width="33%" valign="top"><table width="190" border="0" align="right" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td><table width="191" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos2.gif">
                                                <tr>
                                                  <td height="92" valign="top"><a href="meioambienteCertificado.php"><img src="img/MeioAbmienteHome3.jpg" alt="" width="185" height="85" border="0" style="margin-left:2px; margin-top:2px;" /></a></td>
                                                </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td valign="top" style="padding-top:4px;"><div align="center"><span style="font-size:13px; margin-bottom:4px;"><a href="meioambienteCertificado.php">Protocolo Agroambiental – Etanol Verde</a></span></div></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                  </table></td>
                              </tr>
                              
                          </table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
