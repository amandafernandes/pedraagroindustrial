<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Unidades Produtoras  <br />
                          </div>                          
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="51%" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','340','src','swf/usinaPedra','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaPedra' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="340">
                                  <param name="movie" value="swf/usinaPedra.swf" />
                                  <param name="quality" value="high" />
                                  <param name="wmode" value="transparent" />
                                  <embed src="swf/usinaPedra.swf" width="340" height="340" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                </object>
</noscript></td>
                                <td width="49%" valign="top" style="padding-left:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;">Usina da Pedra</div>
                                  <p>Primeira  unidade do grupo, organizada nas primeiras décadas do século XX, foi berço do  desenvolvimento agroindustrial do interior do estado de São Paulo. <br />
                                    <br />
                                    Localizada  na Fazenda da Pedra em Serrana, na região de Ribeirão Preto, foi adquirida em  1931, por Pedro Biagi, imigrante italiano, que veio para o Brasil ainda criança  com a família, no final do século XIX, em busca de uma vida melhor.<br />
Esses imigrantes e seus descendentes fizeram história!<br />
Logo  depois da crise de 1929 e da bancarrota dos produtores de café, fortaleceram  seus investimentos no que seria a alternativa econômica para a região; lançaram-se  na produção de cana-de-açúcar e na reorganização dos Engenhos Centrais que,  mesmo no contexto da economia cafeeira, existiam.<br />
<br />
Foi  nesse contexto que a Usina da Pedra começou a produzir. Na primeira safra foram  4 mil sacos de açúcar, com o tempo e o trabalho  dedicado de vários parceiros e funcionários, cresceu. Depois, na década de 70, com  o Proálcool, alcançou um patamar importante no cenário agroindustrial  brasileiro. Modernizou-se e na década de 90 iniciou seu processo de expansão,  transformando-se em 2007, no grupo Pedra Agroindustrial S/A.</p>
                                  <div style="font-size:15px; color:#566336; margin-top:6px; margin-bottom:6px;">Espectativa de produção - Safra 2008/2009</div>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                    <tr>
                                      <td width="20%" style="font-size:13px; color:#8d620c; text-align:right; padding-right:4px;"><strong>Álcool</strong></td>
                                      <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                      <td width="20%" style="font-size:21px; color:#595731;"><div align="center">211,8</div></td>
                                      <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                      <td width="54%" style="font-size:13px; color:#8d620c; line-height:15px; padding-left:3px;">milhões de litros</td>
                                    </tr>
                                  </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                    <tr>
                                      <td width="20%" style="font-size:13px; color:#922c07; text-align:right; padding-right:4px;"><strong>Açúcar</strong></td>
                                      <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                      <td width="14%" style="font-size:21px; color:#595731;"><div align="center">3,9</div></td>
                                      <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                      <td width="60%" style="font-size:13px; color:#922c07; line-height:15px; padding-left:3px;">milhões de sacas</td>
                                    </tr>
                                  </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                    <tr>
                                      <td width="20%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;">Endereço</td>
                                      <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="45" /></div></td>
                                      <td width="77%" style="line-height:13px; padding-left:5px;">Fazenda da Pedra<br />
                                        <strong>Cidade:</strong> Serrana - SP<br />
                                        <strong>CEP:</strong> 14.150.000 - <strong>Caixa Postal</strong> 2</td>
                                    </tr>
                                  </table></td>
                              </tr>
                            </table>                            
                            <br />
                            <br />
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top" style="padding-right:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;">Usina Buriti</div>
                                  <p>A  implantação da Usina Buriti começou em 1993, quando a Usina da Pedra adquiriu a  Destilaria Apediá, instalada em Vilhena, no estado de Rondônia. A destilaria  foi desmontada e os equipamentos transferidos para a Fazenda São Luiz da  Esplanada, em Buritizal, no estado de São Paulo. <br />
                                    A  montagem dos equipamentos da unidade teve início em 1994 e sua operação começou  em 1995 com a moagem de aproximadamente 50 mil toneladas de cana-de-açúcar e  produção de 12,2 milhões de litros de álcool hidratado.</p>
                                  <div style="font-size:15px; color:#566336; margin-top:6px; margin-bottom:6px;">Espectativa de produção - Safra 2008/2009</div>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="21%" style="font-size:13px; color:#8d620c; text-align:right; padding-right:4px;"><strong>Álcool</strong></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="20%" style="font-size:21px; color:#595731;"><div align="center">191,3</div></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="53%" style="font-size:13px; color:#8d620c; line-height:15px; padding-left:3px;">milhões de litros</td>
                                      </tr>
                                    </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="21%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;">Endereço</td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="45" /></div></td>
                                        <td width="76%" style="line-height:13px; padding-left:5px;">Fazenda São Luiz da Esplanada <br />
                                          <strong>Cidade:</strong> Buritizal – SP<br />
                                          <strong>CEP:</strong> 14.570.000 - <strong>Caixa Postal </strong>2</td>
                                      </tr>
                                  </table></td>
                                <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','340','src','swf/usinaBuriti','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaBuriti' ); //end AC code
                                </script>
                                    <noscript>
                                      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="340">
                                      <param name="movie" value="swf/usinaBuriti.swf" />
                                      <param name="quality" value="high" />
                                      <param name="wmode" value="transparent" />
                                      <embed src="swf/usinaBuriti.swf" width="340" height="340" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                    </object>
                                  </noscript></td>
                              </tr>
                            </table>
                            <br />
                            <br />
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','340','src','swf/usinaIbira','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaIbira' ); //end AC code
                            </script>
                                    <noscript>
                                      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="340">
                                      <param name="movie" value="swf/usinaIbira.swf" />
                                      <param name="quality" value="high" />
                                      <param name="wmode" value="transparent" />
                                      <embed src="swf/usinaIbira.swf" width="340" height="340" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                    </object>
                                  </noscript></td>
                                <td valign="top" style="padding-left:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;">Usina Ibirá</div>
                                  <p>Localizada  no município de Santa Rosa de Viterbo, São Paulo, no antigo parque industrial  da família Matarazzo e conhecida como usina Amália, a empresa foi arrendada pelo  grupo Pedra em 1998, por um período de 17 anos. No mesmo ano, realizou a  primeira safra como Usina Ibirá moendo, aproximadamente, 55 mil toneladas de  cana e produzindo 51,6 mil sacas de açúcar e 1,7 milhões de litros de álcool.<br />
                                    Hoje,  há mais de uma década integrada ao grupo, apresenta um novo perfil resultado de  investimentos que ano após ano melhoram sua capacidade produtiva. </p>
                                  <div style="font-size:15px; color:#566336; margin-top:6px; margin-bottom:6px;">Espectativa de produção - Safra 2008/2009</div>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="22%" style="font-size:13px; color:#8d620c; text-align:right; padding-right:4px;"><strong>Álcool</strong></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="20%" style="font-size:21px; color:#595731;"><div align="center">54,8</div></td>
                                        <td width="1%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="54%" style="font-size:13px; color:#8d620c; line-height:15px; padding-left:3px;">milhões de litros</td>
                                      </tr>
                                    </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="22%" style="font-size:13px; color:#922c07; text-align:right; padding-right:4px;"><strong>Açúcar</strong></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="14%" style="font-size:21px; color:#595731;"><div align="center">1,7</div></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="58%" style="font-size:13px; color:#922c07; line-height:15px; padding-left:3px;">milhões de sacas</td>
                                      </tr>
                                    </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="22%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;">Endereço</td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="45" /></div></td>
                                        <td width="75%" style="line-height:13px; padding-left:5px;">Pavimentação Asfáltica SRV 322 , KM 03<br />
                                          <strong>Cidade:</strong> Santa Rosa de Viterbo - SP<br />
                                          <strong>CEP:</strong> 14.270.000 - <strong>Caixa Postal </strong>11</td>
                                      </tr>
                                  </table></td>
                              </tr>
                            </table>
                            <br />
                            <br />
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top" style="padding-right:15px;"><div style="font-size:20px; color:#566336; margin-bottom:4px;">Usina Ipê</div>
                                  Localizada em Nova Independência, sua operação teve início em Abril de 2008.
                                  <div style="font-size:15px; color:#566336; margin-top:6px; margin-bottom:6px;">Espectativa de produção - Safra 2008/2009</div>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="21%" style="font-size:13px; color:#8d620c; text-align:right; padding-right:4px;"><strong>Álcool</strong></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="16%" style="font-size:21px; color:#595731;"><div align="center">54,8</div></td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="38" /></div></td>
                                        <td width="57%" style="font-size:13px; color:#8d620c; line-height:15px; padding-left:3px;">milhões de litros</td>
                                      </tr>
                                    </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:9px;">
                                      <tr>
                                        <td width="21%" style="font-size:13px; color:#91962b; text-align:right; padding-right:4px;">Endereço</td>
                                        <td width="3%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="60" /></div></td>
                                        <td width="76%" style="line-height:13px; padding-left:5px;">Rod. Gal. Euclides de Oliveira<br />
                                          Figueiredo. 
                                          Km. 167 <br />
                                          <strong>Cidade: </strong>Nova Independência - SP<br />
                                          <strong>CEP:</strong> 16.940.000 - <strong>Caixa Postal</strong> 2</td>
                                      </tr>
                                  </table></td>
                                <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','340','height','340','src','swf/usinaIpe','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/usinaIpe' ); //end AC code
                                </script>
                                    <noscript>
                                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="340" height="340">
                                      <param name="movie" value="swf/usinaIpe.swf" />
                                      <param name="quality" value="high" />
                                      <param name="wmode" value="transparent" />
                                      <embed src="swf/usinaIpe.swf" width="340" height="340" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
                                    </object>
                                  </noscript></td>
                              </tr>
                            </table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?PHP 
				  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
