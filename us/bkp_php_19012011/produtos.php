<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td width="21%" valign="top" style="padding-right:12px;"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Products</div>
                                    <p><br />
                                      <img src="img/produtoAlcool.jpg" alt="" width="115" height="111" /><br />
                                      <br />
                                      <img src="img/produtoAcucar.jpg" alt="" width="115" height="111" style="margin-top:8px;" /><br />
                                      <br />
                                    <img src="img/produtoEnergiaEletrica.jpg" alt="" width="115" height="111" style="margin-top:3px;" /></p>                                  </td>
                                <td width="79%" valign="top"><div align="right">
                                  <p align="left"><strong><br />
                                    <br />
                                    <br />
                                    <span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;"><img src="img/produtosAlcool.gif" alt="" width="78" height="25" style="margin:0px 0px 4p 0px;" /></span></strong><br />
                                      it is a product obtained from the fermentation of sugarcane juice and intermediate products from the sugar manufacturing process.<br />
                                    The Pedra Agroindustrial Group produces anhydrous ethanol, which is used as a gasoline admixture, and hydrated ethanol, which drives alcohol-fueled cars, in addition to other industrial alcohols which serve as raw material for other products.<br />
									<a href="http://www.cepea.esalq.usp.br/alcool/" target="_blank"><img src="img/indicadoresdePreco.gif" width="111" height="19" border=0  style="margin-top:5px;"/></a>
                                    <br />
                                    <br />
                                    <img src="img/produtosAcucar.gif" alt="" width="59" height="29" /><br />
The company produces several types of sugars, among which VVHP and VVHPC, which are totally exported, and white crystal sugar, whose production is earmarked for the domestic market. <br />
									<a href="http://www.cepea.esalq.usp.br/acucar/" target="_blank"><img src="img/indicadoresdePreco.gif" border=0 style="margin-top:5px;"/></a>
                                    <br />
                                    <br />
                                    <br />
                                    <strong><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;"><img src="img/produtosEnergia.gif" alt="" width="150" height="33" /></span></strong><br />
                                      The Group produces electric energy from the bagasse resulting from sugarcane crushing. The energy thus produced can meet the internal consumption of all the company's units and still provides a surplus for the regional consumer market through the local utilities company.<br />
                                    The total electricity generated by the group is 250,000 MWh a year, enough to supply electricity to a population of 500,000 residential dwellers a year.</p>
                                  </div></td>
                              </tr>
                              
                          </table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
