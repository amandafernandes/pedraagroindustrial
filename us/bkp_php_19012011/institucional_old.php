<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td valign="top"><div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Institucional - Perfil</div>
                                    <p>A Pedra Agroindustrial é uma empresa de agronegócio que trabalha desde  1931 para construir um cenário de desenvolvimento econômico, social e ambiental  que melhore a vida das pessoas. <br />
                                      <br />
                                    </p>
                                    <p>Seu compromisso é o desenvolvimento contínuo dos padrões de qualidade em  todos os processos produtivos de forma que o resultado do trabalho realizado,  além dos ganhos técnicos e de produtividade, minimize os impactos ambientais e valorize  as comunidades onde atua.</p>
                                    </td>
                                <td valign="top"><div align="right"><img src="img/melhorandoaVIda.gif" alt="" width="222" height="130" style="margin-top:12px; margin-right:0px; margin-left:25px;" /></div></td>
                              </tr>
                              <tr>
                                <td colspan="2" valign="top"><p><br />
                                  Sua principal atividade é a produção de álcool, açúcar e energia  elétrica a partir da cana-de-açúcar. Possui quatro unidades produtoras, todas  no estado de São Paulo: Usina da Pedra, em Serrana; Usina Buriti, em Buritizal;  Usina Ibirá, em Santa Rosa  de Viterbo e Usina Ipê, em Nova Independência.<br />
                                  <br />
                                </p>
                                    <p>Associada à Copersucar, organização  que reúne um conjunto de empresas sucroalcooleiras, o grupo Pedra  Agroindustrial preserva sua autonomia produtiva ao mesmo tempo em que está  presente em uma empresa que atua em toda a cadeia de negócio de açúcar e  bioenergia.<br />
                                      <br />
                                    </p>
                                    </td>
                              </tr>
                          </table>
                            <table width="655" border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td height="330" style="background-color:#d5d2b1;"><div align="center"><img src="img/FotoInstitucional.jpg" alt="" width="645" height="320" /></div></td>
                              </tr>
                            </table>
                            <br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="58%" valign="top"><div style="padding-right:28px;">
                                  <div style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Príncipios</div>
                                  <p><span style="font-size:14px; font-weight:bold;">Integridade – presente em nossas ações</span><br />
                                    A conduta íntegra pauta a  convivência com todos os públicos com os quais nos relacionamos. As relações  duradouras e estáveis que mantemos são resultado desse princípio que fortalece  a empresa e valoriza suas conquistas.<br />
                                    <br />
                                    <span style="font-size:14px; font-weight:bold;">Produtividade – desafio permanente</span><br />
                                    Superar a cada safra os limites estabelecidos na anterior nos faz capaz de enfrentar o desafio do crescimento mantendo a estabilidade e a competitividade.<br />
                                    <br />
                                    <span style="font-size:14px; font-weight:bold;">Solidez – compromisso com o futuro</span><br />
                                    É com os olhos voltados para o futuro que a Pedra Agroindustrial se solidifica. São mais de sete décadas de dedicação de milhares de pessoas, de várias gerações que acreditam na força criadora do trabalho.<br />
                                    <br />
                                    <span style="font-size:14px; font-weight:bold;">Unidade – espírito de equipe</span><br />
                                    A colaboração e o trabalho em equipe são fundamentais para o desenvolvimento das empresas. Reunir pessoas de várias idades, com experiências distintas, com histórias de vida diferentes de forma produtiva é uma condição que orienta as ações do grupo.<br />
                                    <br />
                                  </p>
                                </div></td>
                                <td width="42%" valign="top"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-right:30px;">
                                  <tr>
                                    <td valign="top"><div style="font-size:18px; font-weight:normal; color:#766b2d; margin-bottom:8px;">Negócio</div>                                         <span style="font-size:14px; font-weight:bold;">Energia Renovável</span><br />
                                          Produzir  energia a partir da cana-de-açúcar é uma alternativa para o desenvolvimento sustentável  do planeta e contribui para construir o futuro que queremos. 
                                          Esse  é o nosso trabalho. <br />
                                          <br /></td>
                                    </tr>
                                  <tr>
                                    <td valign="top" style="padding-right:15px;"><div style="font-size:18px; font-weight:normal; color:#aa9a40; margin-bottom:8px;">Missão</div>
                                      Atuar na área de  energia renovável com competitividade, respeitando o meio ambiente e  contribuindo para o desenvolvimento social.<br />
                                      <br /></td>
                                    </tr>
                                  <tr>
                                    <td valign="top"><div style="font-size:18px; font-weight:normal; color:#cda71e; margin-bottom:8px;">Visão</div>
                                      Participar da Liderança<br />
                                      no Desenvolvimento do Setor</td>
                                    </tr>
                                  <tr>
                                    <td valign="top"><br />
                                      <div style="font-size:18px; font-weight:normal; color:#1B7027; margin-bottom:8px;">Balanço Patrimonial 2008</div>
                                      Em cumprimento às disposições legais e estatuárias, segue o nosso balanço patrimonial e as demais demonstrações financeiras.
                                      <span style="font-size:14px; font-weight:bold;"><br />
                                      <a href="pdf/balanco_ibsa_2008_doe_1.pdf" target="_blank">Download - Parte 1</a><br />
                                       <a href="pdf/balanco_ibsa_2008_doe_2-1.pdf" target="_blank">Download - Parte 2</a></span></td>
                                  </tr>
                                </table></td>
                              </tr>
                            </table>
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
