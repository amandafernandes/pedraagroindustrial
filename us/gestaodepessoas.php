﻿<?require_once("inc/conn.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;">
		<div style="width:970px; margin:0 auto;">
			<? require_once("topo.html"); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align=left><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td valign="top"><span style="font-size:18px; font-weight:normal; color:#566336;">People Management  </span>
<?
if ($_SESSION['estaLogado']==true){
	include("menuRH.php");
}
?>

<Br>
<br>
                                
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="65%" valign="top"><p><span lang="EN-US" xml:lang="EN-US">Pedra Agroindustrial's commitment to value and strengthen the relationship with its collaborators is a premise for sound entrepreneurial performance.<br />
                                        	<br />
                                        </span><span lang="EN-US" xml:lang="EN-US">The Group believes in the strength of a qualified, committed team, therefore it can provide opportunities for professional growth through career progression and educational benefits.<br />
                                        	<br />
                                        </span><span lang="EN-US" xml:lang="EN-US">Training programs are carried out aiming at professional enhancement and development with emphasis in the quality, safety, and well-being of its collaborators. Every year they participate in training activities such as courses, lectures, seminars, and technical visits.<br />
                                        	<br />
                                        </span><span lang="EN-US" xml:lang="EN-US">The scholarship program stimulates technical and college training and the collaborators' participation in specialization courses. Professional initiation programs such as young apprenticeship and internship and trainee programs offer opportunities for young people to enter the job market.<br />
                                        	<br />
                                        </span><span lang="EN-US" xml:lang="EN-US">All these actions contribute to a quality-based, committed professional profile that is the hallmark of the Pedra Agroindustrial Group's collaborators.</span><br />
                                        		<br />
                                        			</p>
                                          	<table width="95%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td width="37%"><div align="center"><a href="trabalheConosco.php"><img src="img/trabalheConoscoTitulo.gif" alt="" width="127" height="75" border="0" /></a></div></td>
                                              <td width="13%"><div align="center"><img src="img/dividorTitulo.gif" alt="" width="46" height="95" /></div></td>
                                              <td width="50%"><div align="center"><a href="oportunidades.php"><img src="img/VagasOportunidadesTitulo.gif" alt="" width="183" height="75" border="0" /></a></div></td>
                                            </tr>
                                          </table>
                                          <p><br />  
                                              <br />
                                          </p></td>
                                        <td width="35%" style="padding-left:18px;">


										<?
										$sql = "SELECT fotos_gestao.* FROM fotos_gestao order by id";

										$rs = pg_query($sql);
										
										while($rr = pg_fetch_assoc($rs)){
											?>
											<table width="217" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos.gif">
											<tr>
												<td height="135" valign="top" style="padding-left:2px; padding-top:2px;"><img src="adminsite/uploads/gestao/<?=$rr['imagem']?>" alt="" width="211" height="130" border="0" /></td>
											</tr>
											</table>
											<br />
											<?
										}
										?>


										  </td>
                                      </tr>
                                    </table>
                                    <p>&nbsp;</p>
                                    </td>
                                </tr>
                              
                          </table>
                            </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
