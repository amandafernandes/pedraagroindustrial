<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;"><div style="width:970px; margin:0 auto;">
    <div style="position:absolute; z-index:999; width:126px; height:126px; padding:15px 0 0 750px; margin:0 auto;"><img class="pngFix" src="img/selo.png" width="126" height="126" alt="80 anos" /></div> <div align="center">
      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','960','height','121','src','swf/topo','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/topo' ); //end AC code
    </script>
      <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="960" height="121">
          <param name="movie" value="swf/topo.swf" />
          <param name="quality" value="high" />
          <param name="wmode" value="transparent" />
          <embed src="swf/topo.swf" width="960" height="121" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
        </object>
        </noscript>
    </div></td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="left"><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-right:30px;">
                              <tr>
                                <td valign="top"><div style="font-size:18px; font-weight:normal; color:#566336;">Responsabilidade Social - Programa de Viagens &quot;De Malas Prontas&quot;  <br />
                                </div>
                                    <br />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td colspan="3" valign="top"><p>Criado há mais de trinta anos, com o objetivo de  ampliar o repertório cultural e a visão de mundo dos funcionários da empresa e  de seus familiares, o programa vai além. Hoje, quem participa consegue  enriquecer sua cultura e realizar um sonho. <br />
                                            <br />
                                            De Malas Prontas proporciona anualmente a várias  pessoas, conhecer a praia, viajar pela primeira vez de avião, ter contato com diferentes  lugares e culturas no Brasil e fora dele. <br />
<br />
Com o subsídio de 30% oferecido pela empresa para  qualquer dos roteiros organizados, o programa  busca proporcionar uma oportunidade diferenciada para a vida pessoal,  profissional e social de seus funcionários.<br />
                                        <br />
                                        </p>                                            
                                          </td>
                                        </tr>
                                      <tr>
                                        <td width="33%" valign="top"><table width="211" border="0" cellpadding="0" cellspacing="0" background="img/fundoFotos.gif">
                                          <tr>
                                            <td height="135" valign="top" style="padding-left:2px; padding-top:2px;"><img src="img/responsabilidadeViagem2.jpg" alt="" width="206" height="130" border="0" /></td>
                                          </tr>
                                        </table></td>
                                        <td width="33%" ><table width="211" border="0" align="center" cellpadding="0" cellspacing="0" background="img/fundoFotos.gif">
                                          <tr>
                                            <td height="135" valign="top" style="padding-left:2px; padding-top:2px;"><img src="img/responsabilidadeViagem1.jpg" alt="" width="206" height="130" border="0" /></td>
                                          </tr>
                                        </table></td>
                                        <td width="33%" ><table width="211" border="0" align="right" cellpadding="0" cellspacing="0" background="img/fundoFotos.gif">
                                          <tr>
                                            <td height="135" valign="top" style="padding-left:2px; padding-top:2px;"><img src="img/responsabilidadeViagem3.jpg" alt="" width="206" height="130" border="0" /></td>
                                          </tr>
                                        </table></td>
                                      </tr>
                                    </table>
                                    <br />
                                    <a href="responsabilidadeSocial.php">Voltar</a></td>
                                </tr>
                              
                          </table>
                            <br />
                            <br /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
