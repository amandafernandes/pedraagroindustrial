﻿<?
if ($_SESSION['estaLogado']==true){
	die("<script>location='oportunidades.php';</script>");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<style>
	#aguarde {position:absolute;display:none;width:90px;z-index:998;border:1px solid #CCCCCC;background:white;margin-left:35px;margin-top:20px;}
	#aguarde DIV{position:relative;float:left;line-height:30px;margin-left:5px;height:30px;}
	#aguarde DIV IMG{margin-left:7px;margin-top:7px;}
</style>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script type="text/javascript" src="files/png_fix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.pngFix');
	</script>
    <![endif]-->
<script src="files/connecting.js" type="text/javascript"></script>
<script src="files/commom.js" type="text/javascript"></script>
<script>
function verifica_email(str_email){
if (str_email.search(/^[a-zA-Z0-9]+[_a-zA-Z0-9-]*(\.[_a-z0-9-]+)*@[a-z?G0-9]+(-[a-z?G0-9]+)*(\.[a-z?G0-9-]+)*(\.[a-z]{2,4})$/) != -1) return true;
else return false;
}
function verifica1(){
	var f = document.f_com;
	var email = f.email;
	if(email.value.replace(/\ /g, "") == "" || !verifica_email(email.value)){
		alert("Digite o e-mail");
		email.focus();
		return true;
	}
	var senha = f.senha;
	if(senha.value.replace(/\ /g, "") == ""){
		alert("Digite a senha");
		senha.focus();
		return true;
	}
	f.submit();
}
function verifica2(){
	var f = document.f_sem;
	var email = f.email;
	if(email.value.replace(/\ /g, "") == "" || !verifica_email(email.value)){
		alert("Digite o e-mail");
		email.focus();
		return true;
	}
	f.submit();
}

var ESQUECI = {
	'show':function(){
		$('tab_a').style.display = 'none';
		$('tab_b').style.display = 'block';
		$('email').value = '';
		$('senha').value = '';
	},
	'hide':function(){
		$('tab_a').style.display = 'block';
		$('tab_b').style.display = 'none';
		$('id_username').value = '';
		$('aguarde').style.display = 'none';
	},
	'send':function(){
		var erro = '';
		var myPost = '';
		
		myPost += 'acao=1&';

		var f = $('id_username');
		var v = f.value;
		if(v.isWhite()){
			alert("Digite o CPF.");
			f.focus();
			return false;
		}else{
			myPost += 'cpf='+v+'&';
		}

		if (myPost.length>1) { myPost = myPost.substring(0, (myPost.length-1)) }
		
		$('aguarde').style.display = 'block';
		
		var myCallback={
			success:function(o){
				var xml = new xmlParse();
				xml.setObjAjax(o);
				xml.setChildName('contato');
				xml.parse();

				$('aguarde').style.display = 'none';
				
				if (xml.getAttByName('acaoret',0) == 1) {
					ESQUECI.hide();
					alert("Cadastro ainda não está liberado para acessar o sistema.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 2){
					ESQUECI.hide();
					$('erro_email').style.display = "block";
					return false;
				}					
				if (xml.getAttByName('acaoret',0) == 3) {
					ESQUECI.hide();
					alert("A senha foi enviada para o e-mail cadastrado.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 4) {
					ESQUECI.hide();
					alert("Senha não enviada.\nTente novamente mais tarde, por favor.");
					return false;
				}
				if (xml.getAttByName('acaoret',0) == 5) {
					alert("CNPJ / CPF não encontrado.");
					$('id_username').focus();
					return false;
				}
			},
			failure:function(){
				ESQUECI.hide();
				alert("Erro ao tentar enviar senha.\nTente novamente mais tarde, por favor.");
				return false;
			}
		};
		Connect.asyncRequest('POST', "envia_senha.php", myCallback, myPost);
	}
}
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background:url(img/fundoTopo.jpg); background-position:top center; background-repeat:repeat-x; height:120px;">
		<div style="width:970px; margin:0 auto;">
			<? require_once("topo.html"); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td valign="top" style="background:url(img/fundoCorpo.jpg); background-position:top center; background-repeat:repeat-x; height:900px;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align=center><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:950px; max-width:950px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background-color:#e8e7d5; margin-bottom:4px;">
                <tr>
                  <td valign="top" style="height:400px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align=left><? require_once("menu.php");?></td>
                      <td width="76%" valign="top" align=left><table width="97%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoBordaInterna.gif); background-repeat:repeat-x; background-position:top center; height:400px; margin-top:7px; margin-left:12px; margin-right:10px;">
                        <tr>
                          <td valign="top" style="padding-top:12px; padding-left:17px; padding-right:15px;"><span style="font-size:18px; font-weight:normal; color:#566336; margin-bottom:8px;">Trabalhe Conosco</span>
                          



<?
if ($_GET['erro']==1){
	?>
	<script>alert("E-mail ou senha inválidos!\nTente novamente, por favor.")</script>
	<?
}
if ($_GET['erro']==2){
	?>
	<script>
		alert("Você já está inscrito neste processo seletivo.");
		location="oportunidades.php";
	</script>
	<?
}
?>



<br /><br /><br /><br /><br /><br />


<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="34%"><table border="0" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoAcessoRestrito.jpg); background-repeat:no-repeat; background-position:bottom; width:228px; height:183px;">
	  <tr>
		<td valign="top" style="padding-left:25px;">
		<form action="verLogin.php?acao=login&irpara=<?=$_GET['irpara']?>&idp=<?=$_GET['idp']?>" name="f_com" style="margin:0px;" method="post"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td height="80" valign="top"><img src="img/naocadastrado.gif" alt="" width="167" height="48" /></td>
			</tr>
			<tr>
			  <td>
			  
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;" id="tab_a">
				  <tr>
					<td colspan="2" align="left">E-mail</td>
				  </tr>
				  <tr>
					<td colspan="2" align="left"><input name="email" type="text" class="formularioPadrao" id="email" size="25" maxlength="100" /></td>
				  </tr>
				  <tr>
					<td colspan="2" align="left">Senha:</td>
				  </tr>
				  <tr>
					<td width="45%" align="left" valign="middle"><input name="senha" type="password" class="formularioPadrao" id="senha" size="14" /></td>
					<td width="55%" align="left" valign="top"><span style="padding-top:4px; ">
						<input type="button" value="Continuar" class="botaoPadrao" style="width:60px; margin-top:2px; cursor:pointer;" onclick="verifica1();" />
					</span></td>
				  </tr>
				  <tr>
					<td colspan="2" align="left" height="25"><a href="javascript:ESQUECI.show();">Esqueci minha senha</a></td>
				  </tr>				  
			  </table>
			  
				<div id="aguarde">
					<div><img src="img/loading.gif" border=0></div>
					<div class="textoCinza">Aguarde</div>
				</div>

			  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;display:none;" id="tab_b">
				  <tr>
					<td colspan="2" align="left">Digite seu CPF (sem pontuação)<br>e clique em continuar.</td>
				  </tr>			  
				  <tr>
					<td colspan="2" align="left"><input name="id_username" type="text" class="formularioPadrao" id="id_username" size="25" maxlength="20" /></td>
				  </tr>
				  <tr>
					<td height="30" align="left" valign="bottom"><span style="padding-top:4px; ">
						<input type="button" value="Continuar" class="botaoPadrao" style="width:60px; margin-top:2px; cursor:pointer;" onclick="ESQUECI.send();" />
						<input type="button" value="Cancelar" class="botaoPadrao" style="width:60px; margin-top:2px; cursor:pointer;" onclick="ESQUECI.hide();" />
					</span></td>
					</tr>				  
  			  </table>			  
			  </form></td>
			</tr>
		</table></td>
	  </tr>
	</table></td>
	<td width="30%"><div align="center"><img src="img/linhaVerticalBranco.gif" alt="" width="2" height="200" /></div></td>
	<td width="36%"><table border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(img/fundoAcessoRestrito.jpg); background-repeat:no-repeat; background-position:bottom; width:228px; height:198px;">
	  <tr>
		<td valign="top" style="padding-left:8px;"><form action="enviarCurriculo.php" name="f_sem" style="margin:0px;" method="post"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td height="95" valign="top"><img src="img/naosoucadastrado.gif" alt="" width="210" height="63" /></td>
			</tr>
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:25px;">
				  <tr>
					<td align="left">E-mail</td>
				  </tr>
				  <tr>
					<td align="left"><input name="email" type="text" class="formularioPadrao" id="email" size="25" /></td>
				  </tr>

				  <tr>
					<td height="30" align="left" valign="bottom"><span style="padding-top:4px; ">
					  <input type="button" value="Continuar" class="botaoPadrao" style="width:60px; margin-top:2px; cursor:pointer;" onclick="verifica2();" />
					</span></td>
					</tr>
			  </table></form></td>
			</tr>
		</table></td>
	  </tr>
	</table></td>
  </tr>
</table>








                          
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0"  style="width:950px; margin-top:4px;">
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" style="width:942px; background:url(img/fundoRodape.jpg); margin-bottom:1px;">
                <tr>
                  <td style="height:47px;"><?php
                  require_once("rodape.php");
				  ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
          <div align="center"><img src="img/barraRodape.jpg" alt="" width="944" height="16" /></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
