/* configurando swf upload */
function uploadCompleteToHidden(file) {
	if (this.getStats().files_queued === 0) {
		document.getElementById(this.customSettings.cancelButtonId).disabled = true;
	}
	var status = document.getElementById("divStatus");
	status.innerHTML = "Arquivo: <i>" + file.name + "</i> enviado.";
	$('curriculo').value = file.name;
}
var swfu;
var settings = {
	flash_url : "swfupload.swf",
	upload_url: "swfupload.php",	// Relative to the SWF file
	post_params: {"PHPSESSID" : "<?php echo session_id(); ?>"},
	file_size_limit : "100 MB",

	file_types : "*.*",
	file_types_description : "All Files",

	file_upload_limit : 100,
	file_queue_limit : 0,
	custom_settings : {
		progressTarget : "fsUploadProgress",
		cancelButtonId : "btnCancel"
	},
	debug: false,

	// Button settings
	button_image_url: "img/bt_arquivo.gif",	// Relative to the Flash file
	button_width: "64",
	button_height: "18",
	button_placeholder_id: "spanButtonPlaceHolder",
	button_text: '',
	button_text_style: '',
	button_text_left_padding: 0,
	button_text_top_padding: 0,
	
	// The event handler functions are defined in handlers.js
	file_queued_handler : fileQueued,
	file_queue_error_handler : fileQueueError,
	file_dialog_complete_handler : fileDialogComplete,
	upload_start_handler : uploadStart,
	upload_progress_handler : uploadProgress,
	upload_error_handler : uploadError,
	upload_success_handler : uploadSuccess,
	upload_complete_handler : uploadCompleteToHidden,
	queue_complete_handler : null	// Queue plugin event
};

window.onload = function() {
	swfu = new SWFUpload(settings);
};

function mudarFotos(mudarpara){
	document.getElementById('fotogrande').src = 'img/gestaoAmbFauna'+mudarpara+'Grande.jpg';
}

function verificaInclusao(){
	var erro = '';
	var myPost = '';
/*
.para
.nome
.de
ddd fone
.cidade
.uf
.assunto
.msg
*/

	myPost += 'cargo='+$('cargo').value+'&';
	myPost += 'area='+$('area').value+'&';
	myPost += 'obs='+$('obs').value+'&';

	var f = $('nome');
	var v = f.value;
	if (v.isWhite()){
		erro += '- Digite o Nome\n';
	}else{
		myPost += 'nome='+v+'&';
	}

	var f = $('de');
	var v = f.value;
	if (v.isWhite() || !verifica_email(v)){
		erro += '- Digite o E-mail\n';
	}else{
		myPost += 'de='+v+'&';
	}

	var f = $('curriculo');
	var v = f.value;
	if (v.isWhite()){
		erro += '- Envie o arquivo do Curr�culo\n';
	}else{
		myPost += 'curriculo='+v+'&';
	}

	if (myPost.length>1) { myPost = myPost.substring(0, (myPost.length-1)) }
	alert(myPost)
	if (!erro.isWhite()){
		alert("Aten��o para os erros abaixo:\n"+erro);
	} else {
		$('aguarde').style.display = 'block';
		var myCallback={
			success:function(o){
				var xml = new xmlParse();
				xml.setObjAjax(o);
				xml.setChildName('contato');
				xml.parse();

				$('aguarde').style.display = 'none';

				if (xml.getAttByName('acaoret',0) == 1) {
					limpaForm();
					alert("Curr�culo enviado com sucesso.");
				}
				if (xml.getAttByName('acaoret',0) == 2) {
					alert("Curr�culo n�o enviado.\nTente novamente mais tarde, por favor.");
				}
			},
			failure:function(){
				alert("Erro ao tentar enviar o curr�culo.\nTente novamente mais tarde, por favor.");
			}
		};
		Connect.asyncRequest('POST', "envia_trabalhe.php", myCallback, myPost);
	}
}

function limpaForm(){
	$('nome').value = '';
	$('de').value = '';
	$('cargo').selectedIndex = 0;
	$('area').selectedIndex = 0;
	$('curriculo').value = '';
	$('obs').value = '';
	$('divStatus').innerHTML = '';
}