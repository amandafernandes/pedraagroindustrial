var isIE =(/\bmsie\b/i.test(navigator.userAgent)&& document.all&&!(/\bopera\b/i.test(navigator.userAgent)));
var isIE6 =(/\bmsie 6.0\b/i.test(navigator.userAgent));

var $ = function (a) { return document.getElementById(a); };
var $S = function (a) { return document.getElementById(a).style; };

function mostraFlash(){
	var id = arguments[0];
	var swf = arguments[1];
	var width = arguments[2];
	var height = arguments[3];
	var parans = arguments[4] || {'scale':'noscale', 'wmode':'transparent'};
	var expressInstall = arguments[5] || '/media/swf/expressinstall.swf';
	
	var so = new SWFObject(swf, id+"movie", width, height, "8", "transparent", true);
	
	for (k in parans) so.addParam(k, parans[k]);
	
	so.useExpressInstall(expressInstall);
	
	so.write(id);
}

// Retorna o tamanho de um objeto
getSize = function (e) {
	if (typeof e == 'string') e = $(e);
	return {x:e.offsetWidth, y:e.offsetHeight};
};

// Retorna o scroll da página
getScroll = function () {
	if (self.pageXOffset) {
		sX = self.pageXOffset;
	} else if (document.documentElement && document.documentElement.scrollLeft) {
		sX = document.documentElement.scrollLeft;
	} else if (document.body) {
		sX = document.body.scrollLeft;
	}
	if (self.pageYOffset) {
		sY = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop) {
		sY = document.documentElement.scrollTop;
	} else if (document.body) {
		sY = document.body.scrollTop;
	}
	return {x:sX, y:sY};
};

// Retorna o tamanho da área visivel
getDocVisibleSize = function () {
	var _x, _y;
	if (window.innerWidth) {
		_x = window.innerWidth;
	} else if (document.documentElement && document.documentElement.clientWidth) {
		_x = document.documentElement.clientWidth;
	} else if (document.body) {
		_x = document.body.clientWidth;
	}
	if (window.innerHeight) {
		_y = window.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) {
		_y = document.documentElement.clientHeight;
	} else if (document.body) {
		_y = document.body.clientHeight;
	}
	return {x:_x, y:_y};	
};

// Retorna o tamanho total do documento
getDocSize = function () {
	return {x:document.body.offsetWidth, y:document.body.offsetHeight};
};

// Aplica um valor alpha de 0 a 100
setAlpha = function (e, a) {
	a = Math.round(a);
	if (typeof e == 'string') e = $(e);
	with (e.style) {
		if (isIE) {
			filter = 'alpha(opacity='+a+')';
		} else {
			opacity = a/100;
		}
	}
};

filter2 = function(txtEntrada, strValidos) {
	var txtSaida = '';
	for (var a = 0; a<txtEntrada.length; a++) {
		if (strValidos.indexOf(txtEntrada.substr(a, 1)) != -1) {
			txtSaida = txtSaida+txtEntrada.substr(a, 1);
		}
	}
	return txtSaida;
};
mask = function(strMask, ev, objData) {
	ev = ev || event;
	var key = getKey(ev);
	if(key == 9) return;
	if (isNum(key, '%\'#$.') == true) {
		return true;
	}
	if (!(key>=37 && key<=40)) {
		var valor = filter2(objData.value, '0123456789');
		var tam = valor.length;
		var tamMask = strMask.length;
		var strOut = '';
		var intCont = 0;
		for (var a = 0; a<tamMask && intCont<=tam; a++) {
			if (strMask.substr(a, 1) == '#') {
				strOut += valor.substr(intCont++, 1);
			} else {
				strOut += strMask.substr(a, 1);
			}
		}
		objData.value = strOut;
		return true;
	}
};
isNum = function(code, strValidos) {
	var strCode = String.fromCharCode(code);
	strValidos = strValidos || '%\'#$.0123456789`abcdefghi	';
	if (strValidos.indexOf(strCode)>=0) {
		return true;
	} else {
		return false;
	}
};
function isVoid(obj) {
	if (obj != null && typeof obj != 'undefined' && obj != "") return false;
	return true;
}
String.prototype.isDate = function() {
	try {
		var arrData = new Array();
		arrData = this.split('/');
		var dia = arrData[0];
		var mes = arrData[1]-1;
		var ano = arrData[2];
		var dataEntrada = (arrData[0]*1)+'/'+(arrData[1]*1)+'/'+arrData[2];
		if (parseInt(ano) < 1920) {
			return null;
		}
		var minhaData = new Date(ano,mes,dia,12,0,0);
		var dataRetorno = minhaData.getDate()+'/'+(minhaData.getMonth()+1)+'/'+minhaData.getFullYear();
		if (dataEntrada == dataRetorno) {
			return true;
		} else {
			return null;
		}
	} catch(e) {
		return null;
	}
}
String.prototype.isWhite = function() {
  if (!isVoid(this)) {
	var value = this.replace(/^\s+/m,'').replace(/\s+$/m,'');
    return (value == '');
  }
  return true;
}
function setSomenteNum(obj,_event){
	eval('$(\''+obj+'\').'+_event+' = function(e) { return somenteNum(e); }');
}
function somenteNumVirg(e) {
	var whichCode = getKey(e);
	if(/_13_|_8_|_9_|_36_|_35_|_46_|_110_|_188_/.test('_'+whichCode+'_')) return true;
	if((whichCode >= 48 && whichCode <= 57) || (whichCode >= 96 && whichCode <= 105) || whichCode == 110 || whichCode == 188){
		return true;
	}else{
		return false;	
	}
}
function somenteNum(e) {
	var whichCode = getKey(e);
	if(/_13_|_8_|_9_|_36_|_35_|_46_/.test('_'+whichCode+'_')) return true;
	if((whichCode >= 48 && whichCode <= 57) || (whichCode >= 96 && whichCode <= 105)){
		return true;
	}else{
		return false;	
	}
}
function getKey(e) {
	try{
		return event?(event.keyCode?event.keyCode:(event.which?event.which:event.charCode)):null;
	}catch(f){
		return e.keyCode;	
	}
}
function validateEmail(email) {
  return (/^[A-Za-z0-9]+(([\.\_\-]{1}[A-Za-z0-9]+)+)?@[A-Za-z0-9]+(([\.\_\-]{1}[A-Za-z0-9]+)+)?\.[A-Za-z]{2,4}$/.test(email));
}
validaCpf = function(cpf) {
	var rrValida = new Array(00000000000,11111111111,22222222222,33333333333,44444444444,55555555555,66666666666,77777777777,88888888888,99999999999);
	for(i=0;i<11;i++){
		if(cpf == rrValida[i]){
			return false;	
		}
	}
    var i;
    var c = cpf.substr(0,9);
    var dv = cpf.substr(9,2);
    var d1 = 0;   
    for (i = 0; i < 9; i++) d1 += c.charAt(i)*(10-i);
    if (d1 == 0) return false;
    d1 = 11 - (d1 % 11);
    if (d1 > 9) d1 = 0;
    if (dv.charAt(0) != d1) return false;
    d1 *= 2;
    for (i = 0; i < 9; i++) d1 += c.charAt(i)*(11-i);
    d1 = 11 - (d1 % 11);
    if (d1 > 9) d1 = 0;
    if (dv.charAt(1) != d1)  return false;
    return true;
}
validaCnpj = function(cnpj) {
    var i;
    var c = cnpj.substr(0,12);
    var dv = cnpj.substr(12,2);
    var d1 = 0;
    
    for (i = 0; i < 12; i++) d1 += c.charAt(11-i)*(2+(i % 8));
    if (d1 == 0) return false;
    d1 = 11 - (d1 % 11);
    if (d1 > 9) d1 = 0;
    if (dv.charAt(0) != d1)  return false;
    d1 *= 2;
    for (i = 0; i < 12; i++) d1 += c.charAt(11-i)*(2+((i+1) % 8));
    d1 = 11 - (d1 % 11);
    if (d1 > 9) d1 = 0;
    if (dv.charAt(1) != d1)  return false;
    return true;
}

radio = function(object) {
	if(typeof object == 'object'){
		strChek = object.name;
	}else{
		strChek = object;
	}
	obj = document.getElementsByTagName('input');
	for (var i = 0; i< obj.length; i++){
	    if (obj[i].type == 'radio' && obj[i].name == strChek  && obj[i].checked){
			return obj[i].value;
		}
	}
	return null
}


function focusTo(objStr,maxlength,to,e) {
	if (objStr.length == maxlength) document.getElementById(to).focus();
}

function setFocusTo(obj,_event,maxlength,to){
	eval('$(\''+obj+'\').'+_event+' = function(e) { focusTo(this.value,'+maxlength+',\''+to+'\',e); }');
}

function errField(c,a){
	var q = (a)?'inline':'none';
	$('erro_'+c).style.display = q;
}

function selectOption(campo,valor) {
	for (var i = 0; i < campo.length; i++) {
	    if (campo.options[i].value == valor) campo.options[i].selected = true;
	}
}

String.prototype.toCurrency = function() {
	var ret = new String('');
	var fim = new String(',00');
	var exe = new String(this);
	var isNotFirst = null;
	if (this.indexOf('.')>0) {
		var arr = exe.split('.');
		exe = arr[0];
		while (arr[1].length<2) {
			arr[1] += '0';
		}
		if (arr[1].length>2) {
			arr[1] = arr[1].substr(0, 2);
		}
		fim = ','+arr[1];
	}
	while (exe.length>3) {
		if (isNotFirst) {
			ret = '.'+ret;
		}
		isNotFirst = true;
		ret = exe.substr(exe.length-3, 3)+ret;
		exe = exe.substr(0, exe.length-3);
	}
	if (exe.length>0) {
		if (ret.length>0) {
			ret = exe+'.'+ret;
		} else {
			ret = exe;
		}
	}
	return ret+fim;
};

