function mudarFotos(mudarpara){
	document.getElementById('fotogrande').src = 'img/gestaoAmbFauna'+mudarpara+'Grande.jpg';
}

function verificaInclusao(){
	var erro = '';
	var myPost = '';
/*
.para
.nome
.de
ddd fone
.cidade
.uf
.assunto
.msg
*/

	myPost += 'para='+$('para').value+'&';
	myPost += 'uf='+$('uf').value+'&';

	var f = $('nome');
	var v = f.value;
	if (v.isWhite()){
		erro += '- Digite o Nome\n';
	}else{
		myPost += 'nome='+v+'&';
	}

	var f = $('de');
	var v = f.value;
	if (v.isWhite() || !verifica_email(v)){
		erro += '- Digite o E-mail\n';
	}else{
		myPost += 'de='+v+'&';
	}
	
	var f = $('cidade');
	var v = f.value;
	if (v.isWhite()){
		erro += '- Digite a Cidade\n';
	}else{
		myPost += 'cidade='+v+'&';
	}

	var f = $('assunto');
	var v = f.value;
	if (v.isWhite()){
		erro += '- Digite o assunto\n';
	}else{
		myPost += 'assunto='+v+'&';
	}

	var f = $('msg');
	var v = f.value;
	if (v.isWhite()){
		erro += '- Digite o texto para contato\n';
	}else{
		myPost += 'msg='+v+'&';
	}

	var f1 = $('ddd');
	var f2 = $('fone');
	var v1 = f1.value;
	var v2 = f2.value;
	if (v1.isWhite() || v2.isWhite()){
		erro += '- Informe corretamente o telefone\n';
	}else{
		myPost += 'fone='+v1+'-'+v2+'&';
	}

	if (myPost.length>1) { myPost = myPost.substring(0, (myPost.length-1)) }

	if (!erro.isWhite()){
		alert("Aten��o para os erros abaixo:\r"+erro);
	} else {
		$('aguarde').style.display = 'block';
		var myCallback={
			success:function(o){
				var xml = new xmlParse();
				xml.setObjAjax(o);
				xml.setChildName('contato');
				xml.parse();

				$('aguarde').style.display = 'none';

				if (xml.getAttByName('acaoret',0) == 1) {
					limpaForm();
					alert("Contato enviado com sucesso.");
				}
				if (xml.getAttByName('acaoret',0) == 2) {
					alert("Contato n�o enviado.\nTente novamente mais tarde, por favor.");
				}
			},
			failure:function(){
				alert("Erro ao tentar enviar o contato.\nTente novamente mais tarde, por favor.");
			}
		};
		Connect.asyncRequest('POST', "envia_email.php", myCallback, myPost);
	}
}

function limpaForm(){
	$('nome').value = '';
	$('de').value = '';
	$('cidade').value = '';
	$('assunto').value = '';
	$('msg').value = '';
	$('ddd').value = '';
	$('fone').value = '';
	$('para').selectedIndex = 0;
	$('uf').selectedIndex = 0;
}