<?
	require_once("inc/conn.php");
	$rsb = pg_query("select * from banners where idioma='US' order by posicao");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
	<head>
		<meta name="verify-v1" content="EYkmJ03H+SEj4z93Gsb9mrpnBcfa4hrErdnwBrL7T14=" >
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="estilos.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery.js"></script> 
		<script type="text/javascript" src="jquery.cycle.all.min.2.81.js"></script>
		<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
		<!--[if IE 6]>
			<script type="text/javascript" src="files/png_fix.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.pngFix');
			</script>
			<![endif]-->
		<script>
		var pausa = 0;
		$(document).ready(function() {
			$('.slideshow').cycle({ fx:'fade', next:'#prox', prev:'#ant' });
			$('#pausa').click(function(){
				$('.slideshow').cycle('pause');
				$('#gira').css({ 'background-position':'-114px -28px' });
				$('#gira').hover(
					function(){ $(this).css({ 'background-position':'-114px 0px' }); },
					function(){ $(this).css({ 'background-position':'-114px -28px' }); }
				);
				$(this).hover(
					function(){ $(this).css({ 'background-position':'-31px 0px' }) },
					function(){ $(this).css({ 'background-position':'-31px 0px' }) }
				);
			});
			$('#gira').click(function(){
				$('.slideshow').cycle('resume', true);
				$('#pausa').css({ 'background-position':'-31px -28px' });
				$('#pausa').hover(
					function(){ $(this).css({ 'background-position':'-31px 0px' }); },
					function(){ $(this).css({ 'background-position':'-31px -28px' }); }
				);
				$(this).hover(
					function(){ $(this).css({ 'background-position':'-114px 0px' }) },
					function(){ $(this).css({ 'background-position':'-114px 0px' }) }
				);
			});
		});
		</script>
		<style>
		/*nao funciona com apenas 1 imagem*/
		.slideshow img{
			<?if(pg_num_rows($rsb)>1){?>
			display:none;
			<?}?>
		}
		body {background-color:#e8e7d5;}
		.video {
			width: 195px;
			height: 46px;
			background-image:url('swf/videoInstitucional.png');}
		
		.video_ {
			width: 195px;
			height: 46px;
			background-image:url('swf/videoInstitucional_.png');}
		#chamadas {
			background-image:url('img/fundoChamadas.gif');
			background-position:-8px top;
			background-repeat:no-repeat;
			height: 280px;
			width: 710px;
			margin-top:6px;}
		.chamadas {float: left; padding: 5px; }
		#primeira {width: 32%}
		#segunda {width: 30%}
		#terceira {width: 33%}
		#bannerFlash {
			margin-top:6px; 
			margin-right:7px;
			border: 5px solid #d5d2b1;
			width: 687px;
			height: 200px;}
		.imgSlideShow {
			width: 226px;
			height: 230px;
			border: 0px;
			cursor:pointer;
		}
		</style>
	</head>
	<body>
		<div id="bannerFlash">
			<script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','687','height','200','src','swf/banner','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/banner' ); //end AC code
			</script>
			<noscript>
			<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="687" height="200">
				<param name="movie" value="swf/banner.swf" />
				<param name="quality" value="high" />
				<param name="wmode" value="transparent" />
				<embed src="swf/banner.swf" width="687" height="200" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
			</object>
			</noscript>
		</div>
		<div id="chamadas">
			<div id="primeira" class="chamadas">
				<div class="slideshow" class="chamadas">
				<?
				if(pg_num_rows($rsb)){
					while($rrb=pg_fetch_assoc($rsb)) {
						if ($rrb['target'] == '_blank') {?>
							<img src="../adminsite/uploads/bannerhome/<?=$rrb['arquivo'];?>" title="<?=$rrb['titulo'];?>"onclick="window.open('<?=$rrb['link'];?>')" class="imgSlide" />
						<?
						}else{
						?>
							<!--<img src="adminsite/uploads/bannerhome/<?=$rrb['arquivo'];?>" border="0" title="<?=$rrb['titulo'];?>"width="226" height="230" onclick="location='<?=$rrb['link'];?>'" style="cursor:pointer;" />-->
							<img src="../adminsite/uploads/bannerhome/<?=$rrb['arquivo'];?>" title="<?=$rrb['titulo'];?>"onclick="window.open('<?=$rrb['link'];?>')" class="imgSlide" />
						<?}
					}
				}
				?>
				</div>
				<?if(pg_num_rows($rsb)>1){?>
				<div id="bt_galeria">
					<a id="ant" href="#">&nbsp;</a>
					<a id="pausa" href="#" class="pausaOn">&nbsp;</a>
					<a id="gira" href="#" style="background-position:-114px 0px;">&nbsp;</a>
					<a id="prox" href="#">&nbsp;</a>
				</div>
				<?}?>
			</div>
			<div id="segunda" class="chamadas">
				<div style="font:normal 18px 'Trebuchet MS'; color:#936a1f; margin-bottom:4px;">News</div>
				<?
				$rs = pg_query("select * from releases Where idioma = 'US' order by data desc, id desc limit 3 offset 0") ;
				if(pg_num_rows($rs)){
					while($rr=pg_fetch_assoc($rs)){
						?>
						<a href="saladeimprensa2.php?id=<?=$rr['id'];?>" target="_top"><?=dataeuabr($rr['data']);?></a><br />
						<a href="saladeimprensa2.php?id=<?=$rr['id'];?>" target="_top" style="font-size:13px; font-weight:bold;"><?=$rr['titulo'];?></a><br />
						<div style="margin-top:10px;"></div>
						<?
					}
				}
				?>
				<div style="margin-top:10px; margin-bottom:8px; text-align:right;">
					<a href="saladeimprensa.php" target="_top"><img src="img/maisVerde.gif" alt="" width="15" height="15" border="0" /></a>
					<a href="saladeimprensa.php" target="_top">More News</a>
				</div>
				<a href="http://www.pedraagroindustrial.com.br/us/video.php" target="_top">
					<div class="video" onmouseover="this.className='video_'" onmouseout="this.className='video'"></div>
				</a>
			</div>
			<div id="terceira" class="chamadas">
				<div align="center"><a href="http://www.etanolverde.com.br/" target="_blank">
					<img src="img/etanol.jpg" alt="" width="108" height="144" border="0" style="margin-top:12px;" /></a><br />
					<div style="font:normal 14px 'Trebuchet MS'; color:#73703f;margin-top:8px;">Important Links<br />
					</div>
					<div align="left"><span style="padding-left:21px; padding-top:9px; padding-right:9px;">
						<script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','215','height','65','src','swf/links','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','swf/links' ); //end AC code
						</script>
						<noscript>
							<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="215" height="65">
								<param name="movie" value="swf/links.swf" />
								<param name="quality" value="high" />
								<param name="wmode" value="transparent" />
								<embed src="swf/links.swf" width="215" height="65" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
							</object>
						</noscript>
					</span></div>
				</div>
			</div>
		</div>
	</body>
</html>