<?
session_start();

if ($_SESSION['idioma']=='') {
	$idioma = 'BR';
} else {
	$idioma = $_SESSION['idioma'];
}

require_once("../inc/conn.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pedra Agroindustrial - Eventos</title>
<!-- CSS link -->
<link href="media/css/estilo_eventos.css" rel="stylesheet" type="text/css" />

<link href="nyroModal.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="val_cadastro.js"></script>
<script type="text/javascript" src="http_request.js"></script>
<!--
<script type="text/javascript" src="animatedcollapse.js"></script>
<script type="text/javascript" src="jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="jquery.nyroModal-1.4.2.pack.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('filtroAno', 'fade=0,speed=400,group=cardapio,hide=1');
animatedcollapse.addDiv('filtroMes', 'fade=0,speed=400,group=cardapio,hide=1');
animatedcollapse.init();
</script>
-->
</head>
<body>
	<div id="pai">
    	<div id="cabecalho">
       		<img src="media/img/topo_esq.jpg" width="255" height="116" /><img src="media/img/topo_cen.jpg" width="263" height="116" /><img src="media/img/topo_dir.jpg" width="423" height="116" />
        </div>
        <div id="conteudo">
        	<div id="esquerda">
            	<div class="eventos">
                    <?
					$idEvento = -1;
					$numEventos = 0;
					// Eventos Esquerda
                    $sql =  "SELECT t1.*, t2.titulo as categoria FROM eventos t1";
                    $sql .= " INNER JOIN categorias_eventos t2 ON t2.id=t1.idcategoria";
					$sql .= " WHERE (CURRENT_DATE>=t1.dataveicini AND CURRENT_DATE<=t1.dataveicfim)";
					$sql .= " ORDER BY t1.ordem, t1.titulo LIMIT 5";
                	//echo $sql;
                    $rsc = pg_query($sql);
					$numEventos = pg_num_rows($rsc);
					//echo $numEventos;
					if($numEventos<1)
						echo("<div class=\"evtRelacionadoData\">Aguarde, em breve teremos novidades.</div>");
					else
					{					
						while($rrc = pg_fetch_assoc($rsc))
						{
						?>  
						<!-- evento -->
						<div class="dataEvento" style="text-transform:uppercase"><?=$rrc['data'];?><br /><?//=$rrc['categoria'];?></div>
						<div class="tituloEvento" style="text-transform:uppercase"><?=$rrc['titulo'];?></div>
						<div class="resenhaEvento"><?=$rrc['resenha'];?></div>
						<div class="leiaMaisLink"><a href="?idItem=<?=$rrc['id'];?>&tp=ev">[+] leia mais sobre o evento</a></div>
						<div><img src="media/img/separaNoticias.jpg" width="211" height="1" /></div>
						<?
						} //fim while
					} //fim else
					?>
                </div>
            </div>
            <div id="centro">
            <? if($numEventos>0){ ?>
            	<?
				//if(!isset($_GET["filtro"])){
				
				if(isset($_GET["tp"]) and $_GET["tp"]!="")
					$tipo = $_GET["tp"];
				else
					$tipo = "ev";
				
				if($tipo=="ct") //conteudo
				{
					$idCont = $_GET['idItem'];
					$idEvento = $_GET['idEv'];
					
					$sql_meio =  "select t1.*, t3.id as idcat, t3.titulo as categoria from conteudos t1";
					$sql_meio .= " INNER JOIN subcategorias_eventos t3 on t3.id=t1.idcategoria";
					$sql_meio .= " where t1.id = ".$idCont;						
				}
				else //evento
				{					
					if(isset($_GET["idItem"]) and $_GET["idItem"]!="")
					{
						$idEvento = $_GET['idItem'];
						
						$sql_meio =  "select t1.*, t3.id as idcat, t3.titulo as categoria from eventos t1";
						$sql_meio .= " INNER JOIN categorias_eventos t3 on t3.id=t1.idcategoria";
						$sql_meio .= " where t1.id = ".$idEvento;
					}
					else
					{
						$sql_meio =  "select t1.*, t3.id as idcat, t3.titulo as categoria from eventos t1";
						$sql_meio .= " INNER JOIN categorias_eventos t3 on t3.id=t1.idcategoria";
						$sql_meio .= " WHERE (CURRENT_DATE>=t1.dataveicini AND CURRENT_DATE<=t1.dataveicfim)";
						$sql_meio .= " ORDER BY t1.ordem, t1.titulo LIMIT 1";							
					}
				}
				
				$rs = pg_query($sql_meio);
				if(pg_num_rows($rs))
				{
					$rr = pg_fetch_assoc($rs);	
					
					if(!isset($idEvento) or $idEvento==-1)
						$idEvento = $rr["id"];
				?>
					<div class="caminhoData">
						<div class="caminhoEventoAberto" style="text-transform:uppercase"><?=$rr['categoria'];?></div>
						<div class="dataEventoAberto"><?=$rr['data'];?></div>
					</div>
					<div class="tituloEventoAberto" style="text-transform:uppercase"><?=$rr['titulo'];?></div>
                    
					<?
					if ($rr['arquivo']!='')
					{
					?>                
					<div class="imagemEvento"><img src="../adminsite/uploads/eventos/<?=$rr['arquivo'];?>" alt="" style="max-width:405px;" /></div><br />
					<?
					} //fim arquivo
					?>                

                    <? if($tipo=="ct"){ ?>
                    <div class="arquivoDownload" style="text-transform:uppercase">
                        <?
                        $sql  = "SELECT t1.* FROM arquivos t1";
                        $sql .= " WHERE t1.iditem=".$idCont." AND tipo=1";
						$sql .= " ORDER BY t1.titulo";
                    	//echo $sql;
                        $rsc = pg_query($sql);
                        while($rrc = pg_fetch_assoc($rsc))
                        {
                        ?>             
                        <div class="imagemEvento"><img src="../adminsite/uploads/eventos/<?=$rrc['arquivo'];?>" alt="" style="max-width:405px;" /></div><br />
                        <?								
                        } //fim while               
                    ?>                        
                    </div>      
                    <? } //fim arquivo ?>    
                    
                    									
					<div class="textoEventoAberto"><?=$rr['texto'];?></div><br/>
					
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b86cdae355bb1b2"></script>
					<div class="rede">
						<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b86cdae355bb1b2" class="addthis_button_compact" style="z-index:1000">
						<img src="media/img/redes.gif" width="162" height="22" alt="COMPARTILHE" border="0" />
						</a>
					</div>            					
                    
                    <? if($tipo=="ct"){ ?>   
                    
					<?
                    if ($rr['video']!='') {
                    ?>
                    <div class="video" style="z-index:1">
                        <object width="405" height="328">
                        <param name="movie" value="<?=$rr['video'];?>" />
                        <param name="allowFullScreen" value="true" />
                        <param name="allowscriptaccess" value="always" />
                        <embed src="<?=$rr['video'];?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="405" height="328"></embed>
                        </object>
                    </div>
                    <br />
                    <?
                    }
                    ?> 
                                                     
						<?
                        $sql  = "SELECT t1.* FROM arquivos t1";
                        $sql .= " WHERE t1.iditem=".$idCont." AND tipo=0";
						$sql .= " ORDER BY t1.titulo";
                    	//echo $sql;
                        $rsc = pg_query($sql);
						$numDown = pg_num_rows($rsc);
						
						if($numDown){
						?>                
                        <div class="arquivoDownload" style="text-transform:uppercase">        
                        <img src="media/img/download.gif" alt="" width="152" height="16" style="padding-bottom:5px" /><br />                    
                        <?
                        while($rrc = pg_fetch_assoc($rsc))
                        {
                        ?>                  
                      <img src="media/img/iconeDownload.gif" width="25" height="12" alt="DOWNLOAD" /><a href="../adminsite/uploads/eventos/<?=$rrc['arquivo'];?>" target="_blank">                  <?=$rrc['titulo'];?></a><br />
                        <?								
                        } //fim while               
						?>
	                    </div>                        
                        <?
						}
                    ?>                              
                    <? } //fim arquivo ?>              
				<?
				} // fim existe item  
				//} //fim filtro
				?>                     
               
				<?
				if($tipo=="000")
				{
					$titulo = "EVENTOS";
					$tipo = "ev";
					
					$sql_rel  = "select t1.*, t2.titulo as subcategoria, t3.titulo as categoria from eventos t1";
					$sql_rel .= " INNER JOIN subcategorias_eventos t2 on t2.id=t1.idcategoria";
					$sql_rel .= " INNER JOIN categorias_eventos t3 on t3.id=t2.idcategoria";
					$sql_rel .= " WHERE (t1.dataveicini<=CURRENT_DATE AND t1.dataveicfim>=CURRENT_DATE)";
					
					if(isset($_GET["idcat"]))
						$sql_rel .= " AND t3.id = ".$_GET['idcat'];	
					/*						
					else if(isset($_GET["idsubcat"]))
						$sql_rel .= " AND t2.id = ".$_GET['idsubcat'];	
					else if(isset($_GET["ano"]))
						$sql_rel .= " where EXTRACT(YEAR FROM t1.data)=".$_GET['ano'];			
					else if(isset($_GET["mes"]))
						$sql_rel .= " AND EXTRACT(MONTH FROM t1.data)=".$_GET['mes'];						
					else if(isset($rr['idsubcat']) or isset($rr['idcat']))
					{
						$sql_rel .= " AND (t2.id = ".$rr['idsubcat']." OR t3.id=".$rr['idcat'].") and t1.id<>".$rr['id'];	
						$sql_rel .= " ORDER BY t1.data ASC";
						$sql_rel .= " LIMIT 10";
					}	
					*/
					$sql_rel .= " ORDER BY t1.ordem, t1.titulo";
				}
				else
				{
					if(!isset($idEvento))
						$idEvento = $_GET["idEv"];
						
					$titulo = "CONTEÚDO";					
					
					$sql_rel  = "select t1.*, t2.titulo as categoria from conteudos t1";
					$sql_rel .= " INNER JOIN subcategorias_eventos t2 on t2.id=t1.idcategoria";			
					$sql_rel .= " where t1.idevento=".$idEvento;	
					if($tipo=="ct")
						$sql_rel .= " AND t1.id<>".$idCont;
					if(isset($_GET["idsubcat"]))	
						$sql_rel .= " AND t2.id=".$_GET["idsubcat"];
					$sql_rel .= " ORDER BY categoria, t1.titulo ASC";
					//$sql_rel .= " LIMIT 10";

					$tipo = "ct";										
				}
				?>
                
                <div class="separaEvento"><img src="media/img/separaEventoAberto.jpg" width="405" height="1" /></div>
                
                <div class="eventosRelacionados"><img src="media/img/conteudo_relacionado.gif" alt="" width="152" height="16" /></div>
                <div id="relacionados">                 
				<?
				//echo $sql_rel;
				$rs = pg_query($sql_rel);
				if(!pg_num_rows($rs))
				{
					if(isset($_GET["filtro"]))
						echo("<div class=\"evtRelacionadoData\">Não existem itens cadastrados segundo os critérios solicitados.</div>");
					else
						echo("<div class=\"evtRelacionadoData\">Não existem itens relacionados.</div>");					
				}
				else
				{
                    while($rrc = pg_fetch_assoc($rs))
                    {
						
				?>                                       
                	<!-- RELACIONADOS 01 -->
              		<div class="evtRelacionadoTipo" style="text-transform:uppercase"><?=$rrc['categoria'];?></div>
                	<div class="evtRelacionadoTitulo" style="text-transform:uppercase"><?=$rrc['titulo'];?></div>
                	<div class="evtRelacionadoLeia"><a href="?idItem=<?=$rrc['id'];?>&idEv=<?=$idEvento;?>&tp=<?=$tipo;?>">[+]</a></div>
                    <div class="separaRelacionados"><img src="media/img/separaEventoAberto.jpg" width="405" height="1" /></div>                      
					<?								
                    } //fim while
                } // fim existe item               
				?>                         
                </div>
                <br/><br/>
			<? } //fim numEventos ?>
          	</div>
            <!-- fim meio -->
            
            <div id="direita">
            	 <? if($numEventos>0){ ?>            
                 <!-- COMBO TIPO -->
             	 <div class="tituloFiltroCategoria"><img src="media/img/filtroTipo.gif" width="251" height="30" alt="TIPO" /></div>                 
	           	 <div class="comboTipo">
                	<!--<div class="comboFechado">TODOS</div>   comboAbertoItemAberto -->
                    <?
					if(pg_num_rows($rs))
					{						
						$sql  = "select t2.id, t2.titulo from conteudos t1";
						$sql .= " INNER JOIN subcategorias_eventos t2 on t2.id=t1.idcategoria";			
						$sql .= " where t1.idevento=".$idEvento;	
						$sql .= " GROUP BY t2.id, t2.titulo";
					
						$rsc = pg_query($sql);
						while($rrc = pg_fetch_assoc($rsc))
						{
					?>                                  						
                        <div class="categoria" style="text-transform:uppercase"><a href="?filtro=1&idsubcat=<?=$rrc['id'];?>&idEv=<?=$idEvento;?>&tp=ev&idItem=<?=$idEvento;?>"><?=$rrc['titulo'];?></a></div>
                       
					<?
						} //fim while
					?>
                    	<div class="categoria" style="text-transform:uppercase"><a href="?idItem=<?=$idEvento;?>&tp=ev">TODOS</a></div>
                    <?
					} //fim if
					else
					{
						echo("<div class=\"evtRelacionadoData\" style=\"padding-left:34px;\">Este evento não possui conteúdo a ser filtrado.</div>");
					}
					?>                    
             	 </div>
            	 <? } //fim numEventos ?>
            
                  <!-- CADASTRE-SE -->
                 <div class="tituloCadastre"><img src="media/img/cadastrese.gif" width="251" height="30" alt="Cadastre-se" /></div>
                 <div class="form">
                 	<form id="form_cadastro" name="form_cadastro" action="javascript:validar_dados();" method="post" style="margin:0">
						<select name="categoria" id="id_categoria" class="campoNormal">
						  <option value="-1">Selecione a Área de Interesse</option>
							<?
							$rcb = pg_query('select * from categorias_eventos order by idioma, titulo');
							while($rrcb = pg_fetch_assoc($rcb)){
								?>
								<option value="<?=$rrcb['id']?>"><?=$rrcb['titulo']?></option>
								<?
							}
							?>
						</select>                    	
                  <br />
                        <input class="campoNormal" type="text" name="nome" value="NOME" onfocus="javascript:verificar_campo(this);" onblur="javascript:verificar_campo(this);"><br/>
                    <input class="campoMenorFloat" type="text" name="email" value="E-MAIL" onfocus="javascript:verificar_campo(this);" onblur="javascript:verificar_campo(this);">
                        <div class="botaoOKFloat"><input type="image" src="media/img/btOK.jpg" /></div>
                    </form>
                    <br /><br />
                 </div>

			</div>
			<!-- fim direita -->          
        </div>
        <div id="rodape">
        	<div class="rodapeSelos"><img src="media/img/abrinq.gif" width="61" height="45" alt="Abrinq" /></div>
            <div class="rodapeSelos"><img src="media/img/seloEtanol.gif" width="38" height="45" alt="Etanol Verde" /></div>
            <div class="rodapeEndereco">PEDRA AGROINDUSTRIAL, ROD. ABRAÃO ASSED, KM 42, SERRANA - SP</div>
            <div class="rodapeTelefone">TELEFONE: (16) 3987-9000</div>
            <div class="rodapeSeloPub"><img src="media/img/pub.gif" width="71" height="23" alt="PUB DESING" /></div>
        </div>
    </div>
</body>
</html>
