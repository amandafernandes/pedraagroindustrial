// JavaScript Document
function mostraFlash(){
	var id = arguments[0];
	var swf = arguments[1];
	var width = arguments[2];
	var height = arguments[3];
	var parans = arguments[4] || {'scale':'noscale', 'wmode':'transparent', 'pluginspage':'http://www.macromedia.com/go/getflashplayer'};
	var expressInstall = arguments[5] || '/media/swf/expressinstall.swf';
	
	var so = new SWFObject(swf, id+"movie", width, height, "9", "transparent", true);
	
	for (k in parans) so.addParam(k, parans[k]);
	
	so.useExpressInstall(expressInstall);
	
	so.write(id);
}