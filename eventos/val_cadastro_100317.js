// JavaScript Document

//requer HttpRequest

function verificar_campo(campo)
{
	if(campo.value=="" && campo.name=="nome")
		campo.value = "NOME";
	else if(campo.value == "NOME")
		campo.value = "";	

	if(campo.value=="" && campo.name=="email")
		campo.value = "E-MAIL";
	else if(campo.value == "E-MAIL")
		campo.value = "";	
		
}

function validar_dados()
{
	var texto, msgaux, erro; 
	var frm = document.form_cadastro;
	var nomeClasse;
	
	erro = false;
	nomeClasse = "campoCadastroErro";

	//msgaux='O campo em destaque est� incompleto ou incorreto.';
	msgaux='Verifique os erros abaixo:\n';
	
	texto = frm.nome.value;
	if (texto.length < 1 || texto=="NOME") { 
		erro=true;
		msgaux+='-Entre com seu nome\n';
	}	
	
	texto = frm.email.value;
	if (texto.length < 1 || texto=="E-MAIL") 
	 { 
		erro=true;
		msgaux+='-Entre com seu e-mail\n';		
 	 }
	else
	 { 
		//Validando o e-mail
		var str = texto; // email string
		var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
		var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/; // valid
		if (!reg1.test(str) && reg2.test(str)) // if syntax is valid
		 {  texto=''  }
		else
		 { 
			erro=true;
			msgaux+='- Verifique se digitou seu e-mail corretamente';
		 }
	 }

	 // Verifica se ouve erro
	if (erro == false)
	{
		//mostraDiv('aguarde');
		/*criando dados para POST*/
		campos = "email=" + frm.email.value;
		campos += "&nome=" + frm.nome.value;
		campos += "&enviado=1";		
		HttpRequest("cadastro_env.php", "POST", true, campos, retornoCadNewsletter);		
		//frm.submit();
	} 
	else
	{
		//showAlert("", msgaux, 1, -130, -100, 1, 1);
		alert(msgaux);
    }
}

function retornoCadNewsletter()
{
	if (req.readyState == 4) 
	{
		//ocultaDiv("aguarde");		
		if (req.status == 200)
		{ 		
			//alert(req.responseText);
			if(req.responseText == 1)
			{
				//showAlert("", "E-mail cadastrado!<br>Agradecemos sua participa��o.<br>", 1, -130, -100, 1, 1);
				alert("Dados cadastrados! Agradecemos seu interesse!");
			}
			else if(req.responseText == 2)
			{	
				//showAlert("", "Seu email j� estava cadastrado, obrigado!<br>", 1, -130, -100, 1, 1);			
				alert("Seu email j� estava cadastrado! Agradecemos seu interesse!");
			}
		} 
		else 
		{
			//showAlert("", "Erro ao enviar cadastrar seu email<BR>Tente novamente mais tarde", 1, -130, -100, 1, 1);			
			alert("Erro ao cadastrar seu email. Tente novamente mais tarde!");
		}
		document.form_cadastro.reset();
	}
}
