
var req = null;
function HttpRequest(url, metodo, espera, dados, callback)
{
	/*
	url chamada = string
	metodo = POST / GET
	espera = true / false = aguarda ou nao a execu��o da p�gina e depois retorna os dados
	dados = string padronizada dos dados do form - usado apenas em post
	callback = nome funcao callback (passar ref ao objeto - nao string)
	*/
	req = null;

	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
	}else if (window.ActiveXObject) {
		try {
			req = new ActiveXObject("Msxml2.XMLHTTP.4.0");
		} catch(e) {
			try {
				req = new ActiveXObject("Msxml2.XMLHTTP.3.0");
			} catch(e) {
				try {
					req = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {
					try {
						req = new ActiveXObject("Microsoft.XMLHTTP");
					} catch(e) {
						req = false;
					}
				}
			}
		}
	}

	if (req) {
		if (metodo == 'GET')
		{
			var anticache = "&anticache=" + new Date().getMilliseconds() + Math.random();
            req.open("GET", url + anticache, espera);
			req.setRequestHeader("Cache-Control", "no-cache");
            req.setRequestHeader("Cache-Control", "post-check=0, pre-check=0");
            req.setRequestHeader("Pragma", "no-cache");
			req.onreadystatechange = callback;
            req.send(null);
        } 
		if (metodo == 'POST') {
			var anticache = "?anticache=" + new Date().getMilliseconds() + Math.random();
            req.open("POST", url + anticache, espera);
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			req.setRequestHeader("Cache-Control", "no-cache");
			req.setRequestHeader("Cache-Control", "post-check=0, pre-check=0");
			req.setRequestHeader("Pragma", "no-cache");
			req.onreadystatechange = callback;
            req.send(dados);
        }
	}
}