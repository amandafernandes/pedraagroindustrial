﻿<?ini_set('display_errors', 0); error_reporting(E_WARNING);
@session_start(); 
require_once("inc/conn.php");
if ($_SESSION['estaLogado']!=true){
    die("<script>location='trabalheConoscocopy.php';</script>");
}
$_PROCSEL = true;?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="estilos.css" rel="stylesheet" type="text/css" />
<title>Pedra Agroindustrial</title>
<script>
    window.onload = function (){
        var body = document.body,
        html = document.documentElement;
        var height = document.getElementById("container-lista").offsetHeight;
        height  += 200;
        document.getElementById("call").innerHTML = height;
        setInterval(function() {
            parent.postMessage(height,"https://www.pedraagroindustrial.com.br/");
        },1000);
    };
</script>
</head>

<body>
<?if ($_GET['erro'] == 1) {?><script>alert('Inscrição efetuada com sucesso.');</script><?}?>


<?
include("menuRHcopy.php");
?>
<br>
<div id="container-lista">
<?
$rs = pg_query("
SELECT
	rhtd_unidade.nm_unidade as nomeunidade,
	rhtd_vaga_disponivel.processo_seletivo as idprocesso,
	rhtd_vaga_disponivel.titulo_vaga as titulo,
	rhtd_vaga_disponivel.data_divulgacao as data,
	rhtd_vaga_disponivel.qtde_vagas as qtdevagas,
	rhtd_vaga_disponivel.descricao_vaga as descricao,
	rhtd_vaga_disponivel.conhecimentos as conhecimentos,
	rhtd_vaga_disponivel.observacoes as obs,
	rhtd_vaga_disponivel.remuneracao as remuneracao,
	rhtp_area_atuacao_cand.nm_area_atuacao as nomeareaatuacao,
	rhcand_proc_seletivo.status as status 
FROM
	rhtd_unidade
	Inner Join rhtd_vaga_disponivel ON rhtd_vaga_disponivel.unidade = rhtd_unidade.id_unidade
	Inner Join rhtp_area_atuacao_cand ON rhtp_area_atuacao_cand.id_tp_area_atuacao_cand = rhtd_vaga_disponivel.area_atuacao 
	Inner Join rhcand_proc_seletivo ON rhcand_proc_seletivo.processo_seletivo = rhtd_vaga_disponivel.processo_seletivo 
WHERE 
	rhcand_proc_seletivo.candidato = ".$_SESSION['idUserLogado']."
ORDER BY
	rhtd_vaga_disponivel.data_divulgacao DESC
");
if(pg_num_rows($rs)){
	while($rr=pg_fetch_assoc($rs)){
		?>
        <h2>Participação em processos seletivos</h2>
        <br/>
        <div class="job-item">
            <div class="job-item__container">
                <div class="tag green"><span> <?= $rr['nomeunidade'] ?></span></div>
                <h2><?= $rr['titulo'] ?> | <?= dataeuabr($rr['data']) ?></h2>
                <p><b>Área de Atuação : </b> <?= $rr['nomeareaatuacao'] ?> </p>        
                <p><b>Remuneração : </b> R$<?= number_format($rr['remuneracao'], 2, ',', ''); ?></p>
                <p><b>Quantidade de Vagas:</b> <?= $rr['qtdevagas'] ?></p>
                <p><b>Descrição da Vaga:</b><?= $rr['descricao'] ?></p>
                <p><b>Conhecimento : </b>
                <?= $rr['conhecimentos'] ?></p>
                <p><b>Observações : </b>
                <?= $rr['obs'] ?></p>
                <p><?=$rr['status']?></p><br />
            </div>
        </div> 
		<?
	}
}?>	 

<h1 id="call"></h1>
</div>
</body>
</html>
