<?
	define("PAGINA_ATUAL", basename($_SERVER["SCRIPT_NAME"]));
	define("URL_EMAIL", 'http://pub00/usinadapedra/site/');
	define("CURRICULO_RH", "carlos@pubdesign.com.br");


	$ARR_MESES = array("", "Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");


	function _w($s){print("<pre>");print($s);print("</pre>");}
	function _wa($a){print("<pre>");print_r($a);print("</pre>");}


	function valida_email($data) {
		if (preg_match('/^[^0-9][a-zA-Z0-9_-]+([.][a-zA-Z0-9_-]+)*[@][a-zA-Z0-9_-]+([.][a-zA-Z0-9_-]+)*[.][a-zA-Z]{2,4}$/', $data))
			return 0;
		else
			return 1;
	}	

	function valida_data($data) {		
		if (ereg ("([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})", $data))
			return 0;
		else
			return 1;
	}

	function valida_cep($data) {		
		if (ereg ("([0-9]{8})", $data))
			return 0;
		else
			return 1;
	}

	function valida_telefone($data) {
		if (ereg ("([(]{1}[0-9]{2}[)]{1})([0-9]{4})-([0-9]{4})", $data))
			return 0;
		else
			return 1;
	}

	function valida_sonumero($data) {
		if (ereg ("([0-9]{1,})",$data))
			return 0;
		else
			return 1;
	}

	function valida_cpf($cpf) {		
		$nulos = array("12345678909","11111111111","22222222222","33333333333", "44444444444","55555555555","66666666666","77777777777","88888888888","99999999999","00000000000");

		// Retira todos os caracteres que nao sejam 0-9
		$cpf = ereg_replace("[^0-9]", "", $cpf);

		//Retorna falso se houver letras no cpf
		if (!(ereg("[0-9]",$cpf)))
		    return 0;

		//Retorna falso se o cpf for nulo
		if(in_array($cpf, $nulos))
		    return 0;

		/*Calcula o pen�ltimo d�gito verificador*/
		$acum=0;
		for($i=0; $i<9; $i++) {
		  $acum+= $cpf[$i]*(10-$i);
		}
		
		$x=$acum % 11;
		$acum = ($x>1) ? (11 - $x) : 0;
		
		/* Retorna falso se o digito calculado eh diferente do passado na string */
		if ($acum != $cpf[9]){
		  return 0;
		}
		
		/*Calcula o �ltimo d�gito verificador*/
		$acum=0;
		for ($i=0; $i<10; $i++){
		  $acum+= $cpf[$i]*(11-$i);
		}
		
		$x=$acum % 11;
		$acum = ($x > 1) ? (11-$x) : 0;
		
		/* Retorna falso se o digito calculado eh diferente do passado na string */
		if ( $acum != $cpf[10]){
		  return 0;
		} 
		 
		/* Retorna verdadeiro se o cpf eh valido */
		return 1;
	}

	function valida_cnpj($data) {
		if (ereg("([0-9]{2}.[0-9]{3}.[0-9]{3}/[0-9]{4}-[0-9]{2})", $data) 
		&& $data != "111.111.111-11" 
		&& $data != "222.222.222-22"
		&& $data != "333.333.333-33"
		&& $data != "444.444.444-44"
		&& $data != "555.555.555-55"
		&& $data != "666.666.666-66"
		&& $data != "777.777.777-77"
		&& $data != "888.888.888-88"
		&& $data != "999.999.999-99"
		&& $data != "000.000.000-00")
			return 0;
		else
			return 1;		
	}
	
	function valida_ie($data) {
		if (ereg("([0-9]{3}.[0-9]{3}.[0-9]{3}.[0-9]{3})",$data) 
		&& $data != "111.111.111.111" 
		&& $data != "222.222.222.222"
		&& $data != "333.333.333.333"
		&& $data != "444.444.444.444"
		&& $data != "555.555.555.555"
		&& $data != "666.666.666.666"
		&& $data != "777.777.777.777"
		&& $data != "888.888.888.888"
		&& $data != "999.999.999.999"
		&& $data != "000.000.000.000")
		
			return 0;
		else
			return 1;		
	}
	
	
	function dataeuabr($data) {
		if($data == "")return "00/00/0000";	
		$arr = explode("-", $data);
		$dat = $arr[2] . "/" . $arr[1] . "/" . $arr[0];
		return $dat;
	}
	
	
	function databreua($data) {
		if($data == "")return "00/00/0000";
		$arr = explode("/", $data);
		$dat = $arr[2] . "-" . $arr[1] . "-" . $arr[0];
		return $dat;
	}
	
	function retiraAscentos($txt) {
		$in  = explode('-', '�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�-�');
		$out = explode('-', 'a-a-a-a-a-e-e-e-e-i-i-i-i-o-o-o-o-o-u-u-u-u-A-A-A-A-A-E-E-E-E-I-I-I-I-O-O-O-O-O-U-U-U-U');
		return str_replace($in, $out, utf8_decode($txt));
	}
	
	function mascara($in,$mask){
		$cont = 0;
		$strReturn = '';
		if ($mask == '#DECIMAL#') {
			$strReturn = number_format($in,2,",",".");
		} else if ($mask == '#NUMERO#') {
			$strReturn = $in*1;
		} else if ($mask == '#TEXTO#') {
			$strReturn = trim($in);
		} else {
			for ($i = 0;$i < strlen($mask);$i++) {
				if (substr($mask,$i,1) == '#') {
					$strReturn .= substr($in,$cont,1);
					$cont++;
				} else {
					$strReturn .= substr($mask,$i,1);
				}
			}
		}
		return $strReturn;
	}

	function enviaEmail($emailFrom, $titulo, $mensagem, $emailTo, $cc = '', $bcc = ''){
		$quebra = (strpos($_ENV['SERVER_SOFTWARE'], "Microsoft") === false)?"\n":"\r\n";

		$headers = "From: ".$emailFrom.$quebra;
		$headers .= "Content-Type: text/html; charset=UTF-8".$quebra;
		
		if ($cc != '') $headers .= "CC: ".$cc.$quebra;
		if ($bcc != '') $headers .= "BCC: ".$bcc.$quebra;
		
		if(!@mail($emailTo, $titulo, $mensagem, $headers)) return false;
		return true;
	}


	function enviaEmailAnexo($emailFrom, $titulo, $mensagem, $emailTo, $cc = '', $bcc = '', $arquivo, $pasta) {
		if (!$arquivo) return false;

		$urlArquivo = $pasta.$arquivo;

		$fp = fopen($urlArquivo,"rb");
		$anexo = fread($fp,filesize($urlArquivo));           
		$anexo = base64_encode($anexo); 
		fclose($fp);
		$anexo = chunk_split($anexo); 

		$boundary = "XYZ-".date("dmYis")."-ZYX"; 

		$quebra = (strpos($_ENV['SERVER_SOFTWARE'], "Microsoft") === false)?"\n":"\r\n";

		$mens = "--$boundary".$quebra;
		$mens .= "Content-Transfer-Encoding: 8bit".$quebra;
		$mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"".$quebra.$quebra; //plain
		$mens .= $mensagem.$quebra;
		$mens .= "--$boundary".$quebra;
		$mens .= "Content-Type: ".mime_content_type2($arquivo).$quebra; 
		$mens .= "Content-Disposition: attachment; filename=\"".$arquivo."\"".$quebra;
		$mens .= "Content-Transfer-Encoding: base64".$quebra.$quebra; 
		$mens .= $anexo.$quebra; 
		$mens .= "--$boundary--".$quebra; 

		$headers = "MIME-Version: 1.0".$quebra; 
		$headers .= "From: ".$emailFrom.$quebra;
		$headers .= "Reply-To: ".$emailFrom.$quebra;
		$headers .= "Return-Path: ".$emailFrom.$quebra;
		$headers .= "Message-ID: <".time()." TheSystem@".$_SERVER['SERVER_NAME'].">".$quebra;
		$headers .= "X-Mailer: PHP v".phpversion().$quebra;
		$headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"".$quebra;
		$headers .= "$boundary".$quebra;

		if (!mail($emailTo, $titulo, $mens, $headers)) return false;
		return true;
	}


	////////////////////////////////////////////////////////////////
	//COMPLETA COM 0
	////////////////////////////////////////////////////////////////
	function format($numero, $casas, $carac="0", $tipo=STR_PAD_LEFT) {
		//STR_PAD_LEFT (preencher a esquerda).
		//STR_PAD_BOTH (preencher de ambos os lados).
		//STR_PAD_RIGHT (preencher a direita).
		return str_pad($numero,$casas,$carac,$tipo);
	}

	function mime_content_type2($filename) {
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } else {
            return 'application/octet-stream';
        }
    }

	function gera_senha_esqueci(){
		$t = substr(md5(strtotime("now")), 0, 10);
		return array(md5($t), $t);
	}
?>